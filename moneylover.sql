-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th8 30, 2019 lúc 11:53 PM
-- Phiên bản máy phục vụ: 5.7.26-0ubuntu0.18.04.1
-- Phiên bản PHP: 7.3.7-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `moneylover`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_registerToken`
--

CREATE TABLE `tb_registerToken` (
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_todo`
--

CREATE TABLE `tb_todo` (
  `id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 là chưa thực hiện; 1 là đã thực hiện rồi',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_todo`
--

INSERT INTO `tb_todo` (`id`, `description`, `status`, `created_at`, `user_id`) VALUES
(23, 'Tạo báo cao theo danh mục', 1, '2019-08-13 02:35:00', 47),
(27, 'Đòi <a class=\"text-primary\" href=\"http://dark.moneylover.com/transaction/get/40\">khoản cho vay</a> vào ngày 2019-10-28', 0, '2019-08-25 09:14:28', 47),
(29, 'Zố', 0, '2019-08-30 15:55:48', 51);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_transaction`
--

CREATE TABLE `tb_transaction` (
  `id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `type_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_transaction`
--

INSERT INTO `tb_transaction` (`id`, `amount`, `type_id`, `wallet_id`, `description`, `transaction_date`, `created_at`) VALUES
(7, 10000, 1, 11, 'Ăn sáng', '2019-08-07 09:13:12', '2019-08-07 09:13:27'),
(8, 300000, 26, 11, 'Đi tập gym', '2019-08-07 09:49:56', '2019-08-07 09:50:11'),
(9, 200000, 40, 12, 'Đầu tư ví', '2019-08-07 09:50:44', '2019-08-07 09:51:01'),
(10, 10000, 1, 11, 'Ăn sáng', '2019-08-06 09:59:49', '2019-08-07 10:00:04'),
(11, 1000000, 46, 11, 'Lương đi làm', '2019-08-07 12:08:04', '2019-08-07 12:02:58'),
(12, 10000, 56, 12, 'Ăn thịt với rau', '2019-08-08 01:29:45', '2019-08-08 01:30:03'),
(16, 100000, 29, 12, NULL, '2019-08-09 01:31:20', '2019-08-09 01:31:41'),
(17, 50000, 24, 11, NULL, '2019-07-09 10:08:09', '2019-08-09 10:08:31'),
(18, 50000, 2, 11, NULL, '2019-08-10 05:34:45', '2019-08-10 05:22:33'),
(19, 10000, 56, 11, 'ăn sáng', '2019-08-10 23:00:22', '2019-08-10 15:03:03'),
(22, 10000, 45, 11, NULL, '2019-08-13 02:52:32', '2019-08-13 02:52:44'),
(23, 10000, 1, 11, NULL, '2019-08-14 03:52:24', '2019-08-14 03:52:49'),
(24, 40000, 3, 11, 'Ăn chè', '2019-08-14 11:10:40', '2019-08-14 11:11:14'),
(25, 20000, 54, 11, '[[10%/năm]]', '2019-08-16 02:28:19', '2019-08-16 02:28:34'),
(28, 60000, 60, 11, '[[5%/ngày]]', '2019-08-16 06:58:28', '2019-08-16 06:59:51'),
(30, 10000, 54, 11, NULL, '2019-08-16 08:06:35', '2019-08-16 08:07:29'),
(31, 10000, 60, 11, NULL, '2019-08-16 08:13:33', '2019-08-16 08:13:45'),
(32, 10000, 60, 11, '[[5%/ng]]', '2019-08-16 08:13:46', '2019-08-16 08:14:02'),
(33, 10000, 54, 11, '[[5%/ngày]]', '2019-08-16 08:14:45', '2019-08-16 08:15:08'),
(40, 100000, 54, 11, '[[5%/ngày]]', '2019-08-25 09:13:57', '2019-08-25 09:14:28'),
(41, 21000, 56, 11, 'Ăn sáng tại Circle K', '2019-08-26 01:00:07', '2019-08-26 11:25:47'),
(42, 15000, 2, 11, 'Cafe Circle K', '2019-08-26 08:00:00', '2019-08-26 11:27:29'),
(43, 7000, 43, 11, 'Tiền gửi xe', '2019-08-26 11:39:32', '2019-08-26 11:27:54'),
(44, 40000, 46, 11, 'Tiền công', '2019-08-26 11:39:59', '2019-08-26 11:28:22'),
(45, 20000, 56, 11, 'ăn mỳ hộp', '2019-08-27 01:00:32', '2019-08-27 10:01:54'),
(46, 30000, 56, 11, 'ăn sáng', '2019-08-28 01:17:24', '2019-08-28 01:17:40'),
(47, 20000, 44, 11, NULL, '2019-08-27 11:00:52', '2019-08-28 01:18:06'),
(48, 40000, 46, 11, NULL, '2019-08-28 11:00:25', '2019-08-29 01:36:11'),
(49, 20000, 56, 11, NULL, '2019-08-29 01:38:50', '2019-08-29 01:36:30'),
(51, 40000, 44, 11, NULL, '2019-08-29 08:58:17', '2019-08-30 08:58:33'),
(52, 50000, 56, 11, NULL, '2019-08-30 01:00:48', '2019-08-30 09:29:16'),
(53, 40000, 20, 11, 'Ăn cơm gà', '2019-08-30 14:15:20', '2019-08-30 14:15:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_transactionType`
--

CREATE TABLE `tb_transactionType` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` bit(1) NOT NULL COMMENT '1 là giao dịch + tiền, 0 là giao dịch - tiền',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `icon` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_transactionType`
--

INSERT INTO `tb_transactionType` (`id`, `name`, `parent_id`, `type`, `user_id`, `icon`) VALUES
(1, 'Ăn uống', 0, b'0', 0, 'assets/img/icons/eating.png'),
(2, 'Cà Phê', 1, b'0', 0, 'assets/img/icons/coffee.png'),
(3, 'Nhà hàng', 1, b'0', 0, 'assets/img/icons/restaurant.png'),
(4, 'Hóa đơn', 0, b'0', 0, 'assets/img/icons/bill.png'),
(5, 'Điện thoại', 4, b'0', 0, 'assets/img/icons/phone-bill.png'),
(6, 'Nước', 4, b'0', 0, 'assets/img/icons/water-bill.png'),
(7, 'Điện', 4, b'0', 0, 'assets/img/icons/electric-bill.png'),
(8, 'Thuê nhà', 4, b'0', 0, 'assets/img/icons/house-bill.png'),
(9, 'Internet', 4, b'0', 0, 'assets/img/icons/internet-bill.png'),
(10, 'Di chuyển', 0, b'0', 0, 'assets/img/icons/move.png'),
(11, 'Taxi', 10, b'0', 0, 'assets/img/icons/taxi-move.png'),
(12, 'Gửi xe', 10, b'0', 0, 'assets/img/icons/parking-move.png'),
(13, 'Xăng dầu', 10, b'0', 0, 'assets/img/icons/fuel-move.png'),
(14, 'Bảo dưỡng', 10, b'0', 0, 'assets/img/icons/maintain-move.png'),
(15, 'Mua sắm', 0, b'0', 0, 'assets/img/icons/shopping.png'),
(16, 'Quần áo', 15, b'0', 0, 'assets/img/icons/cloth-shopping.png'),
(17, 'Giày dép', 15, b'0', 0, 'assets/img/icons/shoe-shopping.png'),
(18, 'Phụ kiện', 15, b'0', 0, 'assets/img/icons/accessory-shopping.png'),
(19, 'Thiết bị điện tử', 15, b'0', 0, 'assets/img/icons/smartphone-shopping.png'),
(20, 'Bạn bè & Người yêu', 0, b'0', 0, 'assets/img/icons/lover-friend.png'),
(21, 'Giải trí', 0, b'0', 0, 'assets/img/icons/entertain.png'),
(22, 'Phim ảnh', 21, b'0', 0, 'assets/img/icons/movie-entertain.png'),
(23, 'Trò chơi', 21, b'0', 0, 'assets/img/icons/game-entertain.png'),
(24, 'Du lịch', 0, b'0', 0, 'assets/img/icons/travel.png'),
(25, 'Sức khỏe', 0, b'0', 0, 'assets/img/icons/healthy.png'),
(26, 'Thể thao', 25, b'0', 0, 'assets/img/icons/sport-healthy.png'),
(27, 'Khám sức khỏe', 25, b'0', 0, 'assets/img/icons/check-healthy.png'),
(28, 'Thuốc', 25, b'0', 0, 'assets/img/icons/pills-healthy.png'),
(29, 'Chăm sóc cá nhân', 25, b'0', 0, 'assets/img/icons/self-care-healthy.png'),
(30, 'Quà tặng & Quyên góp', 0, b'0', 0, 'assets/img/icons/gift.png'),
(31, 'Cưới hỏi', 30, b'0', 0, 'assets/img/icons/wedding-gift.png'),
(32, 'Tang lễ', 30, b'0', 0, 'assets/img/icons/grave-gift.png'),
(33, 'Từ thiện', 30, b'0', 0, 'assets/img/icons/giveout-gift.png'),
(34, 'Gia đình', 0, b'0', 0, 'assets/img/icons/family.png'),
(35, 'Con cái', 34, b'0', 0, 'assets/img/icons/baby-family.png'),
(36, 'Sửa chữa nhà cửa', 34, b'0', 0, 'assets/img/icons/housefixing-family.png'),
(37, 'Thú cưng', 34, b'0', 0, 'assets/img/icons/pet-family.png'),
(38, 'Giáo dục', 0, b'0', 0, 'assets/img/icons/learning.png'),
(39, 'Sách vở', 38, b'0', 0, 'assets/img/icons/book-learning.png'),
(40, 'Đầu tư', 0, b'0', 0, 'assets/img/icons/chart.png'),
(41, 'Kinh doanh', 0, b'0', 0, 'assets/img/icons/business.png'),
(42, 'Bảo hiểm', 0, b'0', 0, 'assets/img/icons/money-protected.png'),
(43, 'Khoản khác', 0, b'0', 0, 'assets/img/icons/other.png'),
(44, 'Thưởng', 0, b'1', 0, 'assets/img/icons/gold-medal.png'),
(45, 'Tiền lãi', 0, b'1', 0, 'assets/img/icons/percent.png'),
(46, 'Lương', 0, b'1', 0, 'assets/img/icons/salary.png'),
(47, 'Được tặng', 0, b'1', 0, 'assets/img/icons/give-gift.png'),
(48, 'Bán đồ', 0, b'1', 0, 'assets/img/icons/sale.png'),
(49, 'Khoản thu khác', 0, b'1', 0, 'assets/img/icons/other.png'),
(51, 'Tiền lãi ngân hàng', 45, b'1', 47, 'https://image.flaticon.com/icons/svg/1951/1951117.svg'),
(54, 'Cho vay', 0, b'0', 0, 'assets/img/icons/donate.png'),
(55, 'Đòi nợ', 0, b'1', 0, 'assets/img/icons/peak.png'),
(56, 'Ăn sáng', 1, b'0', 47, 'https://image.flaticon.com/icons/svg/926/926255.svg'),
(60, 'Vay', 0, b'1', 0, 'assets/img/icons/invest.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_transferLog`
--

CREATE TABLE `tb_transferLog` (
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_transferLog`
--

INSERT INTO `tb_transferLog` (`from_id`, `to_id`, `amount`, `created_at`) VALUES
(11, 3, 100000, '2019-08-25 09:53:21'),
(NULL, 12, 100000, '2019-08-25 14:47:35'),
(11, 12, 10000, '2019-08-26 01:49:46'),
(11, 12, 100000, '2019-08-26 01:50:38'),
(3, 11, 100000, '2019-08-26 10:53:39'),
(NULL, 12, 10000, '2019-08-26 10:54:07'),
(3, 11, 3000, '2019-08-27 03:50:18'),
(25, 26, 50000, '2019-08-30 15:45:20'),
(25, 26, 500000, '2019-08-30 15:45:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'http://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png',
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8mb4_unicode_ci,
  `remember_token` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_user`
--

INSERT INTO `tb_user` (`id`, `name`, `email`, `avatar`, `password`, `status`, `created_at`, `expired_at`, `description`, `remember_token`) VALUES
(0, 'Nguyễn Văn Phúc', 'root@moneylover.us', 'http://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png', '$2y$10$eNw.8IYl1Cb4pCmSkOfwDuczyR6u.MmoDYQQZM87211U/Ay8c57Iy', 1, '2019-08-01 04:40:42', '2019-08-01 04:40:42', NULL, '8xoACfoReaa07RS6T44GPJxrYoOb75nfZDbCBThG32YF84zWKP5cMXCWa0sW'),
(47, 'Nguyễn Văn Phúc', 'phucit.captech@gmail.com', 'https://static.comicvine.com/uploads/original/11111/111115170/3445548-4383058771-Avata.jpg', '$2y$10$2s6boBlnpMHsl4z3Hnm6Auzf6E/xcA6Q2Z/47utAAp30yI41iFZL.', 1, '2019-07-26 03:33:50', '2020-08-30 15:58:36', 'Sinh viên BKAP thực tập tại RIKKIE', 'BvDCIaPEj7xsAOU9TvQq8V4EEz8wBBLdIORd0VfCZuKodyNzTSExWNqc9njk'),
(51, 'Nguyễn Văn Phúc', 'nguyen.phuc21030110@gmail.com', 'http://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png', '$2y$10$wlPIl79eCEYEJvB3JT0hV.fBEO5vP/3kjo0RioIUvyVwhbcVtXii6', 1, '2019-08-30 15:40:27', '2020-08-30 15:56:28', NULL, 'T3sIlfhYhfAjcHpcEIxqpMJlMYUIoHq3svlbtZXTJKXxTxqj0mVdUQUc9K5z');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_wallet`
--

CREATE TABLE `tb_wallet` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `transfer_time` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_wallet`
--

INSERT INTO `tb_wallet` (`id`, `name`, `user_id`, `type_id`, `amount`, `description`, `transfer_time`, `created_at`, `updated_at`) VALUES
(3, 'Ví tiết kiệm mua nhà', 47, 3, 99997000, 'Heo TT7', 9, '2019-07-31 07:59:08', '2019-08-16 09:43:16'),
(11, 'Ví tiêu dùng', 47, 1, 940000, 'Sắp đóng tiền học rồi', 9, '2019-08-07 03:21:58', '2019-08-27 04:09:55'),
(12, 'VÍ tiền', 47, 1, 670000, NULL, 7, '2019-08-07 07:42:38', '2019-08-26 11:25:00'),
(25, 'Ví tiền tiết kiệm', 51, 3, 450000, 'mô tả', 2, '2019-08-30 15:44:05', '2019-08-30 15:44:48'),
(26, 'Ví 1', 51, 1, 1500000, NULL, 2, '2019-08-30 15:44:21', '2019-08-30 15:44:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_walletType`
--

CREATE TABLE `tb_walletType` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tb_walletType`
--

INSERT INTO `tb_walletType` (`id`, `name`, `icon`, `description`) VALUES
(1, 'Ví tiền mặt', 'assets/img/icons/wallet.png', 'Ví này tượng trưng cho tiền trong túi bạn hiện tại.'),
(2, 'Thẻ tín dụng', 'assets/img/icons/credit-card.png', 'Tượng trưng cho tiền trong thẻ tín dụng của bạn.'),
(3, 'Ví tiết kiệm', 'assets/img/icons/piggy-bank.png', 'Bạn có thể nhập những khoản tiêt kiệm của mình tại đây, tuy nhiên ví này không thể được sử dụng để tạo giao dịch.');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tb_registerToken`
--
ALTER TABLE `tb_registerToken`
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `tb_todo`
--
ALTER TABLE `tb_todo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `tb_transaction`
--
ALTER TABLE `tb_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `wallet_id` (`wallet_id`);

--
-- Chỉ mục cho bảng `tb_transactionType`
--
ALTER TABLE `tb_transactionType`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_user` (`user_id`);

--
-- Chỉ mục cho bảng `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_wallet`
--
ALTER TABLE `tb_wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Chỉ mục cho bảng `tb_walletType`
--
ALTER TABLE `tb_walletType`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tb_todo`
--
ALTER TABLE `tb_todo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT cho bảng `tb_transaction`
--
ALTER TABLE `tb_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT cho bảng `tb_transactionType`
--
ALTER TABLE `tb_transactionType`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT cho bảng `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT cho bảng `tb_wallet`
--
ALTER TABLE `tb_wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT cho bảng `tb_walletType`
--
ALTER TABLE `tb_walletType`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `tb_registerToken`
--
ALTER TABLE `tb_registerToken`
  ADD CONSTRAINT `tb_registerToken_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- Các ràng buộc cho bảng `tb_todo`
--
ALTER TABLE `tb_todo`
  ADD CONSTRAINT `tb_todo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- Các ràng buộc cho bảng `tb_transaction`
--
ALTER TABLE `tb_transaction`
  ADD CONSTRAINT `tb_transaction_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `tb_transactionType` (`id`),
  ADD CONSTRAINT `tb_transaction_ibfk_2` FOREIGN KEY (`wallet_id`) REFERENCES `tb_wallet` (`id`);

--
-- Các ràng buộc cho bảng `tb_transactionType`
--
ALTER TABLE `tb_transactionType`
  ADD CONSTRAINT `tb_user` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- Các ràng buộc cho bảng `tb_wallet`
--
ALTER TABLE `tb_wallet`
  ADD CONSTRAINT `tb_wallet_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  ADD CONSTRAINT `tb_wallet_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `tb_walletType` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

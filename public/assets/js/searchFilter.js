$(document).ready(function(){
	var $openSearchBar = $("#btnSearchBar").on('click',function() {
		var search = $("#search");
		var btnSearchBar = $(this);
		if(search.css('opacity')==0){
			openSearchBar(search, btnSearchBar);
		}else{
			closeSearchBar(search, btnSearchBar);
		};
	})
	var $search = $("#search").on('input',function(){
		var matcher = new RegExp($(this).val(), 'gi');
		$('.box').show().not(function(){
			return matcher.test($(this).find('.name').text())
		}).hide();
	})
})
function openSearchBar(selector, btn) {
	$('#searchIcon').attr('class','fas fa-times');
	$('#btnSearchBar > [data-toggle="tooltip"]').attr('data-original-title','Đóng');
	$('#btnSearchBar > [data-toggle="tooltip"]').attr('title','Đóng');
	btn.css('border-radius','0 9999px 9999px 0');
	selector.css('right','80px');
	selector.css('opacity','1');
	selector.css('width','40%');
}
function closeSearchBar(selector, btn) {
	selector.css('width','0px');
	setTimeout(function() {
		selector.css('opacity','0');
	},400)
	selector.css('right','60px');
	btn.css('border-radius','9999px 9999px 9999px 9999px');
	$('#searchIcon').attr('class','fas fa-search');

	var matcher = new RegExp('', 'gi');
	$('.box').show().not(function(){
		return matcher.test($(this).find('.name').text())
	}).hide();
	$('#btnSearchBar > [data-toggle="tooltip"]').attr('data-original-title','Tìm kiếm');
	$('#btnSearchBar > [data-toggle="tooltip"]').attr('title','Tìm kiếm');
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'  =>'/'],function(){
    Route::get('/', 'Management\DashboardController@Index');

    Route::get('/logout',function(){ Auth::logout(); return redirect()->to('/login'); });

    Route::get('/userinfo','Management\UserInforController@Index');
    Route::post('/userinfo','Management\UserInforController@ChangeInformation');

    Route::get('/userinfo/change-password','Management\UserInforController@IndexChangePassword');
    Route::post('/userinfo/change-password','Management\UserInforController@changePassword');
});

Route::get('/docs',function(){return view('Management.Documentation');});

Route::group(['prefix'  =>'/statistic'],function(){
    Route::get('trend', 'Management\StatisticController@TrendIndex');
    Route::get('/getChart',function(){ return redirect()->to('statistic/trend'); });
    Route::post('/getChart','Management\StatisticController@getChart');

    Route::get('report', 'Management\StatisticController@ReportIndex');
    Route::get('getTable',function(){ return redirect()->to('statistic/report'); });
    Route::post('/getTable','Management\StatisticController@getTable');
});

Route::group(['prefix' => '/todo'],function(){
    Route::get('add',function(){ return redirect()->to('/'); });
    Route::get('update',function(){ return redirect()->to('/'); });
    Route::get('delete',function(){ return redirect()->to('/'); });
    Route::get('get',function(){ return redirect()->to('/'); });
    Route::get('getNoti',function(){ return redirect()->to('/'); });
    Route::post('add','Management\TaskController@add');
    Route::post('update','Management\TaskController@update');
    Route::post('delete','Management\TaskController@delete');
    Route::post('get','Management\TaskController@get');
    Route::post('getNoti','Management\TaskController@getNoti');
});

Route::group(['prefix'  =>'/wallet'],function(){
    Route::get('/','Management\WalletController@Index');
    Route::post('/change','Management\WalletController@changeWalletInformation');

    Route::get('/delete',function(){ return redirect()->to('/wallet'); });
    Route::get('delete/{wallet_id?}', 'Management\WalletController@deleteWallet');

    Route::get('get',function(){ return redirect()->to('/wallet'); });
    Route::get('get/{wallet_id?}', 'Management\WalletController@getWalletInformation');

    Route::get('/choose','Management\WalletController@addWalletIndex');

    Route::get('/type',function(){ return redirect()->to('/wallet/choose'); });
    Route::get('/type/{type_id?}','Management\WalletController@addWalletForm');

    Route::post('/add','Management\WalletController@addWallet');
    Route::get('add',function(){ return redirect()->to('/wallet/type'); });

    Route::get('/transfer','Management\WalletController@chooseWalletToTransfer');
    Route::post('/transfer','Management\WalletController@transferMoneyForm');

    Route::get('/transfer/transfering',function(){ return redirect()->to('/wallet/transfer'); });
    Route::post('/transfer/transfering','Management\WalletController@transferMoney');
});

Route::group(['prefix'  =>'transaction'],function(){
    Route::get('/','Management\TransactionController@getListTransaction');
    
    Route::get('choosewallet','Management\TransactionController@chooseWalletToDoTransaction');
    Route::get('choosewallet/{wallet_id?}','Management\TransactionController@transactionForm');

    Route::get('add',function(){ return redirect()->to('transaction/choosewallet'); });
    Route::post('add','Management\TransactionController@addTransaction');

    Route::get('/addtype','Management\TransactionTypeController@addTransactionTypeForm');
    Route::post('/addtype','Management\TransactionTypeController@addTransactionType');

    Route::get('/updatetype',function(){ return redirect()->back(); });
    Route::post('/updatetype','Management\TransactionTypeController@UpdateTransactionTypeOfUser');

    Route::get('/type','Management\TransactionTypeController@getListTransactionTypeOfUser');
    Route::get('/type/delete/',function(){ return redirect()->to('transaction/type'); });
    Route::get('/type/delete/{type_id?}','Management\TransactionTypeController@DeleteTransactionTypeOfUser');

    Route::get('/type/get/',function(){ return redirect()->to('transaction/type'); });
    Route::get('/type/get/{type_id?}','Management\TransactionTypeController@GetTransactionTypeOfUser');

    Route::get('get',function(){ return redirect()->to('transaction'); });
    Route::get('get/{transaction_id?}','Management\TransactionController@getTransactionDetail');

    Route::get('update',function(){ return redirect()->to('transaction'); });
    Route::post('update','Management\TransactionController@changeTransactionDetail');

    Route::get('delete',function(){ return redirect()->to('transaction'); });
    Route::get('delete/{transaction_id?}','Management\TransactionController@deleteTransaction');
});

Route::group(['prefix'  =>'/login'],function(){
    Route::get('/','Auth\LoginController@Index');
    Route::post('/','Auth\LoginController@Login');

    Route::get('failure',[
        'as'    => 'loginFailure',
        'uses'  =>function(){ return view('Auth.Login'); }
    ]);

    Route::get('reset',[
        'as'    => 'resetPassword',
        'uses'  =>function(){ return view('Auth.ResetPassword'); }
    ]);

    Route::post('reset','Auth\ForgotPasswordController@resetPassword');
});

Route::group(['prefix'  =>'/register'],function(){
    Route::get('/','Auth\RegisterController@Index');
    Route::post('','Auth\RegisterController@Create');

    Route::get('notification',[
        'as'    => 'registerNotification',
        'uses'  =>function(){ return view('Auth.Notification'); }
    ]);

    Route::get('/resend',function(){ return redirect()->to('/login'); });
    Route::get('/resend/{email?}', 'Auth\RegisterController@reSend');

    Route::get('/verify',function(){ return redirect()->to('/login'); });
    Route::get('/verify/{token?}', 'Auth\VerificationController@Verify');
});

Route::group(['prefix'  =>'/forgot'],function(){
    Route::get('/',function(){ return view('Auth.ForgotPassword'); });
    Route::post('/','Auth\ForgotPasswordController@makeResetPasswordAndSendUser');

    Route::get('notification',[
        'as'    => 'forgotNotification',
        'uses'  =>function(){ return view('Auth.Notification'); }
    ]);
    Route::get('failure',[
        'as'    => 'failEmail',
        'uses'  =>function(){ return view('Auth.ForgotPassword'); }
    ]);
});

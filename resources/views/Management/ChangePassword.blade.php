@extends('Management.Widget.master')
@section('banner','Đổi mật khẩu')
@section('title','Đổi mật khẩu')
@section('content')
<div class="row">
	<div class="col-xl-8 order-xl-1">
		<div class="card">
			<div class="card-header">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0 text-white">Đổi mật khẩu</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form method="post" action="{{url('userinfo/change-password')}}">
					{{ csrf_field() }}
					<h6 class="heading-small text-white mb-4">Đổi mật khẩu</h6>
					<div class="pl-lg-4">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group focused">
									<label class="form-control-label text-white" for="">Mật khẩu mới</label>
									<input type="password" autocomplete="off" placeholder="Từ 6 - 32 kí tự" name="newPassword" value="" class="form-control form-control-alternative">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="form-control-label text-white" for="">Nhập lại mật khẩu mới</label>
									<input type="password" autocomplete="off" placeholder="Nhập lại mật khẩu vừa rồi.." name="newPassword_confirm" value="" class="form-control form-control-alternative">
								</div>
							</div>
						</div>
					</div>
					<h6 class="heading-small text-white mb-4">Nhập mật khẩu xác nhận thay đổi thông tin</h6>
					<div class="pl-lg-4">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group focused">
									<input type="password" class="form-control form-control-alternative" autocomplete="off" placeholder="Mật khẩu cũ của bạn" name="oldPassword" value="">
								</div>
							</div>
							<div class="col-lg-6">
								<button type="submit" class="btn btn-lg btn-primary">Thay đổi</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
		<div class="card card-user">
			<div class="card-body">
				<p class="card-text">
				</p><div class="author">
					<div class="block block-one"></div>
					<div class="block block-two"></div>
					<div class="block block-three"></div>
					<div class="block block-four"></div>
					<a href="javascript:void(0)">
						<img class="avatar" src="{{url(Auth::user()->avatar)}}" alt="...">
						<h5 class="title">{{Auth::user()->name}}</h5>
					</a>
					<p class="description">
						Người dùng
					</p>
				</div>
				<p></p>
				<div class="card-description text-center">
					{{Auth::user()->description}}
				</div>
			</div>
			<div class="card-footer">
			</div>
		</div>
	</div>
</div>
@endsection
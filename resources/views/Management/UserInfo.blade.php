@extends('Management.Widget.master')
@section('banner','Thông tin người dùng')
@section('title','Thông tin người dùng')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<div class="row align-items-center">
					<div class="col-9">
						<h3 class="mb-0 text-white">Thông tin tài khoản</h3>
					</div>
					<div class="col-3 text-right">
						<a href="#" id="change-info" class="btn btn-sm btn-primary mr-4">Chỉnh sửa</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form method="post" action="{{url('userinfo')}}" enctype="multipart/form-data">
					<h6 class="heading-small text-white mb-4">Thông tin chung</h6>
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label> Email</label>
								<input type="text" class="form-control" disabled="" placeholder="" value="{{Auth::user()->email}}"  id="input-email">
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label>Tên người dùng</label>
								<input type="text" class="form-control" name="name" placeholder="Tên của bạn" readonly="" autocomplete="off" value="{{Auth::user()->name}}">
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Ảnh đại diện</label>
								<input type="file" class="btn btn-primary" name="avatar_img" readonly="">
								<br>
								<label for="avatar">hoặc nhập đường dẫn (Nếu bạn nhập cả 2, ảnh tải lên sẽ được ưu tiên)</label>
								<input type="text" name="avatar_text" class="form-control" readonly="" placeholder="Ảnh đại diện" autocomplete="off" value="{{url(Auth::user()->avatar)}}">
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<h6 class="heading-small text-white mb-4">Mô tả</h6>
								<textarea rows="4" cols="80" class="form-control" readonly="" name="description" placeholder="Mô tả chút gì đi chứ!" value="Mike">{{Auth::user()->description}}</textarea>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<h6 class="heading-small text-white mb-4">Mật khẩu xác nhận</h6>
								<input type="password" name="password" class="form-control" readonly="" >
							</div>
						</div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-fill btn-primary mt" disabled="true" id="confirm-change">Lưu</button>
						</div>
					</div>
				</form>
			</div>
			<div class="card-footer">
				
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-user">
			<div class="card-body">
				<p class="card-text">
				</p><div class="author">
					<div class="block block-one"></div>
					<div class="block block-two"></div>
					<div class="block block-three"></div>
					<div class="block block-four"></div>
					<a href="javascript:void(0)">
						<img class="avatar" src="{{url(Auth::user()->avatar)}}" alt="...">
						<h5 class="title">{{Auth::user()->name}}</h5>
					</a>
					<p class="description">
						Người dùng
					</p>
				</div>
				<p></p>
				<div class="card-description text-center">
					{{Auth::user()->description}}
				</div>
			</div>
			<div class="card-footer">
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#change-info').on('click',function() {
			$('.form-control').each(function() {
				if($(this).attr('id')!='input-email'){
					$(this).removeAttr('readonly');
					$(this).prop('disabled',false);
				}
			});

			$('#confirm-change').prop('disabled', false);
		})
	})
</script>
@endsection
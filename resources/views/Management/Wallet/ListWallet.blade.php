@extends('../Management.Widget.master')
@section('title','Quản lí ví')
@section('banner','Danh sách các ví của bạn')
@section('content')
<?php use \App\Http\Controllers\Controller; ?>
<a id="btnSearchBar" class="btn btn-primary">
	<center data-toggle="tooltip" data-placement="left" title="Tìm kiếm"><i id="searchIcon" class="fas fa-search"></i></center>
</a>
<input type="text" id="search" autocomplete="off" placeholder="Nhập tên ví của bạn...">
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Danh sách</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row" id="parent">
					@foreach($wallets as $wallet)
					<div class="box col-xl-12 col-lg-12" style="margin-bottom: 15px;">
						<a class="link-walletType" href="{{url('wallet/get')}}/{{$wallet->id}}">
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<span class="name text-yellow h2 font-weight-bold mb-0">{{$wallet->name}}</span>
										</div>
										<div class="col-auto">
											<img src="{{url($wallet->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
										</div>
									</div>
									<p class="mt-3 mb-0 text-white ">
										<span class="text-nowrap">Số dư: {{number_format($wallet->amount,2)}} VNĐ</span>
									</p>
									@if($wallet->description)
									<p class="mt-3 mb-0 text-white text-sm">
										<span class="text-nowrap">
											{{$wallet->description}}
										</span>
									</p>
									@else
									<p class="mt-3 mb-0 text-muted text-sm">
										<span class="text-nowrap">
											Chưa có bất kì mô tả nào..
										</span>
									</p>
									@endif
									<p class="text-right">
									<button class="mt-3 mb-0 text-nowrap text-muted text-sm"  style="background-color: #27293D; border: none; outline: none; padding: 0" data-toggle="tooltip" data-placement="right" title="Lúc {{$wallet->created_at}}">Được khởi tạo từ 
										{{Controller::getTimeDiffSentence($wallet->created_at)}}</button>
									</p>

									</div>
								</div>
							</a>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection
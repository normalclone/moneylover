@extends('../Management.Widget.master')
@section('title','Chọn loại ví')
@section('banner','Chọn loại ví')
@section('content')
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Chọn loại ví bạn muốn thêm</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
				@foreach($walletTypes as $walletType)
				<div class="col-xl-6 col-lg-6" style="margin-bottom: 15px;" >
					<a class="link-walletType" href="{{url('wallet/type')}}/{{$walletType->id}}" >
					<div class="card card-stats mb-4 mb-xl-0">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<span class="h2 font-weight-bold mb-0 text-yellow">{{$walletType->name}}</span>
								</div>
								<div class="col-auto">
										<img src="{{url($walletType->icon)}}" width="50px" height="50px" alt="">
								</div>
							</div>
							<p class="mt-3 mb-0 text-white text-sm">
								<span class="">{{$walletType->description}}</span>
							</p>
						</div>
					</div>
					</a>
				</div>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
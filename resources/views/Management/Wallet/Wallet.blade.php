@extends('Management.Widget.master')
@section('banner','Thông tin ví')
@section('title','Thông tin ví')
@section('content')
<div class="col-md-4">
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
		<div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
			<div class="modal-content bg-gradient-danger">

				<div class="modal-header">
					<h6 class="modal-title text-white" id="modal-title-notification ">Chú ý!</h6>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="py-3 text-center">
						<h4 class="heading mt-4">Bạn có chắc muốn xóa ví này? <br> (Bao gồm cả các giao dịch có liên quan)!</h4>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" onclick="window.location.href = '{{url('/wallet/delete')}}/{{$data['wallet']->id}}' " class="btn-warning btn btn-danger-custom" style="background: white; color: #f56036">Xác nhận xóa</button>
					<button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Đóng</button> 
				</div>

			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-3 order-xl-1">
		<div class="card card-user">
			<div class="card-body">
				<p class="card-text">
				</p><div class="author">
					<div class="block block-one"></div>
					<div class="block block-two"></div>
					<div class="block block-three"></div>
					<div class="block block-four"></div>
					<a href="javascript:void(0)">
						<img class="avatar" src="{{url($data['wallet']->WalletType->icon)}}" alt="..." style="border: none !important; border-radius: 0% !important">
						<h5 class="title">{{$data['wallet']->WalletType->name}}</h5>
					</a>
					<p class="description">
						{{$data['wallet']->name}}
					</p>
				</div>
				<p></p>
				<div class="card-description text-left">
					<p class="text-left text-sm text-muted">
						Được khởi tạo từ : 
					</p>
					<p class="text-right">{{$data['wallet']->created_at}}</p>
					<p class="text-left text-sm text-muted">
						Lần chỉnh sửa cuối : 
					</p>
					<p class="text-right">{{$data['wallet']->updated_at}}</p>
					@if(!empty($data['transactions'][0]))
					<p class="text-left text-sm text-muted">
						Giao dịch cuối được tạo lúc: 
					</p>
					<p class="text-right">{{$data['transactions'][0]->created_at}}</p>
					@endif
				</div>
			</div>
			<div class="card-footer">
				
			</div>
		</div>
	</div>
	<div class="col-xl-9 order-xl-0">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Thông tin</h3>
					</div>
					<div class="col-4 text-right">
						<a href="#!" id="change-info" style="display: inline;" class="btn btn-sm btn-primary">Sửa</a>

						
						<a href="#!" style="display: inline;" data-toggle="modal" data-target="#confirm-delete" class="btn btn-sm btn-primary">Xóa</a>
						
					</div>
				</div>
			</div>
			<div class="card-body">
				<form method="post" action="{{url('wallet/change')}}" >
					{{ csrf_field() }}
					<input type="hidden" value="{{$data['wallet']->id}}" name="wallet_id">
					<h6 class="heading-small text-muted mb-4">Thông tin chung</h6>
					<div class="pl-lg-4">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group focused">
									<label class="form-control-label" for="">Tên {{strtolower($data['wallet']->WalletType->name)}}</label>
									<input type="text" readonly="" autocomplete="off" placeholder="VD: Ví tiền 1..." name="walletName" value="{{$data['wallet']->name}}" class="form-control form-control-alternative">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="form-control-label" for="">Số dư</label>
									<input type="text" readonly="" autocomplete="off" placeholder="VNĐ 1,000,000.00" name="amount" value="{{$data['wallet']->amount}}" class="form-control form-control-alternative money-format">
									<label for="" class="money-text" style="margin: 5px 0 0 0"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="pl-lg-4">
						<div class="form-group focused">
							<label>Mô tả</label>
							<textarea name="description" rows="3" readonly="" class="form-control form-control-alternative" placeholder="Chưa có mô tả nào..">{{$data['wallet']->description}}</textarea>
						</div>
					</div>
					<div class="pl-lg-4">
						<div class="form-group focused">
							<label>Loại ví</label>
							<select name="type_id" disabled="true" class="form-control form-control-alternative">
								@foreach($data['walletType'] as $type)
								@if($data['wallet']->WalletType->id == $type->id)
								<option style="background-color: #27293D; color: white" value="{{$type->id}}" selected="">{{$type->name}}</option>
								@else
								<option style="background-color: #27293D; color: white" value="{{$type->id}}">{{$type->name}}</option>
								@endif
								@endforeach
							</select>
						</div>
					</div>
					<div class="pl-lg-4">
						<div class="row">
							<div class="col-lg-9">
							</div>
							<div class="col-lg-3 text-center">
								<div class="form-group">
									<!-- <label class="form-control-label" for=""></label> -->
									<button type="submit" disabled="true" id="confirm-change" class="btn btn-lg btn-primary">Thay đổi</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@if(count($data['wallet']->Transactions) != 0)
<div class="row mt-3">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Lịch sử giao dịch</h3>
					</div>
					
				</div>
			</div>
			<div class="card-body scroll-bar" style="max-height: 500px; overflow-y: auto">
				<div class="table-responsive ">
					<!-- Projects table -->
					<table class="table align-items-center table-flush">
						<thead class="thead">
							<tr>
								<th scope="col"></th>
								<th scope="col">Loại giao dịch</th>
								<th scope="col">Thời gian (YYYY/MM/DD)</th>
								<th scope="col">Số tiền</th>
							</tr>
						</thead>
						<tbody>
							<?php $count=0 ?>
							@foreach($data['transactions'] as $tr)
							<?php $count++ ?>
							@if($tr->TransactionType->type == 1)
							<tr count="{{$count}}" class="display-none table-tr">
								<th scope="row">
									<img src="{{url($tr->TransactionType->icon)}}" width="25" height="25" alt="">
								</th>
								<td class="text-success">
									{{$tr->TransactionType->name}}
								</td>
								<td class="text-success">
									{{$tr->transaction_date}}
								</td>
								<td class="text-success">
									<i class="fas fa-arrow-up text-success mr-3"></i> {{number_format($tr->amount),2}} VNĐ
								</td>
							</tr>
							@else
							<tr count="{{$count}}" class="display-none table-tr">
								<th scope="row">
									<img src="{{url($tr->TransactionType->icon)}}" width="25" height="25" alt="">
								</th>
								<td class="text-danger">
									{{$tr->TransactionType->name}}
								</td>
								<td class="text-danger">
									{{$tr->transaction_date}}
								</td>
								<td class="text-danger">
									<i class="fas fa-arrow-down text-warning mr-3"></i> {{number_format($tr->amount),2}} VNĐ
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
					<center>
					@if(count($data['wallet']->Transactions)>7)
					<div class="pagination-table" id="transaction-table">
						<a href="#" class="first" data-action="first">&laquo;</a>
						<a href="#" class="previous" data-action="previous">&lsaquo;</a>
						<input type="text" readonly="readonly" data-max-page="40" />
						<a href="#" class="next" data-action="next">&rsaquo;</a>
						<a href="#" class="last" data-action="last">&raquo;</a>
					</div>
					@endif
				</center>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@if(count($data['transferLogs']) != 0)
<div class="row mt-3">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Lịch sử chuyển tiền</h3>
					</div>
					
				</div>
			</div>
			<div class="card-body scroll-bar" style="max-height: 500px; overflow-y: auto">
				<div class="table-responsive ">
					<!-- Projects table -->
					<table class="table align-items-center table-flush">
						<thead class="thead">
							<tr>
								<th scope="col" class="text-center">Từ ví</th>
								<th scope="col" class="text-center">Số tiền</th>
								<th scope="col" class="text-center">Tới ví</th>
								<th scope="col" class="text-center">Thời gian (YYYY/MM/DD)</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['transferLogs'] as $trl)
							<tr>
								<td class="text-danger text-center">{{$trl->fromWalletName()}}</td>
								<td class="text-yellow text-center">{{number_format($trl->amount,0)}} VNĐ</td>
								<td class="text-success text-center">{{$trl->toWalletName()}}</td>
								<td class="text-center">{{$trl->created_at}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<script src="{{url('./assets/js/plugins/jquery.pagination.js')}}"></script>
<script>
	$(function () {
		$('#change-info').on('click',function() {
			$('.form-control').each(function() {
				if($(this).attr('class')!='form-control form-control-alternative form-email'){
					$(this).removeAttr('readonly');
					$(this).prop('disabled',false);
				}
			});

			$('#confirm-change').prop('disabled', false);
		})

		for (var i = 1; i <= 7; i++) { 
			var selector = "tr[count='"+i+"']";
			$(selector).attr('class','table-tr')
		};
		$('#transaction-table').jqPagination({
			max_page	: {{ceil(count($data['wallet']->Transactions)/7)}},
			paged : function(page) {
				$('.table-tr').attr('class','display-none table-tr');
				var cal = page*7;
				for(var i = cal-6; i<=cal;i++){
					var selector = "tr[count='"+i+"']";
					$(selector).attr('class','table-tr')
				}
			}
		});
	})
</script>
@endsection
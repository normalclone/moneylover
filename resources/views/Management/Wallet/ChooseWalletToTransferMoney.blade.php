@extends('../Management.Widget.master')
@section('title','Chuyển tiền')
@section('banner','Chọn ví chuyển và nhận')
@section('content')
<form action="{{url('wallet/transfer')}}" method="post">
	{{ csrf_field() }}
	<input type="hidden" name="from_id" id="from-input">
	<input type="hidden" name="to_id" id="to-input">
	<button type="submit" id="btn-confirm-transfer" class="btn btn-info" disabled="true"  data-toggle="tooltip" data-placement="left" data-original-title="Bước kế tiếp">
		<center ><i class="fas fa-arrow-right"></i></center>
	</a>
</button>
<a id="btnSearchBar" class="btn btn-primary">
	<center data-toggle="tooltip" data-placement="left" data-original-title="Tìm kiếm"><i id="searchIcon" class="fas fa-search"></i></center>
</a>
<input type="text" id="search" autocomplete="off" placeholder="Nhập tên ví của bạn...">
<div class="row">
	<div class="col-xl-6 order-xl-2">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h4 class="mb-0" id="from-text">Chuyển từ</h4>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row" id="parent">
					@foreach($wallets as $wallet)
					<div class="box col-xl-12 col-lg-12" style="margin-bottom: 15px;">
						<a class="link-walletType from" data="{{$wallet->id}}" href="#!" >
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<span class="name h2 text-danger font-weight-bold mb-0">{{$wallet->name}}</span>
										</div>
										<div class="col-auto">
											<img src="{{url($wallet->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
										</div>
									</div>
									<p class="mt-3 mb-0 text-white ">
										<span class="text-nowrap">Số dư: {{number_format($wallet->amount,2)}} VNĐ</span>
									</p>
									<p class="mt-3 mb-0 text-muted text-sm">
										<span class="text-nowrap">
											@if($wallet->description)
											{{$wallet->description}}
											@else
											Chưa có bất kì mô tả nào..
											@endif
										</span>
									</p>
								</div>
							</div>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<div class="col-xl-6 order-xl-2">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h4 class="mb-0" id="to-text">Tới</h4>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row" id="parent">
					@foreach($wallets as $wallet)
					<div class="box col-xl-12 col-lg-12" style="margin-bottom: 15px;">
						<a class="link-walletType to"  data="{{$wallet->id}}" href="#!" >
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<span class="name h2 text-success font-weight-bold mb-0">{{$wallet->name}}</span>
										</div>
										<div class="col-auto">
											<img src="{{url($wallet->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
										</div>
									</div>
									<p class="mt-3 mb-0 text-white ">
										<span class="text-nowrap">Số dư: {{number_format($wallet->amount,2)}} VNĐ</span>
									</p>
									<p class="mt-3 mb-0 text-muted text-sm">
										<span class="text-nowrap">
											@if($wallet->description)
											{{$wallet->description}}
											@else
											Chưa có bất kì mô tả nào..
											@endif
										</span>
									</p>
								</div>
							</div>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.from').on('click',function() {
			event.preventDefault();
			$('.from').each(function() {
				$(this).attr('id','');
			})
			$(this).attr('id','from');
			$('#from-text').html('Chuyển từ: ' + $('#from > .card > .card-body > .row > .col > .name').html());
			$('#from-input').val($('#from').attr('data'));
			checkIfSameWallet($('#from-input').val(), $('#to-input').val());
		})
		$('.to').on('click',function() {
			event.preventDefault();
			$('.to').each(function() {
				$(this).attr('id','');
			})
			$(this).attr('id','to');
			$('#to-text').html('Tới: ' + $('#to > .card > .card-body > .row > .col > .name').html());
			$('#to-input').val($('#to').attr('data'));
			checkIfSameWallet($('#from-input').val(), $('#to-input').val());
		})
	})
	function checkIfSameWallet(from, to) {
		var btn = $('#btn-confirm-transfer');
		if(from != to){
			btn.prop('disabled', false);
		}
		if(from == to || from == "" || to == ""){
			btn.prop('disabled', true);
		}
	}
</script>
@endsection
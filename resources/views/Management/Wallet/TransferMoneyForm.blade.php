@extends('../Management.Widget.master')
@section('title','Chuyển tiền')
@section('banner','Chuyển tiền')
@section('content')
<a id="btnSearchBar" class="btn btn-warning" href="{{url('wallet/transfer')}}">
	<center data-toggle="tooltip" data-placement="left" title="Quay lại">
		<i class="fa fa-reply"></i>
	</center>
</a>
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Chuyển tiền</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form action="{{url('/wallet/transfer/transfering')}}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="from_id" value="{{$data['from']->id}}">
							<input type="hidden" name="to_id" value="{{$data['to']->id}}">
							<h6 class="heading-small text-muted mb-4">Chuyển từ {{$data['from']->name}} tới {{$data['to']->name}}</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="box col-xl-5 col-lg-5" style="margin-bottom: 15px;">
										<a class="link-walletType from" href="{{url('wallet/get')}}/{{$data['from']->id}}" id="from">
											<div class="card card-stats mb-4 mb-xl-0">
												<div class="card-body">
													<div class="row">
														<div class="col">
															<span class="name h2 text-danger font-weight-bold mb-0">{{$data['from']->name}}</span>
														</div>
														<div class="col-auto">
															<img src="{{url($data['from']->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
														</div>
													</div>
													<p class="mt-3 mb-0 text-yellow ">
														<span class="text-nowrap">Số dư: {{number_format($data['from']->amount,2)}} VNĐ</span>
													</p>
													<p class="mt-3 mb-0 text-muted text-sm">
														<span class="text-nowrap">
															@if($data['from']->description)
															{{$data['from']->description}}
															@else
															Chưa có bất kì mô tả nào..
															@endif
														</span>
													</p>
												</div>
											</div>
										</a>
									</div>
									<div class="box col-xl-2 col-lg-2" style="margin-bottom: 15px;">
										<center ><h1 class="arrow-right"><i class="fas fa-arrow-right"></i></h1></center>
										<center ><h1 class="arrow-down"><i class="fas fa-arrow-down"></i></h1></center>
									</div>
									<div class="box col-xl-5 col-lg-5" style="margin-bottom: 15px;">
										<a class="link-walletType from" href="{{url('wallet/get')}}/{{$data['to']->id}}" id="to">
											<div class="card card-stats mb-4 mb-xl-0">
												<div class="card-body">
													<div class="row">
														<div class="col">
															<span class="name h2 text-success font-weight-bold mb-0">{{$data['to']->name}}</span>
														</div>
														<div class="col-auto">
															<img src="{{url($data['to']->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
														</div>
													</div>
													<p class="mt-3 mb-0 text-white ">
														<span class="text-nowrap">Số dư: {{number_format($data['to']->amount,2)}} VNĐ</span>
													</p>
													<p class="mt-3 mb-0 text-muted text-sm">
														<span class="text-nowrap">
															@if($data['to']->description)
															{{$data['to']->description}}
															@else
															Chưa có bất kì mô tả nào..
															@endif
														</span>
													</p>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="form-control-label" for="">Số tiền muốn chuyển</label>
											<input type="text" autocomplete="off" placeholder="VNĐ 1,000,000.00" name="transferAmount" value="" class="form-control form-control-alternative money-format">
											<label for="" class="money-text" style="margin: 5px 0 0 0">1 triệu</label>
										</div>
									</div>

								</div>
							</div>
							<hr class="my-4">
							<h6 class="heading-small text-muted mb-4">Xác nhận</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group focused">
											<input type="password" class="form-control form-control-alternative" autocomplete="off" placeholder="Nhập mật khẩu xác nhận..." name="password" value="">
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group focused">
											<button type="submit" class="btn btn-lg btn-primary" style="margin-top: -5px">Xác nhận </button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
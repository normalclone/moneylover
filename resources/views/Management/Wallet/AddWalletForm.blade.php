@extends('../Management.Widget.master')
@section('title','Thêm '.strtolower($type->name))
@section('banner','Thiết lập thông tin')
@section('content')
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Thêm {{strtolower($type->name)}}</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form action="{{url('/wallet/add')}}" method="post">
							{{ csrf_field() }}
							<input type="hidden" value="{{$type->id}}" name="type_id">
							<h6 class="heading-small text-muted mb-4">Thiết lập</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group focused">
											<label class="form-control-label" for="">Tên {{strtolower($type->name)}}</label>
											<input type="text" autocomplete="off" placeholder="VD: Ví tiền 1..." name="walletName" value="{{old('walletName')}}" class="form-control form-control-alternative">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="">Số dư ban đầu</label>
											<input type="text" autocomplete="off" placeholder="VNĐ 1,000,000.00" name="amount" value="{{old('amount')}}" class="form-control form-control-alternative money-format">
											<label for="" class="money-text" style="margin: 5px 0 0 0"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="pl-lg-4">
								<div class="form-group focused">
									<label>Mô tả</label>
									<textarea name="description" rows="3" class="form-control form-control-alternative" placeholder="Ví này mua heo..">{{old('description')}}</textarea>
								</div>
							</div>
							<hr class="my-4">
							<h6 class="heading-small text-muted mb-4">Xác nhận</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group focused">
											<input type="password" class="form-control form-control-alternative" autocomplete="off" placeholder="Nhập mật khẩu xác nhận..." name="password" value="">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group focused">
											<button type="submit" class="btn btn-lg btn-primary">Thêm {{strtolower($type->name)}}</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@extends('Management.Widget.master')
@section('title','Xu hướng')
@section('banner','Xu hướng')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-xl-12">
    <div class="card shadow">
      <div class="card-header border-0">
        <div class="row align-items-center">
          <div class="col-8">
            <h3 class="mb-0">Tạo truy vấn</h3>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="form-group">
              <label class="form-control-label" for="">Từ ngày</label>
              <input type="text" id="from_date" autocomplete="off" placeholder="____-__-__ __:__:__" value="" class="form-control form-control-alternative datetimepicker">
            </div>
          </div>
          <div class="col-xs-9 col-sm-9 col-md-5 col-lg-5">
            <div class="form-group">
              <label class="form-control-label" for="">Tới ngày</label>
              <input type="text" id="to_date" autocomplete="off" placeholder="____-__-__ __:__:__" value="" class="form-control form-control-alternative datetimepicker">
            </div>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <div class="form-group">
              <br>
              <button class="btn btn-primary" style="margin-top: 5px" id="query">Truy vấn</button>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="form-control-label" for="">Phím tắt</label>
            <div class="row">
              <div class="col-auto" style="margin: 5px 0px">
                <a href="#!" id="this-week" class="text-red"><b>Tuần này</b></a>
              </div>
              <div class="col-auto" style="margin: 5px 0px">
                <a href="#!" id="last-week" class="text-orange"><b>Tuần trước</b></a>
              </div>
              <div class="col-auto" style="margin: 5px 0px">
                <a href="#!" id="this-month" class="text-green"><b>Tháng này</b></a>
              </div>
              <div class="col-auto" style="margin: 5px 0px">
                <a href="#!" id="last-month" class="text-teal"><b>Tháng trước</b></a>
              </div>
              <div class="col-auto" style="margin: 5px 0px">
                <a href="#!" id="this-year" class="text-purple"><b>Năm nay</b></a>
              </div>
              <div class="col-auto" style="margin: 5px 0px;">
                <a href="#!" id="last-year" class="text-white"><b>Năm trước</b></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row mt-3">
  <div class="col-xl-12">
    <div class="card shadow">
      <div class="card-header border-0">
        <div class="row align-items-center">
          <div class="col-8">
            <h3 class="mb-0">Theo thời gian</h3>
          </div>
        </div>
      </div>
      <div class="card-body" id="bar-chart-wrapper">
        <canvas id="bar-chart" height="450" width="100%"></canvas>
      </div>
    </div>
  </div>
</div>
<div class="row mt-3">
  <div class="col-xl-12">
    <div class="card shadow">
      <div class="card-header border-0">
        <div class="row align-items-center">
          <div class="col-8">
            <h3 class="mb-0">Theo danh mục giao dịch</h3>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h2 class="text-center">Thu nhập</h2>
            <div id="pie-chart-add-wrapper" style="height: 370px; width: 100%">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h2 class="text-center">Chi tiêu</h2>
            <div id="pie-chart-sub-wrapper" style="height: 370px; width: 100%">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="ajax-script">
  
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="{{url('assets/js/plugins/moment.js')}}"></script>
<script>
  $(function() {
    gradientBarChartConfiguration = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },

    tooltips: {
      backgroundColor: '#f5f5f5',
      titleFontColor: '#333',
      bodyFontColor: '#666',
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest",
      callbacks:{
        label: function(item) {
          return money(item.yLabel) + ' VNĐ';
        }
      }
    },
    responsive: true,
    scales: {
      yAxes: [{

        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.1)',
          zeroLineColor: "transparent",
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          padding: 20,
          fontColor: "#9e9e9e",
          callback: function(value) {
            return money(value) +" VNĐ";
          }
        }
      }],

      xAxes: [{

        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.1)',
          zeroLineColor: "transparent",
        },
        ticks: {
          padding: 20,
          fontColor: "#9e9e9e",
        }
      }]
    }
  };

    var from_date = $('#from_date');
    var to_date = $('#to_date');
    var daysInMonth = moment().daysInMonth();
    var daysInYear = moment().isLeapYear() ? 366 : 365;
    var queryBtn = $('#query');
    queryBtn.on('click',function() {
      $.ajax({
        url: "{{ url('statistic/getChart') }}", 
        method:"POST", 
        data:{
          startDate : from_date.val(),
          endDate : to_date.val(),
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){ 
          $('#bar-chart-wrapper').html('<canvas id="bar-chart" height="450" width="100%"></canvas>');
          $('#ajax-script').html(data);
        }
      });
    })
    //short date button
    $('#this-week').on('click',function () {
      from_date.val(moment().startOf('week').add(1, 'day').format('YYYY-MM-DD')+" 00:00:00");
      to_date.val(moment().endOf('week').add(1, 'day').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    $('#last-week').on('click',function () {
      from_date.val(moment().startOf('week').subtract(6, 'day').format('YYYY-MM-DD')+" 00:00:00");
      to_date.val(moment().endOf('week').subtract(6, 'day').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    $('#this-month').on('click',function () {
      from_date.val(moment().startOf('month').format('YYYY-MM-DD')+" 00:00:00");
      to_date.val(moment().endOf('month').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    $('#last-month').on('click',function () {
      from_date.val(moment().startOf('month').subtract(daysInMonth, 'day').format('YYYY-MM-DD') + " 00:00:00");
      to_date.val(moment().endOf('month').subtract(daysInMonth, 'day').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    $('#this-year').on('click',function () {
      from_date.val(moment().startOf('year').format('YYYY-MM-DD') + " 00:00:00");
      to_date.val(moment().endOf('year').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    $('#last-year').on('click',function () {
      from_date.val(moment().startOf('year').subtract(daysInYear, 'day').format('YYYY-MM-DD') + " 00:00:00");
      to_date.val(moment().endOf('year').subtract(daysInYear, 'day').format('YYYY-MM-DD') + " 23:59:59");
      queryBtn.click();
    })
    //Date time picker init
    $('.datetimepicker').datetimepicker({
      i18n:{
        en:{
          months:[
          'Tháng 1','Tháng 2','Tháng 3','Tháng 4',
          'Tháng 5','Tháng 6','Tháng 7','Tháng 8',
          'Tháng 9','Tháng 10','Tháng 11','Tháng 12',
          ],
          dayOfWeek:[
          "T2.", "T3", "T4", "T5", 
          "T6", "T7", "CN",
          ]
        }
      },
      mask:true,
      maxDate:'+1970/01/02',
      format: 'Y-m-d H:i:s',
      formatTime:'H:i:s',
      formatDate:'d.m.Y',
    });
  })
  function checkNum(num) {
    if(num<10) return '0'+num;
    return num;
  }
</script>
@endsection
@extends('Management.Widget.master')
@section('title','Trang chủ')
@section('banner','Trang chủ')
@section("content")
<?php use App\Http\Controllers\Controller; ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-xl-3 col-lg-3">
    <div class="card card-stats mb-4 mb-xl-0">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-white mb-0">Tổng tiền thu</h5>
          </div>
          <div class="col-auto">
            <i class="fa fa-rocket   text-success"></i>
          </div>
        </div>
        <span class="h3 font-weight-bold text-success mb-0">{{ number_format(Controller::getTransactionAmountFromType(1)) }} VNĐ</span>
        <p class="mt-3 mb-0 text-white text-sm">
          <span class="text-nowrap">Thu nhập từ trước tới nay</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3">
    <div class="card card-stats mb-4 mb-xl-0">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-white mb-0">Tổng tiền chi</h5>
          </div>
          <div class="col-auto">
            <i class="fas fa-cart-arrow-down text-orange"></i>
          </div>
        </div>
        <span class="h3 font-weight-bold text-danger mb-0">{{ number_format(Controller::getTransactionAmountFromType(0)) }} VNĐ</span>
        <p class="mt-3 mb-0 text-white text-sm">
          <span class="text-nowrap">Chi tiêu từ trước tới nay</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3">
    <div class="card card-stats mb-4 mb-xl-0">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-white mb-0">Tiết kiệm</h5>
          </div>
          <div class="col-auto">
            <i class="fas fa-usd text-yellow"></i>
          </div>
        </div>
        <span class="h3 font-weight-bold text-yellow mb-0">{{ number_format(Controller::calTotalMoneyOfType([3])) }} VNĐ</span>
        <p class="mt-3 mb-0 text-white text-sm">
          <span class="text-nowrap">Tổng số tiền trong ví tiết kiệm</span>
        </p>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-3">
    <div class="card card-stats mb-4 mb-xl-0">
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h5 class="card-title text-uppercase text-white mb-0">Tổng</h5>
          </div>
          <div class="col-auto">
            <i class="fas fa-university text-teal"></i>
          </div>
        </div>
        <span class="h3 font-weight-bold text-teal mb-0">{{ number_format(Controller::calTotalMoneyOfType()) }} VNĐ</span>
        <p class="mt-3 mb-0 text-white text-sm">
          <span class="text-nowrap">Số dư hiện có</span>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="row mt-4">
  <div class="col-xl-8 mb-5 mb-xl-0">
    <div class="card card-chart">
      <div class="card-header ">
        <div class="row">
          <div class="col-sm-6 text-left">
            <h5 class="card-category">Tổng quát</h5>
            <h2 class="card-title">Thu chi</h2>
          </div>
          <div class="col-sm-6">
            <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
              <label class="btn btn-sm btn-primary btn-simple active" id="0">
                <input type="radio" name="options" checked="">
                <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Tuần này</span>
                <span class="d-block d-sm-none">
                  <i class="tim-icons icon-single-02"></i>
                </span>
              </label>
              <label class="btn btn-sm btn-primary btn-simple" id="1">
                <input type="radio" class="d-none d-sm-none" name="options">
                <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Tuần trước</span>
                <span class="d-block d-sm-none">
                  <i class="tim-icons icon-gift-2"></i>
                </span>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="chart-area">
          <canvas id="chartBig1" ></canvas>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-4">
    <div class="card shadow">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="panel panel-default ">
            <div class="card-header bg-transparent custom-easypie-header">
              <div class="row align-items-center">
                <div class="col" style="padding: 0">
                  <h6 class="text-uppercase text-white ls-1 mb-1 text-center">Thu nhập tháng</h6>
                </div>
              </div>
            </div>
            <div class="panel-body easypiechart-panel custom-easypiechart-panel">
              <div class="easypiechart" id="easypiechart-teal" data-percent="{{$percent['add']}}" data-toggle="tooltip" data-placement="top" title="{{number_format($totalThisMonth['add'],)}} VNĐ"><span class="percent">{{$percent['add']}}%</span></div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="panel panel-default ">
            <div class="card-header bg-transparent custom-easypie-header">
              <div class="row align-items-center">
                <div class="col" style="padding: 0">
                  <h6 class="text-uppercase text-white ls-1 mb-1 text-center">Chi tiêu tháng</h6>
                </div>
              </div>
            </div>
            <div class="panel-body easypiechart-panel custom-easypiechart-panel">
              <div class="easypiechart" id="easypiechart-orange" data-percent="{{$percent['subtract']}}" data-toggle="tooltip" data-placement="top" title="{{number_format($totalThisMonth['subtract'],0)}} VNĐ"><span class="percent">{{$percent['subtract']}}%</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card shadow" style=" margin-top: .75rem">
      <div class="card-header bg-transparent">
        <div class="row align-items-center">
          <div class="col">
            <h6 class="text-uppercase text-white ls-1 mb-1">số lượt giao dịch trong tuần này</h6>
          </div>
        </div>
      </div>
      <div class="card-body" style="padding: 1rem">
        <!-- Chart -->
        <div class="chart" style="height: 150px !important;">
          <div class="chartjs-size-monitor">
            <div class="chartjs-size-monitor-expand">
              <div class="">

              </div>
            </div>
            <div class="chartjs-size-monitor-shrink">
              <div class="">

              </div>
            </div>
          </div>
          <div class="chart-area">
            <canvas id="CountryChart"></canvas>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xl-8 mb-5 mb-xl-0">
    <div class="card ">
      <div class="card-header border-0">
        <div class="row align-items-center">
          <div class="col">
            <h3 class="mb-0">Giao dịch gần đây</h3>
          </div>
          <div class="col text-right">
            <a href="{{url('/transaction')}}" class="btn btn-sm btn-primary">Xem thêm</a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive ps" style="padding: 0 !important">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th></th>
                <th class="text-center text-white">Loại giao dịch</th>
                <th class="text-center text-white">Thời gian</th>
                <th class="text-center text-white">Số tiền</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $tr)
              @if($tr->TransactionType->type == 1)
              <tr >
                <th class="text-center" scope="row">
                  <img src="{{url($tr->TransactionType->icon)}}" width="25" height="25" alt="">
                </th>
                <td class="text-center text-success">
                  {{$tr->TransactionType->name}}
                </td>
                <td class="text-center text-success">
                  {{$tr->transaction_date}}
                </td>
                <td class="text-center text-success">
                  <b>+</b>{{number_format($tr->amount),2}} VNĐ
                </td>
              </tr>
              @else
              <tr>
               <th class="text-center" scope="row">
                <img src="{{url($tr->TransactionType->icon)}}" width="25" height="25" alt="">
              </th>
              <td class="text-center text-danger">
                {{$tr->TransactionType->name}}
              </td>
              <td class="text-center text-danger">
                {{$tr->transaction_date}}
              </td>
              <td class="text-center text-danger">
                <b>-</b>{{number_format($tr->amount),2}} VNĐ
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="col-xl-4">
  <div class="card shadow">
    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h6 class="text-uppercase text-light ls-1 mb-1">Danh sách những việc cần làm</h6>
          <h4 class="mb-0">Ghi chú</h4>
        </div>
      </div>
    </div>
    <div class="panel-body-custom scroll-bar">
      <ul class="todo-list">

      </ul>
    </div>
    <div class="panel-footer mt-2">
      <div class="input-group">
        <input id="btn-input" type="text" class="form-control input-md" placeholder="Ghi chú mới.."><span class="input-group-btn">
          <button class="btn btn-primary btn-md" id="btn-todo" style="margin-top: 0px">Thêm</button>
        </span></div>
      </div>
    </div>
  </div>
</div>
<script src="{{url('./assets/js/plugins/easypiechart.js')}}"></script>
<script>
  gradientBarChartConfiguration = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },

    tooltips: {
      backgroundColor: '#f5f5f5',
      titleFontColor: '#333',
      bodyFontColor: '#666',
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    responsive: true,
    scales: {
      yAxes: [{
        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.1)',
          zeroLineColor: "transparent",
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          padding: 20,
          fontColor: "#9e9e9e"
        }
      }],

      xAxes: [{
        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.1)',
          zeroLineColor: "transparent",
        },
        ticks: {
          padding: 20,
          fontColor: "#9e9e9e"
        }
      }]
    }
  };
  gradientChartOptionsConfigurationWithTooltipPurple = {
    maintainAspectRatio: false,
    legend: {
      display: false,
    },

    tooltips: {
      backgroundColor: '#f5f5f5',
      titleFontColor: '#333',
      bodyFontColor: '#666',
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest",
      callbacks:{
        label: function(item) {
          return money(item.yLabel) + ' VNĐ';
        }
      }
    },
    responsive: true,
    scales: {
      yAxes: [{
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.0)',
          zeroLineColor: "transparent",
        },
        ticks: {
          fontSize: 14,
          suggestedMin: 60,
          suggestedMax: 125,
          padding: 20,
          fontColor: "#9a9a9a",
          callback: function(value) {
            return money(value) +" VNĐ";
          }
        }
      }],

      xAxes: [{
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: 'rgba(225,78,202,0.1)',
          zeroLineColor: "transparent",
        },
        ticks: {
          padding: 20,
          fontColor: "#9a9a9a",
          fontSize: 15
        }
      }]
    }
  };
  $(function() {
    $('#btn-input').on('keyup',function(event) {
      if(event.keyCode == 13){
        $('#btn-todo').click();
      }
    })
    //add todo
    $('#btn-todo').on('click',function() {
      var desc = $('#btn-input').val();
      if(desc == ""){return;}
      $.ajax({
        url:"{{ url('todo/add') }}", 
        method:"POST", 
        data:{
          description: desc,
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){ 
          $('#btn-input').val("");
          $('.todo-list').html(data);
        }
      });
      setTimeout(function() {
            getTodoNoti();
          },1000)
    });
    //get todo
    $.ajax({
      url:"{{ url('todo/get') }}", 
      method:"POST", 
      data:{
        _token: $('meta[name="csrf-token"]').attr('content')
      },
      success:function(data){ 
       $('.todo-list').html(data);
     }
   });
    var chart_labels = ['T2','T3','T4','T5','T6','T7','CN'];
    var ctx = document.getElementById("CountryChart").getContext("2d");

    var gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
    gradientStroke.addColorStop(0, 'rgba(29,140,248,0)'); //blue colors


    var myBarChart = new Chart(ctx, {
      type: 'bar',
      responsive: true,
      legend: {
        display: false
      },
      data: {
        labels: ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'],
        datasets: [{
          label: "Số lần ",
          fill: true,
          backgroundColor: gradientStroke,
          hoverBackgroundColor: gradientStroke,
          borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: {{$count['total']}},
        }]
      },
      options: gradientBarChartConfiguration
    });


    var chart_data_add = {{$datasets['add']}};
    var chart_data_sub = {{$datasets['subtract']}};

    var chart_data_add_lastWeek = {{$lastweek_datasets['add']}};
    var chart_data_sub_lastWeek = {{$lastweek_datasets['subtract']}};


    var ctx = document.getElementById("chartBig1").getContext('2d');

    var gradientStrokeGreen = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStrokeGreen.addColorStop(1, 'rgba(66,134,121,0.15)');
    gradientStrokeGreen.addColorStop(0.4, 'rgba(66,134,121,0.0)'); //green colors
    gradientStrokeGreen.addColorStop(0, 'rgba(66,134,121,0)'); 

    var gradientStrokeOrange = ctx.createLinearGradient(0, 230, 0, 50);
    gradientStrokeOrange.addColorStop(1, 'rgba(255,159,137,0.2)');
    gradientStrokeOrange.addColorStop(0.4, 'rgba(255,159,137,0.1)');
    gradientStrokeOrange.addColorStop(0, 'rgba(255,159,137,0.01)'); //orange colors
    var config = {
      type: 'line',
      data: {
        labels: chart_labels,
        datasets: [{
          label: "Thu nhập",
          fill: true,
          backgroundColor: gradientStrokeGreen,
          borderColor: '#2DCE89',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#2DCE89',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#d346b1',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: {{$datasets['add']}},
        },
        {
          label: "Chi tiêu",
          fill: true,
          backgroundColor: gradientStrokeOrange,
          borderColor: '#FF9F89',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#FF9F89',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#FF9F89',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: {{$datasets['subtract']}},
        }]
      },
      options: gradientChartOptionsConfigurationWithTooltipPurple
    };

    var myChartData = new Chart(ctx, config);
    $("#0").click(function() {
      var data = myChartData.config.data;
      data.datasets[0].data = chart_data_add;
      data.datasets[1].data = chart_data_sub;
      data.labels = chart_labels;
      myChartData.update();
    });
    $("#1").click(function() {
      var data = myChartData.config.data;
      data.datasets[0].data = chart_data_add_lastWeek;
      data.datasets[1].data = chart_data_sub_lastWeek;
      data.labels = chart_labels;
      myChartData.update();
    });
  })
</script>
@endsection
@extends('../Management.Widget.master')
@section('title','Chọn ví')
@section('banner','Ví thực hiện giao dịch')
@section('content')
<a id="btnSearchBar" class="btn btn-primary">
	<center data-toggle="tooltip" data-placement="left" title="Tìm kiếm"><i id="searchIcon" class="fas fa-search"></i></center>
</a>
<input type="text" id="search" autocomplete="off" placeholder="Nhập tên ví của bạn...">
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card bg shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Danh sách</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row" id="parent">
					@foreach($wallets as $wallet)
					<div class="box col-xl-12 col-lg-12" style="margin-bottom: 15px;">
						<a class="link-walletType" href="{{url('transaction/choosewallet')}}/{{$wallet->id}}">
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<span class="name h2 text-success font-weight-bold mb-0">{{$wallet->name}}</span>
										</div>
										<div class="col-auto">
											<img src="{{url($wallet->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
										</div>
									</div>
									<p class="mt-3 mb-0 text-yellow ">
										<span class="text-nowrap">Số dư: {{number_format($wallet->amount,2)}} VNĐ</span>
									</p>
									<p class="mt-3 mb-0 text-muted text-sm">
										<span class="text-nowrap">
											@if($wallet->description)
											{{$wallet->description}}
											@else
											Chưa có bất kì mô tả nào..
											@endif
										</span>
									</p>
								</div>
							</div>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
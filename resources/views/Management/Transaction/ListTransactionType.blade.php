@extends('Management.Widget.master')
@section('banner','Danh mục giao dịch')
@section('title','Danh sách kiểu giao dịch đã thêm')
@section('content')
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0 text-white">Danh sách loại giao dịch</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row" id="parent">
					@foreach($type as $t)
					<div class="box col-xl-12 col-lg-12" style="margin-bottom: 15px;">
						<a class="link-walletType" href="#!">
							<div class="card card-stats mb-4 mb-xl-0">
								<div class="card-body">
									<div class="row">
										<div class="col">
											<span class="name h2 text-danger font-weight-bold mb-0">{{$t->name}}</span>
										</div>
										<div class="col-auto">
											<img src="{{url($t->icon)}}" width="50px" height="50px" alt="">
										</div>
									</div>
									<p class="mt-3 mb-0 text-white ">
										<span class="text-nowrap">Có {{count($t->Transactions()->get())}} giao dịch đã được thực hiện với loại này</span>
									</p>
									<p class="mt-3 mb-0 text-purple text-right">
										<button class="btn btn-danger btn-sm" onclick="window.location.href = '{{url("transaction/type/get/")}}/{{$t->id}}'">Sửa</button>
										<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirm-delete">Xóa</button>
									</p>
								</div>
							</div>

						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-4">
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
      <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">

          <div class="modal-header">
            <h6 class="modal-title text-white" id="modal-title-notification ">Chú ý!</h6>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="py-3 text-center">
              <h4 class="heading mt-4">Hành động này sẽ <b>xóa</b> kiểu giao dịch {{strtolower($t->name)}} này! <br> (Bao gồm cả các giao dịch có liên quan)!</h4>
            </div>
          </div>
          <div class="modal-footer">
          	<button type="button" onclick="window.location.href = '{{url('/transaction/type/delete')}}/{{$t->id}}' " class=" btn btn-warning btn-danger-custom" style="background: white; color: #f56036">Xác nhận xóa</button>
            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Đóng</button> 
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
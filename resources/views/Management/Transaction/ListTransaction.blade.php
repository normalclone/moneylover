@extends('Management.Widget.master')
@section('banner','Lịch sử')
@section('title','Lịch sử giao dịch')
@section('content')
<div class="col-md-4">
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
      <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">

          <div class="modal-header">
            <h6 class="modal-title text-white" id="modal-title-notification">Chú ý!</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="py-3 text-center">
              <i class="ni ni-bell-55 ni-3x"></i>
              <h4 class="heading mt-4">Hành động này sẽ xóa vĩnh viễn giao dịch này! <br> Khoản tiền trong giao dịch cũng sẽ được hoàn lại!</h4>
            </div>
          </div>
          <div class="modal-footer">
          	<button type="button" id="confirm-delete-transaction" onclick="" class="btn btn-danger btn-custom-danger" style="background: white; color: #f56036" >Xác nhận xóa</button>
            <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Đóng</button> 
          </div>

        </div>
      </div>
    </div>
  </div>
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Lịch sử giao dịch</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="col-md-12">
					<div class="panel panel-default ">
						@foreach($groupedTransactions as $day)
						<div class="panel-heading text-white">
							Ngày {{$day['date_text']}}
						</div>
						<div class="panel-body timeline-container">
							<ul class="timeline">
								@foreach($day['transactions'] as $tr)
								@if($tr['type']==0)
								<li>
									<div class="timeline-badge no-color">
										<img src="{{url($tr['icon'])}}" width="100%" height="100%" alt="" data-toggle="tooltip" data-placement="right" title="{{$tr['transaction_name']}}">
									</div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h4 class="timeline-title text-danger">{{number_format($tr['amount'])}} VNĐ</h4>
										</div>
										<div class="timeline-body">
											<p class="text-sm text-warning">{{$tr['description']}}</p>
											<p class="text-sm text-yellow">{{$tr['oweAmount']}}</p>
											<p class="mt-3 mb-0 text-muted text-sm">Chi từ {{strtolower($tr['wallet_type'])}} ({{$tr['wallet']}}) lúc: {{explode(' ',$tr['transaction_date'])[1]}}</p>
											<div class="text-right">
												<button class="btn btn-sm btn-primary" onclick="window.location.href = '{{url('transaction/get/')}}/{{$tr['id']}}'">Chi tiết</button>
												<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#confirm-delete" data="{{$tr['id']}}">Xóa</button>
											</div>
											
										</div>
									</div>
								</li>
								@else
								<li>
									<div class="timeline-badge no-color">
										<img src="{{url($tr['icon'])}}" width="100%" height="100%" alt="" data-toggle="tooltip" data-placement="right" title="{{$tr['transaction_name']}}">
									</div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h4 class="timeline-title text-teal">{{number_format($tr['amount'])}} VNĐ</h4>
										</div>
										<div class="timeline-body">
											<p class="text-sm text-success">{{$tr['description']}}</p>
											<p class="text-sm text-danger">{{$tr['oweAmount']}}</p>
											<p class="mt-3 mb-0 text-muted text-sm">Thu vào {{strtolower($tr['wallet_type'])}} ({{$tr['wallet']}}) lúc: {{explode(' ',$tr['transaction_date'])[1]}}</p>
											<div class="text-right">
												<button class="btn btn-sm btn-primary" onclick="window.location.href = '{{url('transaction/get/')}}/{{$tr['id']}}'">Chi tiết</button>
												<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#confirm-delete"  data="{{$tr['id']}}">Xóa</button>
											</div>
										</div>
									</div>
								</li>
								@endif
								@endforeach
							</ul>
						</div>
						@endforeach
					</div>
					<div class="clearfix">{{$groupedTransactions->links()}}</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('button[data-toggle="modal"]').on('click',function() {
		var href = "window.location.href = '{{url('transaction/delete')}}/"+$(this).attr('data')+"'";
		$('#confirm-delete-transaction').attr('onclick',href);
	})
</script>
@endsection
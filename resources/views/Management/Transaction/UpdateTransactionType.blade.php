@extends('Management.Widget.master')
@section('banner','Sửa kiểu giao dịch')
@section('title','Thiết lập')
@section('content')
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0">Thiết lập kiểu giao dịch</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<form method="post" action="{{url('transaction/updatetype')}}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="type_id" value="{{$type->id}}">
					<h6 class="heading-small text-muted mb-4">Chung</h6>
					<div class="pl-lg-4">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group focused">
									<label class="form-control-label" for="input-username">Tên kiểu giao dịch</label>
									<input type="text" autocomplete="off" placeholder="Tiền gì gì đó..." name="nameType" class="form-control form-control-alternative" value="{{$type->name}}">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="form-control-label" for="input-email">Giao dịch cha</label>
									<select name="parent_id" class="form-control form-control-alternative scroll-bar">
										<option value="0" class="text-white" >-------</option>
										@if($type->type == 0)
										@foreach($types as $t)
										@if($type->parent_id == $t->id)
										<option type="0" class="text-danger" value="{{$t->id}}" selected="selected">{{$t->name}}</option>	
										@else
										<option type="0" class="text-danger" value="{{$t->id}}">{{$t->name}}</option>
										@endif
										@endforeach
										@endif

										@if($type->type == 1)
										@foreach($types as $t)
										@if($type->parent_id == $t->id)
										<option type="1" class="text-success" value="{{$t->id}}" selected="selected">{{$t->name}}</option>	
										@else
										<option type="1" class="text-success" value="{{$t->id}}">{{$t->name}}</option>
										@endif
										@endforeach
										@endif
									</select>
									<label class="form-control-label text-xs" for="">(Loại giao dịch không thể bị thay đổi)</label>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="form-control-label" for="avatar">Ảnh minh họa</label>
								<div class="form-group focused">
									<img src="{{url($type->icon)}}" alt="" height="75" width="75">
									<label for="avatar">Tải lên ảnh minh họa của kiểu giao dịch</label>
									<input type="file" class="form-control-file btn btn-primary" style="width: 75%; font-size: .8rem" name="icon" readonly="">
									<br>
									<label for="avatar">hoặc nhập đường dẫn (Nếu bạn nhập cả 2, ảnh tải lên sẽ được ưu tiên)</label>
									<input type="text" class="form-control" name="icon_text" autocomplete="off" placeholder="Đường dẫn ảnh sẽ làm ảnh minh họa cho kiểu giao dịch mới.." value="{{$type->icon}}">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group focused">
									<label class="form-control-label" for="input-username">Loại</label>
									<select name="type" class="form-control form-control-alternative">
										@if($type->ParentTransactionType()->type == 0)
										<option value="0" class="text-danger" selected="selected">Giao dịch trừ tiền</option>
										@else
										<option value="1" class="text-success" selected="selected">Giao dịch cộng tiền</option>
										@endif
									</select>
								</div>
								<div class="form-group focused">
									<label class="form-control-label" for="input-username">Mật khẩu xác nhận</label>
									<input type="password" class="form-control form-control-alternative" placeholder="Nhập mật khẩu của bạn" value="" name="password">
								</div>
								<button type="submit" class="btn btn-lg btn-primary">Xác nhận</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		if($('select[name="parent_id"]').val() != 0){$('select[name="type"]').prop('disabled',true);}
	})
	$('select[name="parent_id"]').on('change',function() {
		var parent_id = $(this).val();
		var type;
		$(this).children('option').each(function() {
			if($(this).attr('value') == parent_id){ type = $(this).attr('type');}
		});
		$('select[name="type"]').children('option').each(function() {
			if(type == $(this).attr('value')){ 
				$(this).attr('selected',""); 
				$('select[name="type"]').prop('disabled',true);
			}else{
				$(this).removeAttr('selected'); 
			}
		})
		if(parent_id == 0){$('select[name="type"]').prop('disabled',false); }
	})
</script>
@endsection

@extends('../Management.Widget.master')
@section('title','Thông tin giao dịch')
@section('banner','Giao dịch')
@section('content')


<!-- Modal -->
<div class="modal fade" id="getType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header" >
				<h5 class="modal-title text-white" id="exampleModalLabel">Danh mục kiểu giao dịch</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body scroll-bar" id="choose-type-div">
				<ul class="navbar-nav">
					<li class="nav-item nav-item">
						<a class="nav-link text-danger" data-toggle="collapse" href="#subtractType">
							&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Khoản chi
						</a>
						<ul class="collapse" id="subtractType">
							@foreach($data['type'] as $type)
							@if($type['type']==0)
							<li class="nav-item nav-item" style="display: block;">
								<div class="row">
									<div class="col-9">
										<a class="nav-link type-link text-danger" href="#!" add-or-sub="{{$type['type']}}" transaction-type="{{$type['id']}}" parent-name="{{$type['name']}}" type-name="{{$type['name']}}">
											<img src="{{$type['icon']}}" width="20" height="20" alt="">
											&nbsp; {{$type['name']}}&nbsp;

											@if(!empty($type['child']))
											<span class="badge badge-danger">{{count($type['child'])}}+</span>
											@endif
										</a>
									</div>
									@if(!empty($type['child']))
									<div class="col-2">
										<a class="nav-link" data-toggle="collapse" href="#sub-type-{{$type['id']}}">
											<span class="badge badge-danger">Xem Thêm</span>
										</a>
									</div>
									@endif
								</div>
							</a>
							@if(!empty($type['child']))
							<ul class="collapse" id="sub-type-{{$type['id']}}">
								@foreach($type['child'] as $child)
								<li class="nav-item "  style="display: block">
									<a class="nav-link type-link text-danger" href="#!" add-or-sub="{{$child['type']}}" transaction-type="{{$child['id']}}" parent-name="{{$child['parent']}}" type-name="{{$child['name']}}">
										<img src="{{$child['icon']}}" width="20" height="20" alt="">&nbsp; {{$child['name']}}
									</a>
								</li>
								@endforeach
							</ul>
						</li>
						@endif
						@endif
						@endforeach
					</ul>
				</li>
				<li class="nav-item nav-item">
					<a class="nav-link text-success" data-toggle="collapse" href="#addType">
						&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Khoản thu
					</a>
					<ul class="collapse" id="addType">
						@foreach($data['type'] as $type)
						@if($type['type']==1)
						<li class="nav-item nav-item" style="display: block;">
							<div class="row">
								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
									<a class="nav-link type-link text-success" href="#!" add-or-sub="{{$type['type']}}" transaction-type="{{$type['id']}}" parent-name="{{$type['name']}}" type-name="{{$type['name']}}">
										<img src="{{$type['icon']}}" width="20" height="20" alt="">&nbsp; {{$type['name']}}&nbsp;
										@if(!empty($type['child']))
										<span class="badge badge-success text-default">{{count($type['child'])}}+</span>
										@endif
									</a>
								</div>
								@if(!empty($type['child']))
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
									<a class="nav-link" data-toggle="collapse" href="#sub-type-{{$type['id']}}">
										<span class="badge badge-success text-default">Xem Thêm</span>
									</a>
								</div>
								@endif
							</div>
						</a>
						@if(!empty($type['child']))
						<ul class="collapse" id="sub-type-{{$type['id']}}">
							@foreach($type['child'] as $child)
							<li class="nav-item"  style="display: block">
								<a class="nav-link type-link text-success" href="#!" add-or-sub="{{$child['type']}}" transaction-type="{{$child['id']}}" parent-name="{{$child['parent']}}" type-name="{{$child['name']}}">
									<img src="{{$child['icon']}}" width="20" height="20" alt="">&nbsp; {{$child['name']}}
								</a>
							</li>
							@endforeach
						</ul>
					</li>
					@endif
					@endif
					@endforeach
				</ul>
			</li>
			<li class="nav-item nav-item">
				<a class="nav-link text-teal" href="{{url('transaction/addtype')}}">
					&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Tạo thêm kiểu giao dịch khác...
				</a>
			</li>
			<li class="nav-item nav-item">
				<a class="nav-link text-teal" href="{{url('transaction/type')}}">
					&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp; Danh sách những kiểu giao dịch đã thêm
				</a>
			</li>
		</ul>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-link  ml-auto text-primary" data-dismiss="modal">Đóng</button> 
	</div>
</div>
</div>
</div>

<a id="btnSearchBar" class="btn btn-warning" href="{{url('transaction/choosewallet')}}">
	<center data-toggle="tooltip" data-placement="left" title="Quay lại">
		<i class="fa fa-reply"></i>
	</center>
</a>
<div class="row">
	<div class="col-xl-12 order-xl-1">
		<div class="card shadow">
			<div class="card-header border-0">
				<div class="row align-items-center">
					<div class="col-8">
						<h3 class="mb-0 text-white">
							Điền thông tin giao dịch
						</h3>
					</div>
					<div class="col-4">
						<h3 class="mb-0 text-right">
							<button class="btn btn-md btn-primary" id="enable-editing">Sửa</button>
						</h3>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<form action="{{url('/transaction/update')}}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="transaction_id" value="{{$data['transaction']->id}}">
							<input type="hidden" name="type_id" value="{{$data['transaction']->type_id}}">
							<h6 class="heading-small text-muted mb-4">Giao dịch từ {{$data['transaction']->Wallet->name}}</h6>
							<div class="pl-lg-4">
								<div class="row">
									<div class="box col-xl-6 col-lg-6" style="margin-bottom: 15px;">
										<a class="link-walletType" href="{{url('wallet/get')}}/{{$data['transaction']->Wallet->id}}" >
											<div class="card card-stats mb-4 mb-xl-0" style="border: 1px solid #e14eca; border-radius: 0 !important">
												<div class="card-body">
													<div class="row">
														<div class="col">
															@if($data['transaction']->TransactionType->type==0)
															<span class="name text-danger h2 font-weight-bold mb-0" id="wallet-name">{{$data['transaction']->Wallet->name}}</span>
															@else
															<span class="name text-success h2 font-weight-bold mb-0" id="wallet-name">{{$data['transaction']->Wallet->name}}</span>
															@endif
														</div>
														<div class="col-auto">
															<img src="{{url($data['transaction']->Wallet->WalletType()->first()->icon)}}" width="50px" height="50px" alt="">
														</div>
													</div>
													<p class="mt-3 mb-0 text-yellow ">
														<span class="text-nowrap">Số dư: {{number_format($data['transaction']->Wallet->amount,2)}} VNĐ</span>
													</p>
													<p class="mt-3 mb-0 text-muted text-sm">
														<span class="text-nowrap">
															@if($data['transaction']->Wallet->description)
															{{$data['transaction']->Wallet->description}}
															@else
															Chưa có bất kì mô tả nào..
															@endif
														</span>
													</p>
												</div>
											</div>
										</a>
									</div>
									<div class="box col-xl-6 col-lg-6" style="margin-bottom: 15px;">
										<a class="link-walletType type" href="#!" id="{{ ($data['transaction']->TransactionType->ParentTransactionType()->type == 0) ? 'subtract' : 'add'}}">
											<div class="card card-stats mb-4 mb-xl-0" style="background-color: rgba(128, 128, 128, .05) !important">
												<div class="card-body">
													<div class="row">
														<div class="col">
															@if($data['transaction']->TransactionType->type==0)
															<span id="transaction-name" class="name h2 text-danger font-weight-bold mb-0">{{$data['transaction']->TransactionType->name}}</span>
															@else
															<span id="transaction-name" class="name h2 text-success  font-weight-bold mb-0">{{$data['transaction']->TransactionType->name}}</span>
															@endif
														</div>
														<div class="col-auto">
															<img src="{{url($data['transaction']->TransactionType->icon)}}" width="50px" height="50px" alt="">
														</div>
													</div>
													<p class="mt-3 mb-0 text-yellow ">
														<span class="text-nowrap group-name">Nhóm: {{$data['transaction']->TransactionType->ParentTransactionType()->name}}</span>
													</p>
													<p class="mt-3 mb-0 text-muted text-sm">
														Giao dịch 
														@if($data['transaction']->TransactionType->ParentTransactionType()->type == 0) 
														trừ tiền
														@else
														cộng tiền
														@endif
													</p>
												</div>
											</div>
										</a>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="form-control-label" for="">Số tiền </label>
											<input type="text" autocomplete="off" placeholder="VNĐ 1,000,000.00" name="amount" value="{{$data['transaction']->amount}}" class="form-control form-control-alternative money-format" readonly="">
											<label for="" class="money-text" style="margin: 5px 0 0 0"></label>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<label class="form-control-label" for="">Ghi chú </label>
											<textarea name="description" rows="4" class="form-control form-control-alternative" placeholder="Thêm một vài ghi chú..." readonly="">{{$data['transaction']->description}}</textarea>
										</div>
									</div>
									<div class="col-lg-12">
										<p class=" text-yellow">{{$data['oweAmount_text']}}</p>
										<br>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label class="form-control-label" for="">Ngày thực hiện</label>
											<input type="text" id="transaction_date" name="transaction_date" value="{{$data['transaction']->transaction_date}}" class="form-control form-control-alternative" readonly="">
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group focused">
											<br>
											<button type="button" class="btn btn-lg btn-primary" onclick="getCurDateTime()" style="margin-top: 5px"  disabled="true">Giờ hiện tại &nbsp; <i class="fa fa-clock-o" aria-hidden="true"></i></button>
											<button type="submit" class="btn btn-lg btn-primary" style="margin-top: 5px"  disabled="true">Xác nhận &nbsp; <i class="fa fa-gavel" aria-hidden="true"></i></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$('#enable-editing').on('click',function() {
			//enable choose type
			$('.type').attr('data-toggle','modal');
			$('.type').attr('data-target',"#getType");
			$('.type .card').removeAttr('style');
			$('.form-control').each(function() {
				$(this).removeAttr('readonly');
				$(this).prop('disabled',false);
			});

			$('.btn-lg').each(function() {
				$(this).prop('disabled', false);
			});

		})

		$('.type-link').on('click',function() {
			$('input[name="type_id"]').val($(this).attr('transaction-type'));
			$('.type .card .card-body .row .col .name').html($(this).attr('type-name'));
			$('.group-name').html("Nhóm: "+ $(this).attr('parent-name'))
			$('.type .card .card-body .row .col-auto img').attr('src',$(this).children("img").attr('src'));
			$('.group-name').attr('class','mt-3 mb-0 text-yellow group-name')
			if($(this).attr('add-or-sub') == 0){
				$('#wallet-name').attr('class','name text-danger h2 font-weight-bold mb-0');
				$('#transaction-name').attr('class','name text-danger h2 font-weight-bold mb-0');
				$('.type').attr('id','subtract');
				$('.type .card .card-body .text-sm').html("Giao dịch trừ tiền");
			}
			if($(this).attr('add-or-sub') == 1){
				$('#wallet-name').attr('class','name text-success h2 font-weight-bold mb-0');
				$('#transaction-name').attr('class','name text-success h2 font-weight-bold mb-0');
				$('.type').attr('id','add')
				$('.type .card .card-body .text-sm').html("Giao dịch cộng tiền");
			}
			$('button[data-dismiss="modal"]').click();
		})
		$('input[name="transaction_date"]').datetimepicker({
			i18n:{
				en:{
					months:[
					'Tháng 1','Tháng 2','Tháng 3','Tháng 4',
					'Tháng 5','Tháng 6','Tháng 7','Tháng 8',
					'Tháng 9','Tháng 10','Tháng 11','Tháng 12',
					],
					dayOfWeek:[
					"T2.", "T3", "T4", "T5", 
					"T6", "T7", "CN",
					]
				}
			},
			mask:true,
			maxDate:'+1970/01/02',
			format: 'Y-m-d H:i:s',
			formatTime:'H:i:s',
			formatDate:'d.m.Y',
		});
	})
	function checkNum(num) {
		if(num<10) return '0'+num;
		return num;
	}
	function getCurDateTime() {
		var cur = new Date();
		d = checkNum(cur.getDate());
		m = checkNum(cur.getMonth()+1);
		y = checkNum(cur.getFullYear());
		h = checkNum(cur.getHours());
		min = checkNum(cur.getMinutes());
		s = checkNum(cur.getSeconds());
		var time = y+'-'+m+'-'+d+' '+h+':'+min+':'+s;
		$('input[name="transaction_date"]').val(time);
	}
</script>
@endsection
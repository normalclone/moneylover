<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    @yield('title')
  </title>
  <!--     Fonts and icons     -->
  <link href="{{url('./assets/img/icons/bitcoin.png')}}" rel="icon" type="image/png">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="{{url('./assets/css/nucleo-icons.css')}}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{url('./assets/css/black-dashboard.css?v=1.0.0')}}" rel="stylesheet" />
  <link href="{{url('./assets/css/style.css')}}" rel="stylesheet" />
  <link href="{{url('./assets/css/jquery.datetimepicker.css')}}" rel="stylesheet" />
  <script src="{{ url('./assets/js/core/jquery.min.js') }}"></script>
</head>

<body class="">
  <div class="wrapper">
    <div class="sidebar">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
      -->
      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="javascript:void(0)" class="simple-text logo-mini">
            ML
          </a>
          <a href="javascript:void(0)" class="simple-text logo-normal">
            MONEY LOVER
          </a>
        </div>
        <ul class="nav">
          <li class="{{ (Request::is('/*') || Request::is('') ? 'active' : '') }}">
            <a href="{{url('/')}}">
              <i class="tim-icons icon-tv-2"></i>
              <p>Trang chủ</p>
            </a>
          </li>
          <li class="{{ (Request::is('statistic/*') || Request::is('statistic') ? 'active' : '') }}">
            <a data-toggle="collapse" href="#sub-item-1" aria-expanded="false">
              <i class="tim-icons icon-chart-bar-32"></i>
              <span class="nav-link-text">Thống kê</span>
              <b class="caret mt-1"></b>
            </a>

            <div class="collapse" id="sub-item-1">
              <ul class="nav pl-4">
                <li class="{{ (Request::is('statistic/trend/*') || Request::is('statistic/trend') ? 'active' : '') }}">
                  <a href="{{url('statistic/trend')}}">
                    <i class="tim-icons icon-light-3"></i>
                    <p>Xu hướng</p>
                  </a>
                </li>
                <li class="{{ (Request::is('statistic/report/*') || Request::is('statistic/report') ? 'active' : '') }}">
                  <a href="{{url('statistic/report')}}">
                    <i class="tim-icons icon-single-copy-04"></i>
                    <p>Báo cáo</p>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="{{ (Request::is('userinfo/*') || Request::is('userinfo') ? 'active' : '') }}">
            <a href="{{url('userinfo')}}">
              <i class="tim-icons icon-single-02"></i>
              <p>Thông tin người dùng</p>
            </a>
          </li>
          <li class="{{ (Request::is('wallet/*') || Request::is('wallet') ? 'active' : '') }}">
            <a data-toggle="collapse" href="#sub-item-2" aria-expanded="false">
              <i class="tim-icons icon-wallet-43"></i>
              <span class="nav-link-text">Ví của bạn</span>
              <b class="caret mt-1"></b>
            </a>

            <div class="collapse" id="sub-item-2">
              <ul class="nav pl-4">
                <li class="{{ (Request::is('wallet/choose') || Request::is('wallet/type/*') ? 'active' : '') }}">
                  <a href="{{url('/wallet/choose')}}">
                    <i class="tim-icons icon-simple-add"></i>
                    <p>Tạo ví mới</p>
                  </a>
                </li>
                <li class="{{ (Request::is('wallet') || Request::is('wallet/get/*') ? 'active' : '') }}">
                  <a href="{{url('/wallet/')}}">
                    <i class="tim-icons icon-paper"></i>
                    <p>Quản lý ví</p>
                  </a>
                </li>
                <li class="{{ (Request::is('wallet/transfer') ? 'active' : '') }}">
                  <a href="{{url('/wallet/transfer')}}">
                    <i class="tim-icons icon-send"></i>
                    <p>Chuyển tiền</p>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="{{ (Request::is('transaction/*') || Request::is('transaction') ? 'active' : '') }}">
            <a data-toggle="collapse" href="#sub-item-3" aria-expanded="false">
              <i class="tim-icons icon-coins"></i>
              <span class="nav-link-text">Giao dịch</span>
              <b class="caret mt-1"></b>
            </a>
            <div class="collapse" id="sub-item-3">
              <ul class="nav pl-4">
                <li class="{{ (Request::is('transaction/choosewallet') || Request::is('transaction/choosewallet/*') ? 'active' : '') }}">
                  <a href="{{url('/transaction/choosewallet')}}">
                    <i class="tim-icons icon-simple-add"></i>
                    <p>Thêm giao dịch</p>
                  </a>
                </li>
                <li class="{{ (Request::is('transaction') ? 'active' : '') }}">
                  <a href="{{url('transaction')}}">
                    <i class="tim-icons icon-notes"></i>
                    <p>Lịch sử giao dịch</p>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle d-inline">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="javascript:void(0)">@yield('banner')</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ml-auto">
              <li class="dropdown nav-item">
                <a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown">
                  <div class="notification1 d-none d-lg-block d-xl-block"></div>
                  <i class="tim-icons icon-sound-wave"></i>
                  <p class="d-lg-none">
                    Ghi chú
                  </p>
                </a>
                <ul class="dropdown-menu dropdown-menu-right dropdown-navbar" id="todo-noti">
                  
                </ul>
              </li>
              <li class="dropdown nav-item">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                  <div class="photo">
                    <img src="{{url(Auth::user()->avatar)}}" alt="Profile Photo">
                  </div>
                  <b class="caret d-none d-lg-block d-xl-block"></b>
                  <p class="d-lg-none">
                    NGƯỜI DÙNG
                  </p>
                </a>
                <ul class="dropdown-menu dropdown-navbar">
                  <li class="nav-link">
                    <a href="{{url('userinfo')}}" class="nav-item dropdown-item">Thông tin cá nhân</a>
                  </li>
                  <li class="nav-link">
                    <a href="http://light.moneylover.com/" class="nav-item dropdown-item">Light Mode</a>
                  </li>
                    <li class="nav-link"><a href="{{url('userinfo/change-password')}}" class="nav-item dropdown-item">Đổi mật khẩu</a></li>
                    <li class="dropdown-divider"></li>
                    <li class="nav-link"><a href="{{url('/logout')}}" class="nav-item dropdown-item">Đăng xuất</a></li>
                  </ul>
                </li>
                <li class="separator d-lg-none"></li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
         @section("content")
         @show
       </div>
       <footer class="footer">
        <div class="container-fluid">
          <ul class="nav">
            <li class="nav-item">
              <a href="{{url('docs')}}" class="nav-link">
                Documentation
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('https://facebook.com/normalclone')}}" class="nav-link">
                Facebook
              </a>
            </li>
            <li class="nav-item">
              <a href="javascript:void(0)" class="nav-link">
                Blog
              </a>
            </li>
          </ul>
          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script><b> Nguyễn Văn Phúc</b> with <b class="text-primary">Love</b></i>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
      <a href="#" data-toggle="dropdown">
        <i class="fa fa-cog fa-2x"> </i>
      </a>
      <ul class="dropdown-menu">
        <li class="adjustments-line text-center color-change">
          <span class="color-label">LIGHT MODE</span>
          <span class="badge light-badge mr-2" onclick="window.location.href = 'http://light.moneylover.com/'"></span>
          <span class="badge dark-badge ml-2" onclick="window.location.href = 'http://dark.moneylover.com/'"></span>
          <span class="color-label">DARK MODE</span>
        </li>
        <li class="adjustments-line text-center text-primary color-change"><a href="{{url('/')}}"><h3>Money Lover</h3></a></li>
        <li class="button-container">
          <a href="{{url('/transaction/choosewallet')}}" class="btn btn-primary btn-block btn-round">Thêm giao dịch</a>
          <a href="{{url('/wallet/transfer')}}" class="btn btn-primary btn-block btn-round">Chuyển tiền</a>
          <a href="{{url('/transaction')}}" class="btn btn-primary btn-block btn-round">Lịch sử giao dịch</a>
          <hr>
          <a href="{{url('/docs')}}" target="_blank" class="btn btn-default btn-block btn-round">
            Hướng dẫn sử dụng
          </a>
          <a href="https://demos.creative-tim.com/black-dashboard/docs/1.0/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block btn-round">
            Điều khoản chính sách
          </a>
        </li>
      </ul>
    </div>
  </div>
  <!--   Core JS Files   -->

  <script src="{{ url('./assets/js/core/popper.min.js') }}"></script>
  <script src="{{ url('./assets/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('./assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!-- Chart JS -->
  <script src="{{ url('./assets/js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('./assets/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('./assets/js/black-dashboard.js?v=1.0.0') }}"></script>
  <script src="{{ url('./assets/js/plugins/autoNumeric.js') }}"></script>
  <script src="{{ url('./assets/js/plugins/jquery.datetimepicker.full.min.js') }}"></script>
  <script src="{{ url('./assets/js/plugins/jquery.datetimepicker.min.js') }}"></script>
  <script src="{{url('./assets/js/searchFilter.js')}}"></script>
  <script>
  </script>
  @if ((Request::is('statistic/*') || Request::is('statistic')))
  <script>
    $(function() {
      $('#sub-item-1').attr('class','collapse show');
      $('a[href="#sub-item-1"]').attr('aria-expanded','true');
    })
  </script>
  @endif
  @if ((Request::is('wallet/*') || Request::is('wallet')))
  <script>
    $(function() {
      $('#sub-item-2').attr('class','collapse show');
      $('a[href="#sub-item-2"]').attr('aria-expanded','true');
    })
  </script>
  @endif
  @if ((Request::is('transaction/*') || Request::is('transaction')))
  <script>
    $(function() {
      $('#sub-item-3').attr('class','collapse show');
      $('a[href="#sub-item-3"]').attr('aria-expanded','true');
    })
  </script>
  @endif
  <script>
    $(function() {
      @if(!(Request::is('transaction/*') || Request::is('transaction')))
      $('a[data-toggle="collapse"]').on('click',function() {
        if($(this).attr('aria-expanded') == "false"){
        var href = $(this).attr('href');
        $('a[data-toggle="collapse"]').each(function() {
          if(href != $(this).attr('href') || $(this).attr('aria-expanded') == "true"){
            $(this).attr('aria-expanded','false');
            $($(this).attr('href')).attr('class','collapse');
          }
        })
      }
      })
      @endif
      getTodoNoti();
    })
    $(function() {
      @if ($success = Session::get('Success'))
      showNotification('top','center','success',"{{$success}}")
      @endif
      @if (count($errors) > 0)
      @foreach ($errors->all() as $error)
      showNotification('top','center','danger',"{{ $error }}")
      @endforeach
      @endif
    });
    function getTodoNoti() {
      $.ajax({
        url:"{{ url('todo/getNoti') }}", 
        method:"POST", 
        data:{
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
          if(data != '<li class="nav-link text-primary nav-item dropdown-item"><a href="javascript:void(0)" class="nav-item dropdown-item">Bạn không có ghi chú nào chưa hoàn thành!</a></li>'){
            $('.notification1').attr('class','notification d-none d-lg-block d-xl-block');
          }
         $('#todo-noti').html(data);
       }
     });
    }
    function showNotification(from, align, type,message, icon="tim-icons icon-bell-55") {
      $.notify({
        icon: icon,
        message: message
      }, {
        type: type,
        timer: 8000,
        placement: {
          from: from,
          align: align
        }
      });
    }
  </script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>
		Hướng dẫn sử dụng
	</title>
	<!--     Fonts and icons     -->
	<link href="{{url('./assets/img/icons/bitcoin.png')}}" rel="icon" type="image/png">
	<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<!-- Nucleo Icons -->
	<link href="{{url('./assets/css/nucleo-icons.css')}}" rel="stylesheet" />
	<!-- CSS Files -->
	<link href="{{url('./assets/css/black-dashboard.css?v=1.0.0')}}" rel="stylesheet" />
	<link href="{{url('./assets/css/style.css')}}" rel="stylesheet" />
	<script src="{{ url('./assets/js/core/jquery.min.js') }}"></script>
</head>

<body class="">
	<div class="wrapper">
		<div class="sidebar">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
    <div class="sidebar-wrapper">
    	<div class="logo">
    		<a href="javascript:void(0)" class="simple-text logo-mini">
    			ML
    		</a>
    		<a href="javascript:void(0)" class="simple-text logo-normal">
    			Tài liệu hướng dẫn
    		</a>
    	</div>
    	<ul class="nav">
    		<li class="{{ (Request::is('/*') || Request::is('') ? 'active' : '') }}">
    			<a href="#gioi-thieu">
    				<i class="fa fa-hand-peace-o" aria-hidden="true"></i>
    				<p>Giới thiệu</p>
    			</a>
    		</li>
    		<li class="{{ (Request::is('wallet/*') || Request::is('wallet') ? 'active' : '') }}">
    			<a data-toggle="collapse" href="#sub-item-2" aria-expanded="true">
    				<i class="tim-icons icon-wallet-43"></i>
    				<span class="nav-link-text">Ví của bạn</span>
    				<b class="caret mt-1"></b>
    			</a>

    			<div class="collapse" id="sub-item-2">
    				<ul class="nav pl-4">
    					<li class="{{ (Request::is('wallet/choose') || Request::is('wallet/type/*') ? 'active' : '') }}">
    						<a href="#tao-vi-moi">
    							<i class="tim-icons icon-simple-add"></i>
    							<p>Tạo ví mới</p>
    						</a>
    					</li>
    					<li class="{{ (Request::is('wallet') || Request::is('wallet/get/*') ? 'active' : '') }}">
    						<a href="#quan-ly-vi">
    							<i class="tim-icons icon-paper"></i>
    							<p>Quản lý ví</p>
    						</a>
    					</li>
    					<li class="{{ (Request::is('wallet/transfer') ? 'active' : '') }}">
    						<a href="#chuyen-tien">
    							<i class="tim-icons icon-send"></i>
    							<p>Chuyển tiền</p>
    						</a>
    					</li>
    				</ul>
    			</div>
    		</li>
    		<li class="{{ (Request::is('transaction/*') || Request::is('transaction') ? 'active' : '') }}">
    			<a data-toggle="collapse" href="#sub-item-3" aria-expanded="true">
    				<i class="tim-icons icon-coins"></i>
    				<span class="nav-link-text">Giao dịch</span>
    				<b class="caret mt-1"></b>
    			</a>
    			<div class="collapse" id="sub-item-3">
    				<ul class="nav pl-4">
    					<li class="{{ (Request::is('transaction/choosewallet') || Request::is('transaction/choosewallet/*') ? 'active' : '') }}">
    						<a href="#them-giao-dich">
    							<i class="tim-icons icon-simple-add"></i>
    							<p>Thêm giao dịch</p>
    						</a>
    					</li>
    					<li class="{{ (Request::is('transaction/choosewallet') || Request::is('transaction/choosewallet/*') ? 'active' : '') }}">
    						<a href="#them-kieu-giao-dich">
    							<i class="tim-icons icon-simple-add"></i>
    							<p>Thêm kiểu giao dịch</p>
    						</a>
    					</li>
    					<li class="{{ (Request::is('transaction') ? 'active' : '') }}">
    						<a href="#lich-su-giao-dich">
    							<i class="tim-icons icon-notes"></i>
    							<p>Lịch sử giao dịch</p>
    						</a>
    					</li>

    				</ul>
    			</div>
    		</li>
    		<li class="{{ (Request::is('statistic/*') || Request::is('statistic') ? 'active' : '') }}">
    			<a data-toggle="collapse" href="#sub-item-1" aria-expanded="true">
    				<i class="tim-icons icon-chart-bar-32"></i>
    				<span class="nav-link-text">Thống kê</span>
    				<b class="caret mt-1"></b>
    			</a>

    			<div class="collapse" id="sub-item-1">
    				<ul class="nav pl-4">
    					<li class="{{ (Request::is('statistic/trend/*') || Request::is('statistic/trend') ? 'active' : '') }}">
    						<a href="#xu-huong">
    							<i class="tim-icons icon-light-3"></i>
    							<p>Xu hướng</p>
    						</a>
    					</li>
    					<li class="{{ (Request::is('statistic/report/*') || Request::is('statistic/report') ? 'active' : '') }}">
    						<a href="#bao-cao">
    							<i class="tim-icons icon-single-copy-04"></i>
    							<p>Báo cáo</p>
    						</a>
    					</li>
    				</ul>
    			</div>
    		</li>
    		<li class="{{ (Request::is('/*') || Request::is('') ? 'active' : '') }}">
    			<a href="{{url('/')}}">
    				<i class="fa fa-reply" aria-hidden="true"></i>
    				<p>Quay lại trang chủ</p>
    			</a>
    		</li>
    	</ul>
    </div>
</div>
<div class="main-panel">
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
		<div class="container-fluid">
			<div class="navbar-wrapper">
				<div class="navbar-toggle d-inline">
					<button type="button" class="navbar-toggler">
						<span class="navbar-toggler-bar bar1"></span>
						<span class="navbar-toggler-bar bar2"></span>
						<span class="navbar-toggler-bar bar3"></span>
					</button>
				</div>
				<a class="navbar-brand" href="javascript:void(0)">Hướng dẫn sử dụng</a>
			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
				<span class="navbar-toggler-bar navbar-kebab"></span>
			</button>
			<div class="collapse navbar-collapse" id="navigation">
				<ul class="navbar-nav ml-auto">
					<li class="dropdown nav-item">
						<a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<div class="notification d-none d-lg-block d-xl-block"></div>
							<i class="tim-icons icon-sound-wave"></i>
							<p class="d-lg-none">
								Ghi chú
							</p>
						</a>
						<ul class="dropdown-menu dropdown-menu-right dropdown-navbar" id="todo-noti">

						</ul>
					</li>
					<li class="dropdown nav-item">
						<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<div class="photo">
								<img src="{{url(Auth::user()->avatar)}}" alt="Profile Photo">
							</div>
							<b class="caret d-none d-lg-block d-xl-block"></b>
							<p class="d-lg-none">
								Log out
							</p>
						</a>
						<ul class="dropdown-menu dropdown-navbar">
							<li class="nav-link">
								<a href="{{url('userinfo')}}" class="nav-item dropdown-item">Thông tin cá nhân</a>
							</li>
							<li class="nav-link">
								<a href="http://light.moneylover.com/" class="nav-item dropdown-item">Light Mode</a>
							</li>
							<li class="nav-link"><a href="{{url('userinfo/change-password')}}" class="nav-item dropdown-item">Đổi mật khẩu</a></li>
							<li class="dropdown-divider"></li>
							<li class="nav-link"><a href="{{url('/logout')}}" class="nav-item dropdown-item">Đăng xuất</a></li>
						</ul>
					</li>
					<li class="separator d-lg-none"></li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- End Navbar -->
	<div class="content">
		<div class="row">
			<div class="card card-stats mb-4 mb-xl-0">
				<div class="card-body">
					<div class="row">
						<div class="col-2" id="gioi-thieu">
							<h3 class="card-title text-uppercase text-white mb-0 mt-2">Giới thiệu</h3>
						</div>
						<div class="col-9">
							<div class="text-white mt-2 ml-3">
								&nbsp;&nbsp;<b class="text-primary">MONEYLOVER</b> là ứng dụng quản lí tài chính cá nhân trên nền Web được phát triển bởi Sinh viên<strong> Nguyễn Văn Phúc</strong> trong kì thực tập tại RIKKEISOFT. 
								<br>
								&nbsp;&nbsp;Trang web được phát triển với các chức năng tương tự và dựa trên ứng dụng <a href="https://moneylover.vn/">MoneyLover</a> trên CHplay cũng như những yêu cầu đề tài thực tập đưa ra.
							</div>
						</div>
					</div>
					<div class="dropdown-divider"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Những người phát triển</h4>
						</div>
						<div class="col-9">
							<div class="text-white mt-2 ml-3">
								- Cố vấn&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Anh Vũ Hoàng Chung (Lập trình viên tại RIKKEISOFT)
								<br>
								- Người phát triển&nbsp;&nbsp;: Nguyễn Văn Phúc (Sinh viên BKAP)
								<br>
							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="tao-vi-moi"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Tạo ví mới</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Bước 1</b> : Bạn chọn tạo ví mới từ menu <b>Ví của bạn</b> hoặc có thể trực tiếp click vào <a href="url{{'/wallet/choose'}}">đường dẫn này</a>.
								<br>
								<b>Bước 2</b> : Chọn 1 trong 3 loại ví có sẵn <br> <br>
								<center><img src="{{url('./assets/img/choosewallet.png')}}" alt="">
									<p class="text-danger">(Lưu ý: <strong>Ví tiết kiệm</strong> không thể dùng để tạo giao dịch)</p></center>
								<b>Bước 3</b> : Điền thông tin theo mẫu <br> <br>
								<center><img src="{{url('./assets/img/walletform.png')}}" alt=""></center>
								<br>
								<b>Bước 4</b> : Xác nhận mật khẩu và click vào nút Thêm <br><br>

							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="quan-ly-vi"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Quản lý ví</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Đầu tiên</b> bạn chọn <b>Quản lý ví</b> từ menu <b>Ví của bạn</b> hoặc có thể trực tiếp click vào <a href="url{{'/wallet/'}}">đường dẫn này</a>.
								<br>
								Giờ danh sách ví đã được hiện ra <br> <br>
								<center><img src="{{url('./assets/img/walletlist.png')}}" alt="">
									<p class="text-teal">(Mẹo: Nếu không tìm được ví mình muốn lấy thông tin, click vào icon hình kính lúp)</p></center>
									<br>
								<b>- Nếu bạn muốn sửa thông tin ví</b> : click vào nút sửa để bật chức năng sửa, sau đó sửa thông tin và xác nhận <br>
								<b>- Còn nếu bạn muốn xóa thông tin ví</b> : click vào nút xóa và ví kèm theo đó là mọi giao dịch của ví sẽ được xóa vĩnh viễn.
								<br> <br>
								<center><img src="{{url('./assets/img/wallet.png')}}" alt=""></center>
								<br> <br>
							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="chuyen-tien"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Chuyển tiền</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Bước 1</b> : Bạn chọn <b>Chuyển tiền</b> menu <b>Ví của bạn</b> hoặc có thể trực tiếp click vào <a href="url{{'/wallet/transfer'}}">đường dẫn này</a>.
								<br>
								<b>Bước 2</b> : Chọn 2 ví trong danh sách ví của bạn rồi <b>Click vào icon hình mũi tên</b>. <br> <br>
								<center><img src="{{url('./assets/img/transferlist.png')}}" alt="">
									<p class="text-danger">(Lưu ý: Bạn không thể thực hiện giao dịch nếu ví chuyển trùng ví nhận)</p>
									<p class="text-teal">(Mẹo: Bạn có thể lọc danh sách ví bằng cách click vào icon kính lúp)</p></center>
								<b>Bước 3</b> : Điền thông tin theo mẫu <br> <br>
								<center><img src="{{url('./assets/img/transferform.png')}}" alt=""></center>
								<br>
								<b>Bước 4</b> : Xác nhận mật khẩu và click Xác nhận <br><br>

							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="them-giao-dich"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Thêm giao dịch</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Bước 1</b> : Bạn chọn <b>Thêm giao dịch</b> menu <b>Giao dịch</b> hoặc có thể trực tiếp click vào <a href="url{{'transaction/choosewallet'}}">đường dẫn này</a>.
								<br>
								<b>Bước 2</b> : Chọn ví trong danh sách ví của bạn. <br> <br>
								<center><img src="{{url('./assets/img/transactionlistwallet.png')}}" alt="">
									<p class="text-teal">(Mẹo: Bạn có thể lọc danh sách ví bằng cách click vào icon kính lúp)</p></center>
									<b>Bước 3</b> : Chọn kiểu giao dịch sẽ thực hiện <br> <br>
								<center><img src="{{url('./assets/img/buttonchoosetype.png')}}" alt=""></center>
								<br>
								<b>Bước 4</b> : Điền thông tin theo mẫu <br> <br>
								<center><img src="{{url('./assets/img/transactionform.png')}}" alt="">
									<p class="text-teal">(Mẹo: Bạn có thể lấy giờ hiện tại làm giờ giao dịch bằng cách click vào nút Giờ hiện tại)</p></center>
								<br>
								<b>Bước 4.5</b> : Nếu đó là khoản vay/nợ bạn có thể click vào checkbox tạo ghi chú để đặt ghi chú cho khoản vay/nợ đó. <br>
								&nbsp;&nbsp;- Bạn có thể nhập lãi suất của khoản vay hoặc nợ theo định dạng [[??%/thời gian]], nếu không đặt thời gian, thời gian mặc định là <b>ngày</b>. <br>&nbsp;&nbsp;- Ví dụ: [[5%/ngày]], [[5%/tháng]], [[10%/year]] <br> <br>
								<center><img src="{{url('./assets/img/transactionform1.png')}}" alt=""></center>
								<br>
								<b>Bước 5</b> : Click thêm giao dịch <br><br>

							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="them-kieu-giao-dich"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Thêm kiểu giao dịch</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Trong lúc đang thêm giao dịch, nếu bạn không thấy loại giao dịch nào phù hợp, bạn có thể tạo loại giao dịch mới cho bản thân bằng cách click vào <b class="text-teal">Tạo thêm kiểu giao dịch khác...</b></b>
								<br>
								<br>
								<center>
									<img src="{{url('./assets/img/transactiontype.png')}}" alt="">
								</center><br> <br>
								<b>Và tiến hành thêm kiểu giao dịch mới</b><br>
								<center>
									<img src="{{url('./assets/img/transactiontypeform.png')}}" alt="">
								</center>
								<br><br>
								<b>Bạn cũng có thể quản các kiểu giao dịch của bản thân qua <b class="text-teal">Danh sách những kiểu giao dịch đã thêm </b></b><br>
								<center>
									<img src="{{url('./assets/img/transactiontypelist.png')}}" alt="">
									<p class="text-danger">(Lưu ý: Xóa kiểu giao dịch cũng đồng nghĩa với việc xóa tất cả giao dịch sử dụng kiểu giao dịch đó)</p>
								</center>
								<br> <br>
							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="lich-su-giao-dich"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Lịch sử giao dịch</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Đầu tiên</b> : Bạn chọn <b>Lịch sử giao dịch</b> menu <b>Giao dịch</b> hoặc có thể trực tiếp click vào <a href="url{{'transaction'}}">đường dẫn này</a>.
								<br>
								&nbsp;&nbsp;- Lịch sử giao dịch sẽ được hiển thị dưới dạng timeline của cứ 3 ngày một.
								<br>
								&nbsp;&nbsp;- Lịch sử giao dịch cũng sẽ cho phép bạn xem chi tiết các giao dịch cũng như sửa và xóa chúng.
								<br>
								<center><img src="{{url('./assets/img/history.png')}}" alt=""></center>
							<br>
							<b> Để có thể sửa thông tin giao dịch, bạn cũng cần phải kích hoạt chức năng sửa thông qua việc click vào chức năng <b class="text-primary">Sửa</b></b>
							<br> <br>
							<center><img src="{{url('./assets/img/updatetransaction.png')}}" alt=""></center>
							<br>
							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="xu-huong"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Xu hướng</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Xu hướng là công cụ truy vấn trực quan giúp người dùng đánh giá hiệu suất, tỉ lệ giữa tiêu dùng và thu nhập trong tháng, để từ đó có chi tiêu hợp lý hơn trong khoảng thời gian tiếp theo.</b>
								<br>
								<br>
								<b>Để sử dụng chức năng này, người dùng click vào Xu hướng trong menu Thống kê, sau đó chọn thời gian mà mình muốn rồi tiến hành truy vấn</b>
								<br>
								<br>
								<center><img src="{{url('./assets/img/trendquery.png')}}" alt="">
									<p class="text-teal">(Mẹo: Lựa chọn các phím tắt giúp bạn lấy thời gian truy vấn nhan hơn)</p>
								</center>
							<br>
							<br> <br>
							<center><img src="{{url('./assets/img/trendtimechart.png')}}" alt="">
								<p class="text-teal">Biểu đồ theo thời gian</p></center>
							<br>
							<center><img src="{{url('./assets/img/trendcatechart.png')}}" alt="">
								<p class="text-teal">Biểu đồ theo danh mục <br> (Mẹo: Bạn có thể download biểu đồ bằng cách dùng chức năng ở menu góc phải trên)</p></center>
							<br>
							</div>
						</div>
					</div>
					<div class="dropdown-divider" id="bao-cao"></div>
					<div class="row">
						<div class="col-2">
							<h4 class="card-title text-uppercase text-white mb-0 mt-2">Báo cáo</h4>
						</div>
						<div class="col-10">
							<div class="text-white mt-2 ml-3">
								<b>Báo cáo là công cụ theo dõi thu chi thông qua danh sách từ truy vấn được truyền vào</b>
								<br>
								<br>
								<b>Để sử dụng chức năng này, người dùng click vào Báo cáo trong menu Thống kê, sau đó chọn thời gian mà mình muốn rồi tiến hành truy vấn</b>
								<br>
								<br>
								<center><img src="{{url('./assets/img/reportquery.png')}}" alt="">
									<p class="text-teal">(Mẹo: Lựa chọn các phím tắt giúp bạn lấy thời gian truy vấn nhan hơn)</p>
								</center>
								<b>Điều khác biệt duy nhất trong truy vấn của báo cáo với truy vấn của xu hướng chính là bạn có thể truy vấn theo danh mục.</b>
								</center>
							<br>
							<br> 
							<center><img src="{{url('./assets/img/choosecate.png')}}" alt="">
									<p class="text-danger">(Lưu ý: Chọn các danh mục lớn cũng sẽ lấy các giao dịch của danh mục con phía trong nó)</p></center>
							<br>
							<b>Và kết quả...</b>
							<br>
							<br>
							<center><img src="{{url('./assets/img/reportrs.png')}}" alt="">
									<p class="text-teal">(Lưu ý: Bạn có thể tải về file excel)</p></center>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<footer class="footer">
        <div class="container-fluid">
          <ul class="nav">
            <li class="nav-item">
              <a href="{{url('docs')}}" class="nav-link">
                Documentation
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('https://facebook.com/normalclone')}}" class="nav-link">
                Facebook
              </a>
            </li>
            <li class="nav-item">
              <a href="javascript:void(0)" class="nav-link">
                Blog
              </a>
            </li>
          </ul>
          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script><b> Nguyễn Văn Phúc</b> with <b class="text-primary">Love</b></i>
          </div>
        </div>
      </footer>
</div>
</div>
<div class="fixed-plugin">
	<div class="dropdown show-dropdown">
		<a href="#" data-toggle="dropdown">
			<i class="fa fa-cog fa-2x"> </i>
		</a>
		<ul class="dropdown-menu">
			<li class="adjustments-line text-center color-change">
				<span class="color-label">LIGHT MODE</span>
				<span class="badge light-badge mr-2" onclick="window.location.href = 'http://light.moneylover.com/'"></span>
				<span class="badge dark-badge ml-2" onclick="window.location.href = 'http://dark.moneylover.com/'"></span>
				<span class="color-label">DARK MODE</span>
			</li>
			<li class="adjustments-line text-center text-primary color-change"><a href="{{url('/')}}"><h3>Money Lover</h3></a></li>
			<li class="button-container">
				<a href="{{url('/transaction/choosewallet')}}" class="btn btn-primary btn-block btn-round">Thêm giao dịch</a>
				<a href="{{url('/wallet/transfer')}}" class="btn btn-primary btn-block btn-round">Chuyển tiền</a>
				<a href="{{url('/transaction')}}" class="btn btn-primary btn-block btn-round">Lịch sử giao dịch</a>
				<hr>
				<a href="{{url('/docs')}}" target="_blank" class="btn btn-default btn-block btn-round">
					Hướng dẫn sử dụng
				</a>
				<a href="https://demos.creative-tim.com/black-dashboard/docs/1.0/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block btn-round">
					Điều khoản chính sách
				</a>
			</li>
		</ul>
	</div>
</div>
<!--   Core JS Files   -->

<script src="{{ url('./assets/js/core/popper.min.js') }}"></script>
<script src="{{ url('./assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ url('./assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ url('./assets/js/plugins/bootstrap-notify.js') }}"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ url('./assets/js/black-dashboard.js?v=1.0.0') }}"></script>
<script>
	$('a').click = function() {
        $(document).scrollTo($(this).attr('href'));
    }
</script>
</body>

</html>
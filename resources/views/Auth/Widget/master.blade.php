<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
  <link href="{{url('./assets/img/icons/bitcoin.png')}}" rel="icon" type="image/png">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/Auth/style.css') }}">
</head>
<body>
	<div class="bg-wave">
		<div class="wave-background">
			<svg class="wave-svg" width="100%" height="100%">
				<path class="dynamic-wave" d="" data-wave-color="#825ee4" data-wave-height="0.6" data-wave-bones="5" data-wave-speed="1"></path>
			</svg>
		</div>
	</div>
	@if (count($errors) > 0)
    <div id="myModal" class="modal fade" role="dialog">
  	<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<div class="modal-header">
        	<h4 class="modal-title">Thông báo</h4>
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
      	</div>
      	<div class="modal-body">
        	<div class="alert">
     			<ul>
      			@foreach ($errors->all() as $error)
      			 	<li>{{ $error }}</li>
      			@endforeach
     			</ul>
    		</div>
      	</div>
      	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
      	</div>
    </div>
  	</div>
	</div>
   	@endif
    @if ($wrongPassword = Session::get('wrongPassword'))
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Thông báo</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="alert">
          <ul>
            <li>{{$wrongPassword['message']}}</li>
          </ul>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        </div>
    </div>
    </div>
  </div>
    @endif
	<section class="form-box">
		@section('content')
        @show
    </section>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<script src="{{ asset('/js/Auth/Wave.js') }}"></script>
@if (count($errors) > 0 || $wrongPassword = Session::get('wrongPassword'))
<script>
	$(function() {
		$('#myModal').modal('show');
	})
</script>
@endif
</html>
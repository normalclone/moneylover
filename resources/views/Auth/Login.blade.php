@extends('Auth.widget.master')
@section('title','Đăng nhập')
@section('content')
		<svg viewbox="-10 0 140 20">
			<defs>
				<linearGradient id="gradient" x1="0" x2="0" y1="0" y2="1">
					<stop offset="5%" stop-color="#825ee4"/>
					<stop offset="95%" stop-color="#825ee4"/>
				</linearGradient>
				<pattern id="wave" x="0" y="0" width="120" height="100" patternUnits="userSpaceOnUse">
					<path id="wavePath" d="M-40 9 Q-30 7 -20 9 T0 9 T20 9 T40 9 T60 9 T80 9 T100 9 T120 9 V20 H-40z" mask="url(#mask)" fill="url(#gradient)"> 
						<animateTransform attributeName="transform" begin="0s" dur="1.2s" type="translate" from="0,0" to="40,0" repeatCount="indefinite" />
					</path>
				</pattern>
			</defs>
			<text text-anchor="middle" x="60" y="17" font-size="17" fill="url(#wave)"  fill-opacity="1">MONEYLOVER</text>
			<text text-anchor="middle" x="60" y="17" font-size="17" fill="url(#gradient)" fill-opacity="0.6">MONEYLOVER</text>
		</svg>
		<form action="{{url('login/')}}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="username">Email</label>
				@if ($wrongPassword = Session::get('wrongPassword'))
				<input type="text" class="form-control" id="username" name="email" value="{{$wrongPassword['email']}}" aria-describedby="emailHelp" placeholder="Nhập email đăng nhập">
				@else
				<input type="text" class="form-control" id="username" name="email" value="{{old('email')}}" aria-describedby="emailHelp" placeholder="Nhập email đăng nhập">
				@endif
			</div>
			<div class="form-group">
				<label for="pw">Password</label>
				<input type="password" class="form-control" id="pw" name="pw" placeholder="Nhập mật khẩu">
			</div>
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="remember" name="remember" checked="checked">
				<label class="form-check-label" for="remember">Lưu tài khoản.</label>
				<a href="{{url('/forgot')}}" style="float: right">Quên mật khẩu?</a>
			</div>
			<button type="submit" class="btn" style="width: 70%">Đăng nhập</button>
			<button type="button" class="btn" style="width: 28.5%" onclick=" location.href = '/register' ">Đăng ký</button>
		</form>
@endsection
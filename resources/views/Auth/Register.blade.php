@extends('Auth.widget.master')
@section('title','Đăng ký')
@section('content')
		<svg viewbox="-10 0 140 20">
			<defs>
				<linearGradient id="gradient" x1="0" x2="0" y1="0" y2="1">
					<stop offset="5%" stop-color="#825ee4"/>
					<stop offset="95%" stop-color="#825ee4"/>
				</linearGradient>
				<pattern id="wave" x="0" y="0" width="120" height="100" patternUnits="userSpaceOnUse">
					<path id="wavePath" d="M-40 9 Q-30 7 -20 9 T0 9 T20 9 T40 9 T60 9 T80 9 T100 9 T120 9 V20 H-40z" mask="url(#mask)" fill="url(#gradient)"> 
						<animateTransform attributeName="transform" begin="0s" dur="1.2s" type="translate" from="0,0" to="40,0" repeatCount="indefinite" />
					</path>
				</pattern>
			</defs>
			<text text-anchor="middle" x="60" y="17" font-size="17" fill="url(#wave)"  fill-opacity="1">Đăng ký</text>
			<text text-anchor="middle" x="60" y="17" font-size="17" fill="url(#gradient)" fill-opacity="0.6">Đăng ký</text>
		</svg>

		<form method="POST" action="{{url('register/')}}">
			{{ csrf_field() }}
			
			<fieldset class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="vd: example@mail.com" value="{{ old('email') }}">
				<small class="text-muted"></small>
			</fieldset>
			<fieldset class="form-group">
				<label for="username">Tên người dùng</label>
				<input type="text" class="form-control" id="username" name="name" placeholder="Nhập tên của bạn" value="{{ old('name') }}">
				<small class="text-muted"></small>
			</fieldset>
			<fieldset class="form-group">
				<label for="pw">Mật khẩu</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="6-16 ký tự gồm chữ và số">
				<small class="text-muted"></small>
			</fieldset>
			<fieldset class="form-group">
				<label for="repw">Nhập lại mật khẩu</label>
				<input type="password" class="form-control" id="repassword" name="repassword" placeholder="Nhập lại mật khẩu phía trên">
				<small class="text-muted"></small>
			</fieldset>
			<div class="form-check">
				<input type="checkbox" class="form-check-input" name="accept" id="accept" checked="checked">
				<label class="form-check-label" for="accept">Tôi đồng ý với các </label> <a href="index.html"> điều khoản chính sách!</a>
			</div>
			<button type="submit" class="btn" style="width: 55%">Xác nhận</button>
			<button type="button" class="btn" style="width: 43.5%" onclick=" location.href = '/login' ">Quay lại đăng nhập</button>
		</form>
@endsection
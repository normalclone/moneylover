@extends('Auth.widget.master')
@section('title','Xác nhận tài khoản')
@section('content')
<script>
	window.onload = function() {
		var element = document.getElementById('notify');
		setInterval(function() {
			var count = element.innerHTML;
			if(count == 1){
				window.location.href = "{{ url('/login') }}";
			}
			element.innerHTML = count - 1;
		},1000)
	}
</script>
@if ($registerSuccess = Session::get('registerSuccess'))
<label for="email" style="margin: 0 15px">
	Mail đã được gửi! Vui lòng kiểm tra email <b>{{ $registerSuccess }}</b> để kích hoạt tài khoản. <br><br>
	<a href="{{ url('register/resend') }}/{{ $registerSuccess }}"> Click vào đây</a> nếu bạn chưa nhận được email kích hoạt!
	<br><a href="{{ url('/login') }}"> Click vào đây</a> để trở lại trang đăng nhập.
</label>
@elseif ($registerFail = Session::get('registerFail'))
<label for="email" style="margin: 0 15px">
	- Yêu cầu không thành công mail đã được gửi chưa tới 1 phút trước 
	<br>- Thời gian chờ gửi lại mail kích hoạt là <b>1 phút!</b> Vui lòng kiểm tra email <b>{{ $registerFail }}</b> để kích hoạt tài khoản hoặc chờ để gửi lại mail. <br>
	<br><a href="{{ url('register/resend') }}/{{ $registerFail }}"> Click vào đây</a> nếu bạn chưa nhận được email kích hoạt!
	<br><a href="{{ url('/login') }}"> Click vào đây</a> để trở lại trang đăng nhập.
</label>
@elseif ($registerVerified = Session::get('registerVerified'))
<label for="email" style="margin: 0 15px">
	<b>Tài khoản <b>{{$registerVerified}}</b> của bạn đã được kích hoạt rồi! <br>
	<br><a href="{{ url('/login') }}"> Click vào đây</a> hoăc chờ <b id="notify">10</b>s để trở lại trang đăng nhập.
</label>
@elseif ($verifySuccess = Session::get('verifySuccess'))
<label for="email" style="margin: 0 15px">
	Tài khoản email <b>{{$verifySuccess}}</b> đã được xác thực thành công!
	<br><a href="{{ url('/login') }}"> Click vào đây</a> hoăc chờ <b id="notify">10</b>s để trở lại trang đăng nhập.
</label>
@elseif ($verifyFail = Session::get('verifyFail'))
<label for="email" style="margin: 0 15px">
	<b>Đường dẫn kích hoạt của bạn không đúng hoặc không còn khả dụng! </b><br>
	Vui lòng <b>đăng nhập lại</b> để nhận mail kích hoạt! <br>
	<br><a href="{{ url('/login') }}"> Click vào đây</a> hoăc chờ <b id="notify">10</b>s để trở lại trang đăng nhập.
</label>
@elseif ($email_LoginNotVerified = Session::get('email_LoginNotVerified'))
<label for="email" style="margin: 0 15px">
	Tài khoản <b>{{$email_LoginNotVerified}}</b> chưa được kích hoạt!<br>
	Vui lòng <b>kiểm tra email</b> để nhận mật khẩu mới! <br>
	<br><a href="{{ url('register/resend') }}/{{ $email_LoginNotVerified }}"> Click vào đây</a> nếu bạn chưa nhận được email kích hoạt!
	<br><a href="{{ url('/login') }}"> Click vào đây</a> hoăc chờ <b id="notify">10</b>s để trở lại trang đăng nhập.
</label>
@elseif ($forgotEmail = Session::get('forgotEmail'))
<label for="email" style="margin: 0 15px">
	<b>Mật khẩu mới đã được gửi tới mail {{$forgotEmail}} của bạn!</b><br>
	Vui lòng <b>kiểm tra email</b> để nhận mật khẩu mới! <br>
	<br><a href="{{ url('/login') }}"> Click vào đây</a> hoăc chờ <b id="notify">10</b>s để trở lại trang đăng nhập.
</label>
@else
<script>
	window.location.href = "{{url('register/')}}";
</script>
@endif
@endsection
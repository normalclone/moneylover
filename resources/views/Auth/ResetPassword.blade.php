@extends('Auth.widget.master')
@section('title','Xác nhận tài khoản')
@section('content')

@if ($email_LoginNotReseted = Session::get('email_LoginNotReseted'))
<form action="{{url('login/reset')}}" method="post">
	{{ csrf_field() }}
	<input type="hidden" name="email" value="{{$email_LoginNotReseted}}">
	<label for="newPassword">
		Nhập mật khẩu mới cho <b>{{$email_LoginNotReseted}}</b>!<br>
	</label>
	<div class="form-group">
		<label for="pw">Mật khẩu mới</label>
		<input type="password" class="form-control" id="pw" name="newPassword" placeholder="Từ 6 - 32 kí tự..">
	</div>
	<div class="form-group">
		<label for="pw">Nhập lại mật khẩu</label>
		<input type="password" class="form-control" id="pw" name="newPassword_confirm" placeholder="Nhập lại mật khẩu phía trên">
	</div>
	<button type="submit" class="btn" style="width: 100%">Xác nhận</button>
</form>
@else
<script>
	window.location.href = "{{url('login/')}}";
</script>
@endif
@endsection
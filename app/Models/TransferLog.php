<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Wallet;

class TransferLog extends Model
{
     protected $table = "tb_transferLog";
     public $timestamps = false;
     protected $fillable = [
       'from_id','to_id','created_at','amount'
    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
    public function toWalletName()
    {   
        $wl = Wallet::where('id',$this->to_id)->select('name')->first();
        if(!$wl){
            return "Ví đã bị xóa";
        }
        return $wl->name;
    }
    public function fromWalletName()
    {
        $wl = Wallet::where('id',$this->from_id)->select('name')->first();
        if(!$wl){
            return "Ví đã bị xóa";
        }
        return $wl->name;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
     protected $table = "tb_transactionType";
     public $timestamps = false;
     protected $fillable = [
       'id','name','parent_id','type','user_id','icon'
    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
    public function Transactions()
    {
    	return $this->hasMany('App\Models\Transaction','type_id','id');
    }

    public function ParentTransactionType()
    {
        $parent = $this->where('id',$this->parent_id)->first();
        if(!$parent){return $this;}
        return $parent;
    }
    public function ChildTransactionType()
    {
        $child = $this->where('parent_id',$this->id)->get();
        if(count($child)==0){return $this->where('id',$this->id)->get();}
        return $child;
    }
}

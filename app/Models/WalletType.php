<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletType extends Model
{
     protected $table = "tb_walletType";
     public $timestamps = false;
     protected $fillable = [
       'id','name','icon','description'
    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
     protected $table = "tb_wallet";
     public $timestamps = false;
     protected $fillable = [
       'id','name','user_id','type_id','amount','description','created_at','updated_at','transfer_time'
    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
    public function User()
    {
        return $this->belongsto('App\Models\User');
    }
    public function WalletType()
    {
        return $this->hasOne('App\Models\WalletType','id','type_id');
    }
    public function Transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }
}

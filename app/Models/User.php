<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "tb_user";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','expired_at','created_at','avatar','description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
     
    ];
    public function Wallets()
    {
        return $this->hasMany('App\Models\Wallet');
    }
    public function TransactionTypes()
    {
        return $this->hasMany('App\Models\TransactionType');
    }
    public function Tasks()
    {
        return $this->hasMany('App\Models\Task');
    }
}

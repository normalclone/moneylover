<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
     protected $table = "tb_transaction";
     public $timestamps = false;
     protected $fillable = [
       'id','amount','type_id','wallet_id','description', 'transaction_date','created_at'

    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
    public function Wallet()
    {
        return $this->belongsto('App\Models\Wallet');
    }
    public function TransactionType()
    {
        return $this->hasOne('App\Models\TransactionType','id','type_id');
    }
}

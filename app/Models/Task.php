<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
     protected $table = "tb_todo";
     public $timestamps = false;
     protected $fillable = [
       'id','description','status','created_at','user_id'
    ];
    protected $hidden = [
    ];
    protected $casts = [
     
    ];
    public function User()
    {
        return $this->belongsto('App\Models\User');
    }
}

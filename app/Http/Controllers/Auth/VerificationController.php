<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DB;
use App\Models\User;
class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */


    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        // $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function Verify($token)
    {
        $tbRegisterToken = DB::table('tb_registerToken')->where('token',$token);

        if($userID = $tbRegisterToken->select('user_id')->first()){
            $userID = $userID->user_id;
            $tbRegisterToken->delete();
            User::where('id',$userID)->update(['status' => 1]);
            return \Redirect::route('registerNotification')
            ->with('verifySuccess', User::where('id',$userID)
                ->select('email')
                ->first()
                ->email);
        }else{
            return \Redirect::route('registerNotification')->with('verifyFail', 1);
        }
    }
}

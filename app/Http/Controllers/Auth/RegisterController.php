<?php

namespace App\Http\Controllers\Auth;

use DB;
use App\Models\User;
use App\Http\Requests\RegisterRequest;
use App\Mail\RegisterTokenMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function Index()
    {
        return view('Auth.Register');
    }

    private function makeToken($email)
    {
        return md5(now()->timestamp.$email);
    }

    public function reSend($email)
    {
        if($userInformation = User::select('id','name','status')->where('email',$email)->first()){
            $userID = $userInformation->id;
            $userName = $userInformation->name;
            $userStatus = $userInformation->status;
            if($userStatus != 0){ return \Redirect::route('registerNotification')->with('registerVerified', $email); }

            $tbRegisterToken = DB::table('tb_registerToken');
            
            $TokenCreatedTime = $tbRegisterToken->select('created_at')->where('user_id',$userID)->first(); 

            $Date = !empty($TokenCreatedTime) ? date_timestamp_get(new \Datetime($TokenCreatedTime->created_at)) : now()->timestamp - 1000;
            if(now()->timestamp - $Date > 60){
                $token = $this->makeToken($email);
                $tbRegisterToken->where('user_id',$userID)->delete();
                $tbRegisterToken->insert([
                    'user_id' => $userID,
                    'token' => $token,
                ]);
                $data = array(
                    'name' => $userName,
                    'token' => $token,
                );
                Mail::to($email)->send(new RegisterTokenMail($data));
                return \Redirect::route('registerNotification')->with('registerSuccess', $email);
            }else{
                return \Redirect::route('registerNotification')->with('registerFail', $email);
            }
        }else{
            return redirect()->to('/login');
        }
    }
    protected function Create(RegisterRequest $rq)
    {
        $validator = $rq->validated();
        // Create user
        User::create([
            'email' => $rq->email,
            'password' => Hash::make($rq->password),
            'name' => $rq->name,
            'expired_at' => now()->addYear(),
            'created_at' => now()
        ]);
        //Create token
        $id = User::select('id')->where('email', $rq->email)->first()->id;
        $token = $this->makeToken($rq->email);
        DB::table('tb_registerToken')->insert([
            'user_id' => $id,
            'token' => $token,
        ]);
        //Send mail
        $data = array(
            'name' => $rq->name,
            'token' => $token,
        );
        Mail::to($rq->email)->send(new RegisterTokenMail($data));
        //Return message
        return \Redirect::route('registerNotification')->with('registerSuccess',$rq->email);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */  

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function Index()
    {
        return view('Auth.Login');
    }
    public function Login(LoginRequest $rq)
    {
        $validator = $rq->validated();
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->pw],$rq->remember)){
            if(Auth::user()->status == 0){
                $email = Auth::user()->email;
                Auth::logout();
                return \Redirect::route('registerNotification')->with('email_LoginNotVerified', $email);
            }elseif(Auth::user()->status == 3){
                $email = Auth::user()->email;
                Auth::logout();
                return \Redirect::route('resetPassword')->with('email_LoginNotReseted',$email);
            }else{
                Auth::user()->update(['expired_at'=>now()->addYear()]);
                return redirect()->to('/');
            }
        }else{
            return \Redirect::route('loginFailure')->with('wrongPassword', ['message'=>'Sai mật khẩu!', 'email'=>$rq->email]);
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\ForgotPasswordMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function makeResetPasswordAndSendUser(ForgotPasswordRequest $rq)
    {
        $validator = $rq->validated();
        $user = User::where('email',$rq->email);
        if(!empty($user->first()) && $user->select('status')->first()->status != 0){
            $newPassword = substr(md5(mt_rand()), 0, 10);

            $user->update(['password'=>Hash::make($newPassword), 'status'=>3, 'updated_at'=>now()]);

            $data = array(
                'name'      => $user->select('name')->first()->name,
                'password'  => $newPassword,
            );
            Mail::to($rq->email)->send(new ForgotPasswordMail($data));
            return \Redirect::route('forgotNotification')->with('forgotEmail', $rq->email);
        }else{
            return \Redirect::route('failEmail')->with('emailFail', $rq->email);
        }
    }

    public function resetPassword(ResetPasswordRequest $rq){
        $validator = $rq->validated();
        User::where('email',$rq->email)->update(['password'=>Hash::make($rq->newPassword),'expired_at'=>now()->addYear(),'status'=>1]);
        Auth::attempt(['email' => $rq->email, 'password' => $rq->newPassword]);
        return redirect()->to('/');
    }
}

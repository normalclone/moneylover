<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Task;
use Illuminate\Http\Request;
class TaskController extends Controller
{

	public function __construct()
	{
		$this->middleware('notguest');
	}
	public function add(Request $rq){
		Task::create(['description'=>$rq->description, 'user_id' => Auth::user()->id, 'created_at' => now()]);
		$this->get();
		return;
	}
	public function delete(Request $rq){
		Task::where('id',$rq->id)->delete();
		$this->get();
		return;
	}
	public function update(Request $rq){
		$task = Task::where('id',$rq->id);
		if($task->first()->status != 0){
			$task->update(['status'=>0]);
		}else{
			$task->update(['status'=>1]);
		}
		$this->get();
		return;
	}
	public function getNoti(){
		$todoList = Auth::user()->Tasks()->orderBy('created_at',"Desc")->get();
		$noti = "";
		foreach ($todoList as $td) {
			if($td->status == 0){
				$noti .= '<li class="nav-link pr-2 pl-2 nav-item dropdown-item"> - '.$td->description.'</li>';
			}
		}
		if($noti == ""){echo '<li class="nav-link text-primary nav-item dropdown-item"><a href="javascript:void(0)" class="nav-item dropdown-item">Bạn không có ghi chú nào chưa hoàn thành!</a></li>';}
		else{echo $noti;}
	}
	public function get(){
		$todoList = Auth::user()->Tasks()->orderBy('created_at',"Desc")->get();
		$rs = "<script>
			$(function() {
			$('.todo-delete').on('click',function(){
				$.ajax({
					url:'".url('todo/delete')."', 
					method:'POST', 
					data:{
						id : $(this).attr('data'),
						_token: $('meta[name=\"csrf-token\"]').attr('content')
						},
						success:function(data){ 
							$('.todo-list').html(data);
						}
				});
				setTimeout(function() {
    				getTodoNoti();
  				},1000)
			});
			$('.todo-checkbox').on('click',function() {
				$.ajax({
					url:'".url('todo/update')."', 
					method:'POST', 
					data:{
						id : $(this).attr('data'),
						_token: $('meta[name=\"csrf-token\"]').attr('content')
						},
						success:function(data){ 
							$('.todo-list').html(data);
						}
				});
				setTimeout(function() {
    				getTodoNoti();
  				},1000)
			});

			});
			</script>"; 
		
		foreach ($todoList as $td) {
			if($td->status == 0){
				$rs .= '<li class="todo-list-item">
				<div class="checkbox">
					<div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input todo-checkbox" type="checkbox" data="'.$td->id.'" id="checkbox-'.$td->id.'">
                              <span class="form-check-sign">
                                <span class="check"></span>
                             </span>
                        </label>
                    
				<label class="display-inline" for="checkbox-'.$td->id.'">'.$td->description.'</label>
				</div>
				</div>
				<div class="pull-right action-buttons"><a href="#!" class="trash todo-delete" data="'.$td->id.'"><em class="fa fa-trash"></em></a></div>
				<br>
				</li>';
			}
			else{
				$rs .= '<li class="todo-list-item">
				<div class="checkbox">
				<div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input todo-checkbox" type="checkbox" data="'.$td->id.'" id="checkbox-'.$td->id.'" checked="">
                              <span class="form-check-sign">
                                <span class="check"></span>
                             </span>
                        </label>
                    
				<label class="display-inline" style="text-decoration: line-through;color: #777 !important; " for="checkbox-'.$td->id.'">'.$td->description.'</label>
				</div>
				</div>
				<div class="pull-right action-buttons"><a href="#!" class="trash todo-delete" data="'.$td->id.'"><em class="fa fa-trash"></em></a></div>
				<br>
				</li>';
			}
		}
		echo $rs;
		if(count($todoList) == 0){echo '<li class="todo-list-item"><p class="text-sm text-muted">Chưa có ghi chú nào!</li>';}
		return;
	}

}

<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Management\DashboardController;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
class StatisticController extends DashboardController
{

	public function __construct()
	{
		$this->middleware('notguest');
	}
	public function ReportIndex()
	{
		return view('Management.Statistic.ReportIndex')->with('types',$this->_2LevelTransactionTypeMenu());
	}
	public function TrendIndex(){
		return view('Management.Statistic.TrendIndex');	
	}
	public function getTable(Request $rq){
		$dateStart = Carbon::create($rq->startDate);
		$dateEnd = Carbon::create($rq->endDate);
		$wallets = Auth::user()->Wallets()->select('type_id','id')->get();
		$ID = "";
		foreach ($wallets as $k => $w) {
			if($w->type_id != 3 && $ID != ""){
				$ID = $ID." or wallet_id = ".$w->id;
			}
			if($w->type_id != 3 && $ID == ""){
				$ID = "wallet_id = ".$w->id;
			}

		}
		$whereClause = "";
		if($rq->id == "all"){
			$whereClause = "(transaction_date >= '".$dateStart."' and transaction_date <= '".$dateEnd."'". ") AND (".$ID.")";
		}else{
			$type_id = "";
			$childs = TransactionType::where('id',$rq->id)->first()->ChildTransactionType();
			foreach ($childs as $c) {
				if($type_id != ""){
					$type_id = $type_id." or type_id = ".$c->id;
				}else{
					$type_id = "type_id = ".$c->id;
				}
			}
			$whereClause = "(transaction_date >= '".$dateStart."' and transaction_date <= '".$dateEnd."'". ") AND (".$ID.") AND (".$type_id.")";
		}
		$transaction = Transaction::whereRaw($whereClause)->with('TransactionType')->select('id','amount','transaction_date','wallet_id','type_id','description')->orderBy('transaction_date','desc')->get();
		echo '<table class="table align-items-center table-flush" id="data-table">
            <thead class="thead">
              <tr class="">
                <th scope="col" class="noExl"> </th>
                <th scope="col">Loại giao dịch</th>
                <th scope="col">Thời gian</th>
                <th scope="col">Số tiền</th>
                <th scope="col">Ví sử dụng</th>
                <th scope="col" class="noExl">Thao tác</th>
                <th scope="col" style="display: none">Chi tiết</th>
              </tr>
            </thead>
            <tbody id="result-wrapper">';
            $count = 0;
		foreach ($transaction as $tr) {
			$count++;
			if($tr->TransactionType->type != 0){
				echo '<tr count="'.$count.'" class="display-none table-tr">
				<th scope="row" class="noExl">
				<img src="'.url($tr->TransactionType->icon).'" width="25" height="25" alt="">
				</th>
				<td class="text-success">
				'.$tr->TransactionType->name.'
				</td>
				<td class="text-success">
				'.$tr->transaction_date.'
				</td>
				<td class="text-success">
				+'.number_format($tr->amount,0).' VNĐ
				</td>
				<td class="text-success">
				'.$tr->Wallet()->first()->name.'
				</td>
				<td class="noExl">
				<a href="'.url('/transaction/get/').'/'.$tr->id.'" class="btn btn-primary text-white btn-sm">Chi tiết</a>
				</td>
				<td style="display:none">
				'.$tr->description.'
				</td>
				</tr>';
			}else{
				echo '<tr count="'.$count.'" class="display-none table-tr">
				<th scope="row" class="noExl">
				<img src="'.url($tr->TransactionType->icon).'" width="25" height="25" alt="">
				</th>
				<td class="text-danger">
				'.$tr->TransactionType->name.'
				</td>
				<td class="text-danger">
				'.$tr->transaction_date.'
				</td>
				<td class="text-danger">
				-'.number_format($tr->amount,0).' VNĐ
				</td>
				<td class="text-danger">
				'.$tr->Wallet()->first()->name.'
				</td>
				<td class="noExl">
				<a href="'.url('/transaction/get/').'/'.$tr->id.'" class="btn btn-primary text-white btn-sm">Chi tiết</a>
				</td>
				<td style="display:none">
				'.$tr->description.'
				</td>
				</tr>';
			}
		}
		echo "</tbody>
          </table>";
        if(count($transaction)>7){
        	echo '<center><div class="pagination-table" id="transaction-table">
						<a href="#" class="first" data-action="first">&laquo;</a>
						<a href="#" class="previous" data-action="previous">&lsaquo;</a>
						<input type="text" readonly="readonly" data-max-page="40" />
						<a href="#" class="next" data-action="next">&rsaquo;</a>
						<a href="#" class="last" data-action="last">&raquo;</a>
					</div></center>
					<script src="'.url("./assets/js/plugins/jquery.pagination.js").'"></script>';
				}
			echo "<script>
	$(function () {
		for (var i = 1; i <= 7; i++) { 
			var selector = \"tr[count='\"+i+\"']\";
			$(selector).attr('class','table-tr')
		};
		$('#transaction-table').jqPagination({
			max_page	: ".ceil(count($transaction)/7).",
			paged : function(page) {
				$('.table-tr').attr('class','display-none table-tr');
				var cal = page*7;
				for(var i = cal-6; i<=cal;i++){
					var selector = \"tr[count='\"+i+\"']\";
					$(selector).attr('class','table-tr')
				}
			}
		});
	})
</script>";
	}

	private function _2LevelTransactionTypeMenu(){
		$type = TransactionType::where('user_id',0)->orWhere('user_id',Auth::user()->id)->get();
		$transactionTypes = array();
		$del = array();
		foreach ($type as $t) {
			$subType = array();
			$icon = "";
			if(explode('/',$t->icon)[0] == "assets"){
				$icon = url($t->icon);
			}else{
				$icon = $t->icon;
			}
			foreach ($type as $key =>$ty) {
				$_icon = "";
				if(explode('/',$ty->icon)[0] == "assets"){
					$_icon = url($ty->icon);
				}else{
					$_icon = $ty->icon;
				}
				if($ty->parent_id == $t->id){
					array_push($subType, array('id'=>$ty->id,'name'=>$ty->name,'icon'=>$_icon,'type'=>$ty->type,'parent'=>$t->name));
					array_push($del, $key);
				}
			}
			$data = array('id'=>$t->id,'name'=>$t->name,'icon'=>$icon,'type'=>$t->type,'child'=>$subType);
			array_push($transactionTypes,$data);
		}
		foreach ($del as $d) {
			unset($transactionTypes[$d]);
		}
		return $transactionTypes;
	}

	public function getChart(Request $rq){
		$txt = '<script>
    $(function(){
	var ctx = document.getElementById("bar-chart").getContext("2d");
    var gradientStroke_sub = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke_sub.addColorStop(1, "rgba(245, 54, 92, .2)");
    gradientStroke_sub.addColorStop(0.4, "rgba(245, 54, 92, .1)");
    gradientStroke_sub.addColorStop(0, "rgba(245, 54, 92, .01)"); //danger colors

    var gradientStroke_add = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke_add.addColorStop(1, "rgba(45, 206, 137, .2)");
    gradientStroke_add.addColorStop(0.4, "rgba(45, 206, 137, .1)");
    gradientStroke_add.addColorStop(0, "rgba(45, 206, 137, .01)"); //danger colors
    	
      var myBarChart = new Chart(ctx, {
      type: "bar",
      responsive: true,
      legend: {
        display: false
      },
      data: {
        labels: [[bar-chart-label]],
        datasets: [{
          label: "Thu",
          fill: true,
          backgroundColor: gradientStroke_add,
          hoverBackgroundColor: gradientStroke_add,
          borderColor: "rgba(45, 206, 137, 1)",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: [[bar-chart-data-add]],
        },
        {
          label: "Chi",
          fill: true,
          backgroundColor: gradientStroke_sub,
          hoverBackgroundColor: gradientStroke_sub,
          borderColor: "rgba(245, 54, 92, 1)",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: [[bar-chart-data-sub]],
        }]
      },
      options: gradientBarChartConfiguration
    });
    })
  </script>
  <!-- chart-add -->
  <script>
    $(function(){
     var chart = new CanvasJS.Chart("pie-chart-add-wrapper", {
      backgroundColor: "#27293D",
      exportEnabled: true,
      animationEnabled: true,
      title: {
		text: "[[pie-chart-title-add]]",
		fontColor: "#24A46D",
		fontSize: 20,
		},
      legend:{
        cursor: "pointer",
        itemclick: explodePie
      },
      data: [{
        type: "pie",
        startAngle: 240,
        yValueFormatString: "#,### \"VNĐ\"",
        indexLabelFontColor: "white",
        indexLabel: "{label} {y}",
        dataPoints: [[pie-chart-data-add]]
  }]
});
chart.render();
    })

  </script>
  <!-- chart-sub -->
  <script>
    $(function(){
     var chart = new CanvasJS.Chart("pie-chart-sub-wrapper", {
      backgroundColor: "#27293D",
      exportEnabled: true,
      animationEnabled: true,
      title: {
		text: "[[pie-chart-title-sub]]",
		fontColor: "#EC0C38",
		fontSize: 20,
		},
      legend:{
        cursor: "pointer",
        itemclick: explodePie
      },
      data: [{
        type: "pie",
        startAngle: 240,
        indexLabelFontColor: "white",
        yValueFormatString: "#,### \"VNĐ\"",
        indexLabel: "{label} {y}",
        dataPoints: [[pie-chart-data-sub]]
  }]
});
chart.render();
    })

    function explodePie (e) {
  if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
  } else {
    e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
  }
  e.chart.render();

}
  </script>';
		$dateStart = Carbon::create($rq->startDate);
		$dateEnd = Carbon::create($rq->endDate);
		$wallets = Auth::user()->Wallets()->select('type_id','id')->get();
		$ID = "";
		foreach ($wallets as $k => $w) {
			if($w->type_id != 3 && $ID != ""){
				$ID = $ID." or wallet_id = ".$w->id;
			}
			if($w->type_id != 3 && $ID == ""){
				$ID = "wallet_id = ".$w->id;
			}

		}
		$whereClause = "(transaction_date >= '".$dateStart."' and transaction_date <= '".$dateEnd."'". ") AND (".$ID.")";
		$transaction = Transaction::whereRaw($whereClause)->with('TransactionType')->select('amount','transaction_date','type_id')->get();
		echo '<pre>';
		//Bar chart
		$date = $this->getDaysBetweenDate($dateStart,$dateEnd);
		$rs = array();
		$lb = "[";
		$subData = "[";
		$addData = "[";
		$dateDiff = count($date);
		if($dateDiff <= 31){
			$rs = $this->getAmountEachDay($date,$transaction);
			
			foreach ($rs as $r) {
				$day = explode('-', $r['date']);
				if($lb == "[") {
					$lb.='"'.$day[2].'/'.$day[1].'"';
					$addData.=$r['add'];
					$subData.=$r['sub'];
				}else{
					$lb.=','.'"'.$day[2].'/'.$day[1].'"';
					$addData.=','.$r['add'];
					$subData.=','.$r['sub'];
				}
			}
		}
		elseif($dateDiff <= 800){
			$dateStart = Carbon::create($rq->startDate);
			$dateEnd = Carbon::create($rq->endDate);
			$months = $this->getMonthsBetweenDate($dateStart,$dateEnd);
			
			$rs = $this->getAmountEachMonth($months,$transaction);
			foreach ($rs as $r) {
				$month = explode('-', $r['date']);
				if($lb == "[") {
					$lb.='"'.$month[1].'/'.$month[0].'"';
					$addData.=$r['add'];
					$subData.=$r['sub'];
				}else{
					$lb.=','.'"'.$month[1].'/'.$month[0].'"';
					$addData.=','.$r['add'];
					$subData.=','.$r['sub'];
				}
			}
		}
		else{
			$dateStart = Carbon::create($rq->startDate);
			$dateEnd = Carbon::create($rq->endDate);
			$years = $this->getYearsBetweenDate($dateStart,$dateEnd);

			$rs = $this->getAmountEachYear($years,$transaction);
			foreach ($rs as $r) {
				$year = $r['date'];
				if($lb == "[") {
					$lb.='"'.$year.'"';
					$addData.=$r['add'];
					$subData.=$r['sub'];
				}else{
					$lb.=','.'"'.$year.'"';
					$addData.=','.$r['add'];
					$subData.=','.$r['sub'];
				}
			}
		}
		//end bar chart
		//Pie chart
		$types = $this->getListTransactionType($transaction);
		$rs = $this->getAmountEachType($types,$transaction);
		$pieChartData_add = "[";
		$pieChartData_sub = "[";
		$sumAdd = 0;
		$sumSub = 0;
		foreach ($rs as $r) {
			if($r['transactionType']['type'] == 0){
				$sumSub+=$r['amount'];
				$pieChartData_sub .="{y: ".$r['amount'].", label: \"".$r['transactionType']['name']."\"},";
			}else{
				$sumAdd+=$r['amount'];
				$pieChartData_add .="{y: ".$r['amount'].", label: \"".$r['transactionType']['name']."\"},";
			}
		}
		//result
		$lb.="]";
		$subData.="]";
		$addData.="]";
		$pieChartData_add.="]";
		$pieChartData_sub.="]";
		$percentAdd = 0;
		$percentSub = 0;
		if($sumAdd != 0 || $sumSub != 0){
			$percentAdd = ($sumAdd*100)/($sumAdd+$sumSub);
			$percentSub = ($sumSub*100)/($sumAdd+$sumSub);
		}
		$txt = str_replace("[[bar-chart-label]]", $lb, $txt);
		$txt = str_replace("[[bar-chart-data-add]]", $addData, $txt);
		$txt = str_replace("[[bar-chart-data-sub]]", $subData, $txt);
		$txt = str_replace("[[pie-chart-title-add]]", '+'.number_format($sumAdd).' VNĐ ('.number_format($percentAdd,2).'%)', $txt);
		$txt = str_replace("[[pie-chart-title-sub]]",'-'.number_format($sumSub).' VNĐ ('.number_format($percentSub,2).'%)', $txt);
		$txt = str_replace("[[pie-chart-data-add]]", $pieChartData_add, $txt);
		$txt = str_replace("[[pie-chart-data-sub]]", $pieChartData_sub, $txt);
		print_r($txt);
	}

	public function getAmountEachMonth($listMonths,$transactions){
		$amounts = array();
		foreach ($listMonths as $month) {
			$sub = 0;
			$add = 0;
			$total = 0;
			foreach ($transactions as $tr) {
				$trMonth = explode(' ', $tr->transaction_date)[0];
				$trMonth = explode('-',$trMonth);
				if($month == $trMonth[0].'-'.$trMonth[1]){
					$total += $tr->amount;
					if($tr->TransactionType->type == 0){ $sub += $tr->amount; }
					else {$add += $tr->amount; }
				}
			}
			array_push($amounts,array('date'=>$month,'sub'=>$sub,'add'=>$add,'total'=>$total));
		}
		return $amounts;
	}
	public function getAmountEachYear($listYears,$transactions){
		$amounts = array();
		foreach ($listYears as $year) {
			$sub = 0;
			$add = 0;
			$total = 0;
			foreach ($transactions as $tr) {
				$trYear = explode('-', $tr->transaction_date)[0];
				if($year == $trYear){
					$total += $tr->amount;
					if($tr->TransactionType->type == 0){ $sub += $tr->amount; }
					else {$add += $tr->amount; }
				}
			}
			array_push($amounts,array('date'=>$year,'sub'=>$sub,'add'=>$add,'total'=>$total));
		}
		return $amounts;
	}
	public function getMonthsBetweenDate($dateStart,$dateEnd){
		$months = array();
		while ($dateStart < $dateEnd) {
			$month = explode('-',explode(' ',$dateStart->toDateTimeString())[0]);
			array_push($months,$month[0].'-'.$month[1]);
			$dateStart->addMonth();
		}
		return $months;
	}

	public function getYearsBetweenDate($dateStart,$dateEnd){
		$years = array();
		while ($dateStart < $dateEnd){
			$year = explode('-', $dateStart->toDateTimeString())[0];
			array_push($years,$year);
			$dateStart->addYear();
		}
		return $years;
	}

	public function getAmountEachType($listTypes,$transactions){
		$amounts = array();
		foreach ($listTypes as $t) {
			$amount = 0;
			foreach ($transactions as $tr) {
				if($t['id'] == $tr->type_id){
					$amount+=$tr->amount;
				}
			}
			array_push($amounts, array('transactionType'=>$t,'amount'=>$amount));
		}
		return $amounts;
	}
	public function getListTransactionType($transaction){
		$types = array();
		foreach ($transaction as $tr) {
			if(!in_array($tr->TransactionType,$types)){
				array_push($types, $tr->TransactionType);
			}
		}
		$rsType = array();
		foreach ($types as $t) {
			array_push($rsType, array('id'=>$t->id,'name'=>$t->name, 'type'=>$t->type));
		}
		return $rsType;
	}
}
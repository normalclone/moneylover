<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\TransactionType;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Http\Requests\AddTransactionTypeRequest;
use App\Http\Requests\UpdateTransactionTypeRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TransactionTypeController extends Controller
{
	public function __construct(){
		$this->middleware('notguest');
	}


	public function addTransactionTypeForm(){
		return view('Management.Transaction.AddTransactionType')->with('types',TransactionType::where('parent_id',0)->where('user_id',0)->orWhere('user_id',Auth::user()->id)->get());
	}

	public function addTransactionType(AddTransactionTypeRequest $rq){
		if(!Hash::check($rq->password,Auth::user()->password)){return redirect()->back()->withErrors('Sai mật khẩu');}
		if($rq->type == "" && $rq->parent_id == 0){ return redirect()->back()->withErrors('Bạn chưa chọn loại giao dịch');}
		if(empty($rq->icon) && empty($rq->icon_text)){ return redirect()->back()->withErrors('Bạn chưa nhập icon!');}
		$existType = TransactionType::where('parent_id',0)->where('user_id',0)->orWhere('user_id',Auth::user()->id)->select('name')->get();
		foreach ($existType as $t) {
			if($rq->nameType == $t->name){ return redirect()->back()->withErrors('Loại giao dịch này đã tồn tại rồi! Vui lòng kiểm tra lại');}
		}
		if($rq->type=="" && $rq->parent_id != 0){
			$flag = 0;
			$check = TransactionType::where('id',$rq->parent_id)->select('user_id','parent_id')->first();
			if($check = TransactionType::where('id',$rq->parent_id)->count() == 0){return redirect()->back()->withErrors("Giao dịch cha không hợp lệ");}
			if($check->user_id != 0 && $check->user_id != Auth::user()->id){$flag = 1;}
			if($check->parent_id != 0){$flag = 1;}
			if($flag !=0 ){return redirect()->back()->withErrors("Giao dịch cha không hợp lệ");}
			$type = TransactionType::where('id',$rq->parent_id)->select('type')->first()->type;
		}else{
			$type = $rq->type;
		}	
		$parent_id = !empty($rq->parent_id) ? $rq->parent_id : 0;
		if(!empty($rq->icon)){
			$imageName = time().'.'.$rq->icon->getClientOriginalExtension();
			$rq->icon->move(public_path('assets/img/icons/uploads'), $imageName);
			$icon = url('assets/img/icons/uploads/'.$imageName);
		}else{
			if(!isset(explode(url('/'), $rq->icon_text)[1])){
				if( isset(explode(url('/'),url($rq->icon_text))[1]) ){return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
				$headers = get_headers($rq->icon_text, 1);
				if (!(strpos($headers['Content-Type'][1], 'image/') !== false) && !(strpos($headers['Content-Type'], 'image/') !== false)) { return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
			}else{
				if(!file_exists('.'.explode(url('/'), $rq->icon_text)[1])){return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
			}
			$icon = $rq->icon_text;
		}
		TransactionType::create([
			'name'		=>$rq->nameType,
			'parent_id'	=>$parent_id,
			'type'		=>(int)$type,
			'user_id'	=>Auth::user()->id,
			'icon'		=>$icon,
		]);
		return redirect()->to('transaction/type')->with('Success','Thêm loại giao dịch mới thành công!');
	}

	public function getListTransactionTypeOfUser(){
		return view('Management.Transaction.ListTransactionType')->with('type',Auth::user()->TransactionTypes()->get());
	}

	public function getTransactionTypeOfUser($type_id){
		$type = TransactionType::where('id',$type_id)->first();
		if(!$type || $type->user_id != Auth::user()->id){ return redirect()->back()->withErrors('Loại giao dịch này không tồn tại!');}

		return view('Management.Transaction.UpdateTransactionType')->with(['type'=>$type,'types'=>TransactionType::where('parent_id',0)->where('user_id',0)->orWhere('user_id',Auth::user()->id)->get()]);
	}

	public function updateTransactionTypeOfUser(UpdateTransactionTypeRequest $rq){
		if(!Hash::check($rq->password,Auth::user()->password)){return redirect()->back()->withErrors('Sai mật khẩu');}
		if(empty($rq->icon) && empty($rq->icon_text)){ return redirect()->back()->withErrors('Bạn chưa nhập icon!');}
		if($rq->parent_id!=0 && TransactionType::where('id',$rq->parent_id)->select('parent_id')->first()->parent_id != 0){return redirect()->back()->withErrors('Kiểu giao dịch cha không đúng!');}
		$oldType = TransactionType::where('id',$rq->type_id)->first();
		if(!$oldType || $oldType->user_id != Auth::user()->id){ return redirect()->back()->withErrors('Loại giao dịch này không tồn tại!');}
		if($oldType->name != $rq->nameType ){
			$existType = TransactionType::where('parent_id',0)->where('user_id',0)->orWhere('user_id',Auth::user()->id)->select('name')->get();
			foreach ($existType as $t) {
				if($rq->nameType == $t->name){ return redirect()->back()->withErrors('Loại giao dịch này đã tồn tại rồi! Vui lòng kiểm tra lại');}
			}
		}
		$icon = "";
		if(!empty($rq->icon)){
			$imageName = time().'.'.$rq->icon->getClientOriginalExtension();
			$rq->icon->move(public_path('assets/img/icons/uploads'), $imageName);
			$icon = url('assets/img/icons/uploads/'.$imageName);
		}else{
			if(!isset(explode(url('/'), $rq->icon_text)[1])){
				if( isset(explode(url('/'),url($rq->icon_text))[1]) ){return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
				$headers = get_headers($rq->icon_text, 1);
				if (!(strpos($headers['Content-Type'][1], 'image/') !== false) && !(strpos($headers['Content-Type'], 'image/') !== false)) { return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
			}else{
				if(!file_exists('.'.explode(url('/'), $rq->icon_text)[1])){return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
			}
			$icon = $rq->icon_text;
		}
		if($oldType->name == $rq->nameType && $oldType->parent_id == $rq->parent_id && $oldType->icon == $icon){ return redirect()->back()->withErrors('Bạn không thay đổi gì cả!');}
		if($oldType->icon != $icon){
			$oldIcon = "./assets/img/icons/uploads/".explode('/',$oldType->icon)[7];
			if(file_exists($oldIcon)){unlink($oldIcon);}
		}
		$parent_id = !empty($rq->parent_id) ? $rq->parent_id : 0;
		TransactionType::where('id',$rq->type_id)->update([
			'name'		=> $rq->nameType,
			'parent_id'	=> $parent_id,
			'icon'		=> $icon,
		]);
		return redirect()->back()->with("Success",'Thay đổi thông tin thành công!');
	}

	public function DeleteTransactionTypeOfUser($type_id){
		$type = TransactionType::where('id',$type_id)->first();
		if(!$type || $type->user_id != Auth::user()->id){ return redirect()->back()->withErrors('Loại giao dịch này không tồn tại!');}
		$icon = "./assets/img/icons/uploads/".explode('/',$type->icon)[7];
		if(file_exists($icon)){unlink($icon);}
		Transaction::where('type_id',$type_id)->delete();
		TransactionType::where('id',$type_id)->delete();
		return redirect()->to('transaction/type')->with('Success','Xóa loại giao dịch và các giao dịch liên quan thành công!');
	}
}

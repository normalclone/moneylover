<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeInformationRequest;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserInforController extends Controller
{

	public function __construct()
	{
		$this->middleware('notguest');
	}

	public function Index()
	{
		return view('Management.UserInfo');
	}
	
	public function ChangeInformation(ChangeInformationRequest $rq)
	{	
		$avatar = "";
		if(!Hash::check($rq->password,Auth::user()->password)){    
			return redirect('userinfo')->withErrors('Sai mật khẩu');
		}
		if(empty($rq->avatar_img) && empty($rq->avatar_text)){
			return redirect('userinfo')->withErrors('Không có bất kí bức ảnh nào được tìm thấy, vui lòng nhập ảnh mới hoặc giữ nguyên mặc định');
		}
		if(!empty($rq->avatar_img)){
			//Code delete old img
			$curAvatar = explode('/',Auth::user()->avatar);
			$curLink = explode('/', url('/'));
			$curAvt = "";
			for ($i=3; $i < count($curAvatar) ; $i++) { 
				$curAvt.='/'.$curAvatar[$i];
			}
			if($curAvatar[2]==$curLink[2] && file_exists("./".$curAvt)){
				unlink("./".$curAvt);
			}
			//End delete
			$imageName = time().'.'.$rq->avatar_img->getClientOriginalExtension();
			$rq->avatar_img->move(public_path('uploads'), $imageName);
			$avatar = url('uploads/'.$imageName);
		}else{
			if(!isset(explode(url('/'), $rq->avatar_text)[1])){
				if( isset(explode(url('/'),url($rq->avatar_text))[1]) ){return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
				$headers = get_headers($rq->avatar_text, 1);
				if (!(strpos($headers['Content-Type'][1], 'image/') !== false) && !(strpos($headers['Content-Type'], 'image/') !== false)) { return redirect()->back()->withErrors('Đường dẫn bạn nhập không phải hình ảnh!');}
			}
			//delete old img
			$curAvatar = explode('/',Auth::user()->avatar);
			$curLink = explode('/', url('/'));
			$curAvt = "";
			for ($i=3; $i < count($curAvatar) ; $i++) { 
				$curAvt.='/'.$curAvatar[$i];
			}
			if($curAvatar[2]==$curLink[2] && file_exists("./".$curAvt)){
				unlink("./".$curAvt);
			}
			//end delete
			$avatar = $rq->avatar_text;
		}
		if(Auth::user()->avatar==$avatar && Auth::user()->name == $rq->name && Auth::user()->description == $rq->description){
			return redirect('userinfo')->withErrors('Bạn không thay đổi thông tin gì.');
		}else{
			User::where('id', Auth::user()->id)->update([
				'name' => $rq->name, 
				'avatar' => $avatar, 
				'description' => $rq->description
			]);
			return redirect('userinfo')->with('Success',"Thay đổi thông tin thành công!");
		}
	}

	public function IndexChangePassword(){
		return view('Management.ChangePassword');
	}

	public function changePassword(ChangePasswordRequest $rq){
		if(!Hash::check($rq->oldPassword,Auth::user()->password)){  
			return redirect('userinfo/change-password')->withErrors('Sai mật khẩu');
		}
		elseif($rq->newPassword != $rq->newPassword_confirm){   
			return redirect('userinfo/change-password')->withErrors('Mật khẩu nhập lại không đúng');
		}
		else{
			User::where('id', Auth::user()->id)->update(['password' => Hash::make($rq->newPassword)]);
			return redirect('userinfo')->with('Success',"Đổi mật khẩu thành công!");
		}
	}
}

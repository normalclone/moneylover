<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddWalletRequest;
use App\Http\Requests\ChangeWalletInformationRequest;
use App\Http\Requests\ChooseWalletToTransferMoneyRequest;
use App\Http\Requests\TransferMoneyRequest;
use App\Models\WalletType;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Transaction;
use App\Models\TransferLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class WalletController extends Controller
{
	public function __construct()
	{
		$this->middleware('notguest');
	}

	public function addWalletIndex(){
		return view('Management.Wallet.ChooseWalletType')->with('walletTypes',WalletType::all());
	}

	public function addWalletForm($type_id){
		$walletType = WalletType::find($type_id);
		if(!$walletType){
			return redirect()->to('wallet/type');
		}
		return view('Management.Wallet.AddWalletForm')->with('type',$walletType);
	}

	public function addWallet(AddWalletRequest $rq){
		if(!Hash::check($rq->password,Auth::user()->password)){    
			return redirect()->back()->withErrors('Sai mật khẩu');
		}
		$amount = $this->getAmountToInsertFromText($rq->amount);
		
		Wallet::create([
			'name' 			=> $rq->walletName,
			'user_id' 		=> Auth::user()->id,
			'type_id' 		=> $rq->type_id,
			'amount'		=> $amount,
			'description'	=> $rq->description,
			'updated_at' 	=> now(),
			'created_at' 	=> now()
		]);
		return redirect()->to('wallet/choose')->with('Success','Thêm ví thành công!');
	}

	public function Index(){
		$wallets = Auth::user()->Wallets()->get();
		if($wallets->count()==0) return redirect()->to('wallet/choose')->withErrors('Bạn chưa có ví nào! Hãy tạo ví trước!');
		return view('Management.Wallet.ListWallet')->with('wallets',$wallets);
	}

	public function getWalletInformation($wallet_id){
		$wallet = Wallet::where('id',$wallet_id)->with('WalletType')->first();
		$walletType = WalletType::all();
		//Check owner
		if(!$wallet || $wallet->User()->first()->id != Auth::user()->id){
			return redirect()->to('wallet')->withErrors('Ví này không tồn tại!');
		};
		$transactions = Transaction::where('wallet_id',$wallet_id)->orderBy('transaction_date','Desc')->get();
		$transferLog = TransferLog::whereRaw('from_id ='.$wallet_id.' or to_id ='.$wallet_id)->orderBy('created_at','desc')->get();
		return view('Management.Wallet.Wallet')->with('data',array(
			'wallet'=>$wallet,
			'walletType'=>$walletType,
			'transactions'=>$transactions,
			'transferLogs'=>$transferLog
		));
	}

	public function changeWalletInformation(ChangeWalletInformationRequest $rq){
		$wallet = Wallet::where('id',$rq->wallet_id)->first();
		if(!$wallet || $wallet->User()->first()->id != Auth::user()->id){
			return redirect()->back()->withErrors('Sai thông tin ví!');
		}
		$amount = $this->getAmountToInsertFromText($rq->amount);

		if($wallet->name 		== $rq->walletName 
		&& $wallet->amount 		== $amount
		&& $wallet->type_id 	== $rq->type_id
		&& $wallet->description == $rq->description ){
			return redirect()->back()->withErrors('Bạn không thay đổi thông tin nào.');
		}

		Wallet::where('id',$rq->wallet_id)->update([
			'name' 			=> $rq->walletName,
			'amount' 		=> $amount,
			'type_id'		=> $rq->type_id,
			'description' 	=> $rq->description,
			'updated_at'	=> now()
		]);
		return redirect()->back()->with('Success','Thay đổi thông tin thành công!');
	}

	public function deleteWallet($wallet_id){
		$wallet = Wallet::where('id',$wallet_id)->first();
		if(!$wallet || $wallet->User()->first()->id != Auth::user()->id){
			return redirect()->to('wallet')->withErrors('Sai thông tin ví!');
		}
		TransferLog::where('from_id',$wallet_id)->update(['from_id'=> null]);
		TransferLog::where('to_id',$wallet_id)->update(['to_id'=> null]);
		Transaction::where('wallet_id',$wallet_id)->delete();
		Wallet::where('id',$wallet_id)->delete();
		return redirect()->to('wallet')->with('Success','Xóa ví và các thông tin liên quan thành công!');
	}

	public function chooseWalletToTransfer(){
		$wallets = Auth::user()->Wallets()->get();
		if($wallets->count()==0) return redirect()->to('wallet/choose')->withErrors('Bạn chưa có ví nào! Hãy tạo ví trước!');

		return view('Management.Wallet.ChooseWalletToTransferMoney')->with('wallets',$wallets);
	}

	public function transferMoneyForm(ChooseWalletToTransferMoneyRequest $rq){
		$fromWallet =  Wallet::where('id',$rq->from_id)->first();
		$toWallet =  Wallet::where('id',$rq->to_id)->first();
		if($fromWallet->user_id != Auth::user()->id || $toWallet->user_id != Auth::user()->id){ return redirect()->back()->withErrors("Ví không đúng!");}
		return view('Management.Wallet.TransferMoneyForm')->with('data',['from'=>$fromWallet, 'to'=>$toWallet]);
	}

	public function transferMoney(TransferMoneyRequest $rq){
		if(!Hash::check($rq->password,Auth::user()->password)){    
			return redirect()->back()->withErrors('Sai mật khẩu');
		}
		$fromWallet = Wallet::where('id',$rq->from_id);
		$toWallet = Wallet::where('id',$rq->to_id);

		$fromWalletAmount = $fromWallet->select('amount')->first()->amount;
		$toWalletAmount = $toWallet->select('amount')->first()->amount;

		if($fromWallet->select('user_id')->first()->user_id != Auth::user()->id || $toWallet->select('user_id')->first()->user_id != Auth::user()->id){ return redirect()->back()->withErrors('Ví không hợp lệ!');}
		$fromWalletTransferTime = $fromWallet->select('transfer_time')->first()->transfer_time;
		$toWalletTransferTime = $toWallet->select('transfer_time')->first()->transfer_time;

		$transferAmount = $this->getAmountToInsertFromText($rq->transferAmount);
		if($fromWalletAmount < $transferAmount){ return redirect()->back()->withErrors("Số dư trong ví nhỏ hơn số tiền chuyển!");}
		if($transferAmount == 0){return redirect()->back()->withErrors("Chuyển tiền vô hiệu! Số dư bằng 0");}

		$fromWallet->update(['amount' => $fromWalletAmount-$transferAmount,'transfer_time' => $fromWalletTransferTime + 1]);
		$toWallet->update(['amount' => $toWalletAmount+$transferAmount,'transfer_time' =>$toWalletTransferTime + 1]);
		TransferLog::create(['from_id'=>$rq->from_id,'to_id'=>$rq->to_id,'amount'=>$transferAmount,'created_at'=>now()]);
		return redirect()->to('wallet')->with('Success','Chuyển tiền từ '.$fromWallet->select('name')->first()->name.' sang '.$toWallet->select('name')->first()->name.' thành công!');
	}

	public function sortTransByDate( $a, $b ) {
		return strtotime($b->transaction_date) - strtotime($a->transaction_date);
	}
}

<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddTransactionRequest;
use App\Http\Requests\AddTransactionTypeRequest;
use App\Http\Requests\ChangeTransactionDetailRequest;
use App\Models\Wallet;
use App\Models\Task;
use App\Models\TransactionType;
use App\Models\Transaction;

class TransactionController extends Controller
{
	public function __construct(){
		$this->middleware('notguest');
	}

	public function chooseWalletToDoTransaction(){
		$wallets = Wallet::whereRaw("user_id = ".Auth::user()->id." AND (type_id = 2 or type_id = 1)")->get();
		if($wallets->count()==0) return redirect()->to('wallet/choose')->withErrors('Bạn chưa có ví nào! Hãy tạo ví trước!');
		return view('Management.Transaction.ChooseWalletToDoTransaction')->with('wallets',$wallets);
	}

	public function transactionForm($wallet_id){
		$wallet = Wallet::where('id',$wallet_id)->first();
		//Check owner
		if(!$wallet || $wallet->User()->first()->id != Auth::user()->id){
			return redirect()->to('transaction/choosewallet')->withErrors('Ví này không tồn tại!');
		};
		return view('Management.Transaction.TransactionForm')
		->with('data',[
			'wallet'=>$wallet,
			'type'=>$this->_2LevelTransactionTypeMenu(), 
			'maxDate'=>explode(' ',now()->endOfWeek()->addDay())[0]
		]);
	}

	private function _2LevelTransactionTypeMenu(){
		$type = TransactionType::where('user_id',0)->orWhere('user_id',Auth::user()->id)->get();
		$transactionTypes = array();
		$del = array();
		foreach ($type as $t) {
			$subType = array();
			$icon = "";
			if(explode('/',$t->icon)[0] == "assets"){
				$icon = url($t->icon);
			}else{
				$icon = $t->icon;
			}
			foreach ($type as $key =>$ty) {
				$_icon = "";
				if(explode('/',$ty->icon)[0] == "assets"){
					$_icon = url($ty->icon);
				}else{
					$_icon = $ty->icon;
				}
				if($ty->parent_id == $t->id){
					array_push($subType, array('id'=>$ty->id,'name'=>$ty->name,'icon'=>$_icon,'type'=>$ty->type,'parent'=>$t->name));
					array_push($del, $key);
				}
			}
			$data = array('id'=>$t->id,'name'=>$t->name,'icon'=>$icon,'type'=>$t->type,'child'=>$subType);
			array_push($transactionTypes,$data);
		}
		foreach ($del as $d) {
			unset($transactionTypes[$d]);
		}
		return $transactionTypes;
	}

	public function addTransaction(AddTransactionRequest $rq){
		$transaction_date = Carbon::create($rq->transaction_date);
		if($transaction_date->timestamp > now()->endOfWeek()->timestamp){
			return redirect()->back()->withErrors('Không thể nhập giao dịch của tuần sau!');
		}

		$wallet = Wallet::where('id',$rq->wallet_id)->first();
		//Check owner
		if(!$wallet || $wallet->User()->first()->id != Auth::user()->id){
			return redirect()->to('transaction/choosewallet')->withErrors('Ví này không tồn tại!');
		};

		$type = TransactionType::where('id',$rq->type_id)->first();
		if(!$type){
			return redirect()->back()->withErrors('Kiểu giao dịch này không tồn tại!');
		}elseif($type->user_id != 0){
			if($type->user_id != Auth::user()->id){
				return redirect()->back()->withErrors('Kiểu giao dịch này không tồn tại!');
			}
		}
		$amount = $this->getAmountToInsertFromText($rq->amount);
		$wallet = Wallet::where('id',$rq->wallet_id);
		$t = TransactionType::where('id',$rq->type_id)->select('type','parent_id');
		$type = $t->first()->type;
		if($type == 0){
			$rs = $wallet->select('amount')->first()->amount - $amount;
			if($rs < 0){
				return redirect()->back()->withErrors('Số dư ví không đủ!');
			}
			$wallet->update(['amount' => $rs]);
		}
		elseif($type == 1) {
			$wallet->update(['amount' => $wallet->select('amount')->first()->amount + $amount]);
		}
		$id = Transaction::create([
			'amount' => $amount,
			'type_id' => $rq->type_id,
			'wallet_id' => $rq->wallet_id,
			'description' => $rq->description,
			'transaction_date' => $transaction_date,
			'created_at' => now()
		])->id;
		if($t->first()->parent_id == 54 || $rq->type_id == 54 || $rq->type_id == 60 || $t->first()->parent_id == 60){
			if($rq->noteChecker == "on"){
				if(!empty($rq->noteDate)){
					if(Carbon::create($rq->noteDate)->timestamp - now()->timestamp <= 0){
						Transaction::where('id',$id)->delete();
						return redirect()->back()->withErrors('Không thể tạo ghi chú với ngày đòi/trả nhỏ hơn hiện tại!');
					}else{
						if($t->first()->parent_id == 54 || $rq->type_id == 54){
							Task::create(['description'=>'Đòi <a class="text-primary" href="'.url('transaction/get').'/'.$id.'">khoản cho vay</a> vào ngày '.explode(' ',$rq->noteDate)[0],'created_at'=>now(),'user_id'=>Auth::user()->id]);
						}else{
							Task::create(['description'=>'Trả <a class="text-primary" href="'.url('transaction/get').'/'.$id.'">khoản nợ</a> vào ngày '.explode(' ',$rq->noteDate)[0],'created_at'=>now(),'user_id'=>Auth::user()->id]);
						}
					}
				}else{
					Transaction::where('id',$id)->delete();
					return redirect()->back()->withErrors('Thiếu dữ liệu! Không thể tạo ghi chú');
				}
			}
		}
		return redirect()->to('')->with('Success','Giao dịch hoàn tất!');
	}

	public function deleteTransaction($transaction_id){
		$transaction = Transaction::where('id',$transaction_id)->with('Wallet')->with('TransactionType')->first();
		if(!$transaction || $transaction->Wallet->user_id != Auth::user()->id){ return redirect()->to('transaction')->withErrors('Giao dịch này không tồn tại!');}
		$oldAmount = $transaction->Wallet->amount;
		if($transaction->TransactionType->type == 0){
			$transaction->Wallet->update(['amount'=>$oldAmount+$transaction->amount]);
		}else{
			if($oldAmount-$transaction->amount<0){return redirect()->back()->withErrors('Nếu xóa giao dịch này, số tiền trong ví sẽ âm! Vui lòng SỬA LẠI SỐ DƯ của ví trước khi xóa!');}
			$transaction->Wallet->update(['amount'=>$oldAmount-$transaction->amount]);
		}
		Transaction::where('id',$transaction_id)->delete();
		return redirect()->to('transaction')->with('Success','Xóa giao dịch thành công!');
	}

	public function getTransactionDetail($transaction_id){
		$transaction = Transaction::where('id',$transaction_id)->with('Wallet')->with('TransactionType')->first();
		if(!$transaction || $transaction->Wallet->user_id != Auth::user()->id){ return redirect()->to('transaction')->withErrors('Giao dịch này không tồn tại!');}
		$oweAmount_text = "";
		$type = $transaction->TransactionType()->first();
		if($type->id == 54 || $type->parent_id == 54 || $type->id == 60 || $type->parent_id == 60){
			$desc = $transaction->description;
			$pos1 = strpos($desc, '[[');
			$pos2 = strpos($desc, ']]');
			$sub = substr($desc,$pos1,$pos2-$pos1);
			if($sub){
				$sub = str_replace(',','.',substr($sub,2,strlen($sub)));
				$arr = explode('/', $sub);
				if(!isset($arr[1])){
					$percent = (float)explode('%',$arr[0])[0];
					$diff = Carbon::create($transaction->transaction_date)->diffInDays(now());
					$oweAmount = $transaction->amount + $diff*($percent/100)*$transaction->amount;
					$oweAmount_text = "Tiền gốc + lãi theo ngày tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$transaction->amount).")";
				}else{
					switch (strtolower($arr[1])) {
						case 'th':
						case 'month':
						case 't':
						case 'tháng':{
							$percent = (float)explode('%',$arr[0])[0];
							$diff = Carbon::create($transaction->transaction_date)->diffInMonths(now());
							$oweAmount = $transaction->amount + $diff*($percent/100)*$transaction->amount;
							$oweAmount_text = "Tiền gốc + lãi theo tháng tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$transaction->amount).")";
						}
						break;
						case 'n':
						case 'year':
						case 'y':
						case 'năm':{
							$percent = (float)explode('%',$arr[0])[0];
							$diff = Carbon::create($transaction->transaction_date)->diffInYears(now());
							$oweAmount = $transaction->amount + $diff*($percent/100)*$transaction->amount;
							$oweAmount_text = "Tiền gốc + lãi theo năm tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$transaction->amount).")";
						}
						break;
						default:{
							$percent = (float)explode('%',$arr[0])[0];
							$diff = Carbon::create($transaction->transaction_date)->diffInDays(now());
							$oweAmount = $transaction->amount + $diff*($percent/100)*$transaction->amount;
							$oweAmount_text = "Tiền gốc + lãi theo ngày tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$transaction->amount).")";
						}
						break;
					}
				}
			}
		}

		return view('Management.Transaction.UpdateTransaction')->with('data',['transaction'=>$transaction,'type'=>$this->_2LevelTransactionTypeMenu(),'oweAmount_text'=>$oweAmount_text]);
	}

	public function changeTransactionDetail(ChangeTransactionDetailRequest $rq){
		$transaction_date = Carbon::create($rq->transaction_date);
		if($transaction_date->timestamp > now()->endOfWeek()->timestamp){
			return redirect()->back()->withErrors('Không thể nhập giao dịch của tuần sau!');
		}

		$oldTransaction = Transaction::where('id',$rq->transaction_id)->with('TransactionType')->first();
		if($oldTransaction->Wallet()->first()->User()->first()->id != Auth::user()->id){
			return redirect()->back()->withErrors('Bạn không có thẩm quyền sửa giao dịch này!');
		}
		if( $this->getAmountToInsertFromText($rq->amount) == $oldTransaction->amount 
			&& $rq->description == $oldTransaction->description
			&& $rq->transaction_date == $oldTransaction->transaction_date 
			&& $rq->type_id == $oldTransaction->type_id){
			return redirect()->back()->withErrors('Bạn không thay đổi gì cả!');
	}

	$wallet = $oldTransaction->Wallet()->first();
	$oldTransactionType = $oldTransaction->TransactionType;
	$newTransactionType = TransactionType::where('id',$rq->type_id)->select('type','user_id')->first();
	if($newTransactionType->user_id != 0 ){
		if($newTransactionType->user_id != Auth::user()->id){ return redirect()->back()->withErrors('Loại giao dịch này không tồn tại!');}
	}
	if($oldTransactionType->type == 0){
		Wallet::where('id',$wallet->id)->update(['amount' => $wallet->amount + $oldTransaction->amount]);
	}else{
		Wallet::where('id',$wallet->id)->update(['amount' => $wallet->amount - $oldTransaction->amount]);
	}

		//re-get 
	$wallet = $oldTransaction->Wallet()->first();
	if($newTransactionType->type == 0){
		if(($wallet->amount - $this->getAmountToInsertFromText($rq->amount))<0){
			Wallet::where('id',$wallet->id)->update(['amount' => $wallet->amount + $oldTransaction->amount]);
			return redirect()->back()->withErrors("Số dư trong ví không đáp ứng thay đổi này!");}
		Wallet::where('id',$wallet->id)->update([
			'amount' => $wallet->amount - $this->getAmountToInsertFromText($rq->amount)
		]);
		Transaction::where('id',$rq->transaction_id)
		->update([
			'amount'=> $this->getAmountToInsertFromText($rq->amount),
			'type_id'=>$rq->type_id ,
			'description' => $rq->description, 
			'transaction_date' => $rq->transaction_date
		]);
	}else{
		Wallet::where('id',$wallet->id)->update([
			'amount' => $wallet->amount + $this->getAmountToInsertFromText($rq->amount)
		]);
		Transaction::where('id',$rq->transaction_id)->update([
			'amount'=> $this->getAmountToInsertFromText($rq->amount),
			'type_id'=>$rq->type_id ,
			'description' => $rq->description, 
			'transaction_date' => $rq->transaction_date
		]);
	}
	$redirect = '/transaction/get/'.$rq->transaction_id;
	return redirect()->to($redirect)->with('Success','Thay đổi giao dịch thành công!');
}

public function getListTransaction(Request $rq){
		//get all transaction of user
	$userWalletsWithTransaction = Auth::user()->Wallets()->with('Transactions')->get();

	$rsArray = $this->getGroupedTransactionFollowDate($userWalletsWithTransaction);
	$currentPage = LengthAwarePaginator::resolveCurrentPage();
	$rsCollection = collect($rsArray);
	$perPage = 3;
	$currentPageItems = $rsCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
	$paginatedItems= new LengthAwarePaginator($currentPageItems , count($rsCollection), $perPage);
	$paginatedItems->setPath($rq->url());

	return view('Management.Transaction.ListTransaction')->with('groupedTransactions',$paginatedItems);
}

public function getGroupedTransactionFollowDate($userWalletsWithTransaction){
	$allTransactions = array();
	foreach ($userWalletsWithTransaction as $wallet) {
		if(!empty($wallet->Transactions)){
			foreach ($wallet->Transactions as $tr) {
				$oweAmount_text = "";
				$type = TransactionType::where('id',$tr->type_id)->select('id','parent_id','type','name','icon')->first();
				$desc = $tr->description ? $tr->description : "Chưa có ghi chú nào cho giao dịch này....";
				if($type->id == 54 || $type->parent_id == 54 || $type->id == 60 || $type->parent_id == 60){
					$pos1 = strpos($desc, '[[');
					$pos2 = strpos($desc, ']]');
					$sub = substr($desc,$pos1,$pos2-$pos1);
					if($sub){
						$sub = str_replace(',','.',substr($sub,2,strlen($sub)));
						$arr = explode('/', $sub);
						if(!isset($arr[1])){
							$percent = (float)explode('%',$arr[0])[0];
							$diff = Carbon::create($tr->transaction_date)->diffInDays(now());
							$oweAmount = $tr->amount + $diff*($percent/100)*$tr->amount;
							$oweAmount_text = "Tiền gốc + lãi theo ngày tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$tr->amount).")";
						}else{
							switch (strtolower($arr[1])) {
								case 'th':
								case 't':
								case 'tháng':{
									$percent = (float)explode('%',$arr[0])[0];
									$diff = Carbon::create($tr->transaction_date)->diffInMonths(now());
									$oweAmount = $tr->amount + $diff*($percent/100)*$tr->amount;
									$oweAmount_text = "Tiền gốc + lãi theo tháng tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$tr->amount).")";
								}
								break;
								case 'n':
								case 'year':
								case 'y':
								case 'năm':{
									$percent = (float)explode('%',$arr[0])[0];
									$diff = Carbon::create($tr->transaction_date)->diffInYears(now());
									$oweAmount = $tr->amount + $diff*($percent/100)*$tr->amount;
									$oweAmount_text = "Tiền gốc + lãi theo năm tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$tr->amount).")";
								}
								break;
								default:{
									$percent = (float)explode('%',$arr[0])[0];
									$diff = Carbon::create($tr->transaction_date)->diffInDays(now());
									$oweAmount = $tr->amount + $diff*($percent/100)*$tr->amount;
									$oweAmount_text = "Tiền gốc + lãi theo ngày tính tới hiện tại : ".number_format($oweAmount)." VNĐ (+".number_format($diff*($percent/100)*$tr->amount).")";
								}
								break;
							}
						}
					}
				}
				$data = array(
					'id'				=> $tr->id,
					'amount' 			=> $tr->amount, 
					'description' 		=> $desc, 
					'wallet' 			=> $wallet->name,
					'wallet_type'		=> $wallet->WalletType()->first()->name,
					'transaction_date' 	=> $tr->transaction_date, 
					'type' 				=> $type->type, 
					'transaction_name'	=> $type->name,
					'oweAmount'			=> $oweAmount_text,
					'icon'				=> $type->icon);
				array_push($allTransactions, $data);
			}
		}
	}
		//group all transaction into same date
	$groupedTransactions = array();
	foreach ($allTransactions as $key1 => $tr1) {
		$date = explode(' ', $tr1['transaction_date'])[0];
		$data = array();
		foreach ($allTransactions as $key2 => $tr2) {
			if($date == explode(' ', $tr2['transaction_date'])[0]){
				array_push($data,$tr2);
				unset($allTransactions[$key2]);
			}
		}
		if(!empty($data)){
			usort($data, array($this, "sortTransByDate"));
			$explodeDate = explode('-', $date);
			array_push($groupedTransactions,[
				'date'=>$date,
				'date_text'=> $explodeDate[2]." tháng ".$explodeDate[1]." năm ".$explodeDate[0], 
				'transactions'=>$data]);
		}
	}
	usort($groupedTransactions, array($this, "sortGroupByDate"));
	return $groupedTransactions;
}
public function sortTransByDate( $a, $b ) {
	return strtotime($b['transaction_date']) - strtotime($a['transaction_date']);
}
public function sortGroupByDate( $a, $b ) {
	return strtotime($b['date']) - strtotime($a['date']);
}
}
<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use Illuminate\Support\Carbon;
class DashboardController extends Controller
{

	public function __construct()
	{
		$this->middleware('notguest');
	}

	public function Index(){
		//Get nearly transaction
		$wallets = Auth::user()->Wallets()->select('type_id','id')->get();
		$flag = 0;
		foreach ($wallets as $w) {
			if($w->type_id == 1 || $w->type_id == 2){
				$flag = 1;
				break;
			}
		}
		if($flag == 0){return redirect()->to('wallet/choose')->withErrors('Bạn chưa có ví thực hiện giao dịch nào! Vui lòng tạo ví để sử dụng.');}
		$whereClause = $this->whereWallet($wallets);
		$nearTransactions = Transaction::whereRaw($whereClause)->with('TransactionType')->orderBy('transaction_date','Desc')->take(6)->get();
		//End get nearly
		$whereClause = "(transaction_date >= '".now()->startOfMonth()."' and transaction_date <= '".now()->endOfMonth()."'". ") AND (".$whereClause.")";
		$thisMonthTransaction = Transaction::whereRaw($whereClause)->with('TransactionType')->select('amount','type_id')->get();
		//Get percent
		$sumAdd = $this->getTransactionAmountFromTypeInThisMonth(1);
		$sumSub = $this->getTransactionAmountFromTypeInThisMonth(0);

		//Get dataset for chart_1
		$dataset = $this->getTransactionAmountDatasetBetweenDays(now()->startOfWeek(), now()->endOfWeek(),$wallets);
		$lastweek_datasets = $this->getTransactionAmountDatasetBetweenDays(now()->startOfWeek()->subDays(7),now()->endOfWeek()->subDays(7),$wallets);
		//Get dataset for chart_2
		$count = $this->countTransactionDatasetBetweenDays(now()->startOfWeek(), now()->endOfWeek(),$wallets);
		$percent = array('add' => 0, 'subtract' =>0);
		if(count($thisMonthTransaction) != 0){
			$percent = array('add' => number_format(($sumAdd*100)/($sumAdd+$sumSub),2), 'subtract' =>number_format(($sumSub*100)/($sumAdd+$sumSub),2));
		}
		//End
		return view('Management.Dashboard')->with([
			'datasets'=>$dataset,
			'lastweek_datasets' => $lastweek_datasets,
			'count'=>$count,
			'transactions'=>$nearTransactions,
			'percent' => $percent,
			'totalThisMonth' => array('add' =>$sumAdd, 'subtract' =>$sumSub)
		]);
	}
	
	public function countTransactionDatasetBetweenDays($dateStart,$dateEnd,$wallets){
		$whereClause = "(transaction_date >= '".$dateStart."' and transaction_date <= '".$dateEnd."'". ") AND (".$this->whereWallet($wallets).")";
		$transactions = Transaction::whereRaw($whereClause)->with('TransactionType')->select('type_id','transaction_date')->get();
		$date = $this->getDaysBetweenDate($dateStart,$dateEnd);
		$counts = array();
		foreach ($date as $d) {
			$sub = 0;
			$add = 0;
			$total = 0;
			foreach ($transactions as $tr) {
				if($d == explode(' ', $tr->transaction_date)[0]){
					$total += 1;
					if($tr->TransactionType->type == 0){ $sub += 1; }
					else {$add += 1; }
				}
			}
			array_push($counts,array('date'=>$d,'sub'=>$sub,'add'=>$add,'total'=> $total));
		}
		return $this->getConvertedTransactionAmountDataset($counts);
	}

	public function getTransactionAmountDatasetBetweenDays($dateStart,$dateEnd,$wallets){
		$whereClause = "(transaction_date >= '".$dateStart."' and transaction_date <= '".$dateEnd."'". ") AND (".$this->whereWallet($wallets).")";

		$transactions = Transaction::whereRaw($whereClause)->with('TransactionType')->select('type_id','transaction_date','amount')->get();
		$date = $this->getDaysBetweenDate($dateStart,$dateEnd);
		$amounts = $this->getAmountEachDay($date,$transactions);
		return $this->getConvertedTransactionAmountDataset($amounts);
	}

	public function getDaysBetweenDate($dateStart,$dateEnd){
		$date = array();
		while ($dateStart < $dateEnd) {
			array_push($date,explode(' ',$dateStart->toDateTimeString())[0]);
			$dateStart->addDay();
		}
		return $date;
	}

	public function getAmountEachDay($date,$transactions){
		$amounts = array();
		foreach ($date as $d) {
			$sub = 0;
			$add = 0;
			$total = 0;
			foreach ($transactions as $tr) {
				if($d == explode(' ', $tr->transaction_date)[0]){
					$total += $tr->amount;
					if($tr->TransactionType->type == 0){ $sub += $tr->amount; }
					else {$add += $tr->amount; }
				}
			}
			array_push($amounts,array('date'=>$d,'sub'=>$sub,'add'=>$add,'total'=>$total));
		}
		return $amounts;
	}
	public function getConvertedTransactionAmountDataset($amounts){
		$addString = "";
		$subString = "";
		$totalString = "";
		foreach ($amounts as $a) {
			if($addString == ""){
				$addString = "[".$a['add'];
				$subString = "[".$a['sub'];
				$totalString = "[".$a['total'];
			}else{
				$addString = $addString.",".$a['add'];
				$subString = $subString.",".$a['sub'];
				$totalString = $totalString.",".$a['total'];
			}
		}
		$addString = $addString."]";
		$subString = $subString."]";
		$totalString = $totalString."]";
		return array('add'=>$addString,'subtract'=>$subString,'total'=>$totalString);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Carbon;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public static function getTimeDiffSentence($time){
        $dt = Carbon::create($time);
        return $dt->diffForHumans(now());
    }

    public static function getAmountToInsertFromText($rqAmount){
        $amount = explode(' ', $rqAmount)[1];
        $amount = str_replace(',', '', $amount);
        return $amount;
    }

    public static function calTotalMoneyOfType($type=array(1,2)){
        $userID = Auth::user()->id;
        $total = 0;
        foreach ($type as $t) {
            $wallets = Wallet::where('user_id',$userID)->where('type_id',$t)->select('amount')->get();
            foreach ($wallets as $w) {
                $total+=$w->amount;
            }
        }
        return $total;
    }

    public static function getTransactionAmountFromTypeInThisMonth($transaction_type){
        $wallets = Auth::user()->Wallets()->select('type_id','id')->get();
        $ID = "";
        foreach ($wallets as $k => $w) {
            if($w->type_id != 3 && $ID != ""){
                $ID = $ID." or wallet_id = ".$w->id;
            }
            if($w->type_id != 3 && $ID == ""){
                $ID = "wallet_id = ".$w->id;
            }

        }
        $whereClause = "(transaction_date >= '".now()->startOfMonth()."' and transaction_date <= '".now()->endOfMonth()."'". ") AND (".$ID.")";

        $transactions = Transaction::whereRaw($whereClause)->with('TransactionType')->select('type_id','transaction_date','amount')->get();
        $amount = 0;
        foreach ($transactions as $tr) {
            if($tr->TransactionType->type == $transaction_type){$amount += $tr->amount;}
        }
        return $amount;
    }

    public static function getTransactionAmountFromType($transaction_type){
        $wallets = Auth::user()->Wallets()->select('type_id','id')->get();
        $ID = "";
        foreach ($wallets as $k => $w) {
            if($w->type_id != 3 && $ID != ""){
                $ID = $ID." or wallet_id = ".$w->id;
            }
            if($w->type_id != 3 && $ID == ""){
                $ID = "wallet_id = ".$w->id;
            }
        }
        $transactions = Transaction::whereRaw($ID)->with('TransactionType')->select('type_id','transaction_date','amount')->get();
        $amount = 0;
        foreach ($transactions as $tr) {
            if($tr->TransactionType->type == $transaction_type){$amount += $tr->amount;}
        }
        return $amount;
    }
    public function whereWallet($wallets){
        $ID = "";
        foreach ($wallets as $k => $w) {
            if($w->type_id != 3 && $ID != ""){
                $ID = $ID." or wallet_id = ".$w->id;
            }
            if($w->type_id != 3 && $ID == ""){
                $ID = "wallet_id = ".$w->id;
            }
        }
        return $ID;
    }
}

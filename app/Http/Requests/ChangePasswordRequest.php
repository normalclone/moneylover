<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldPassword'   => 'required|min:6|max:32',
            'newPassword'   => 'required|min:6|max:32',
            'newPassword_confirm' => 'required|same:newPassword'
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'min'       => ':attribute phải từ :min kí tự',
            'max'       => ':attribute vượt quá kích thước quy định',
            'same'      => 'Mật khẩu nhập lại không đúng',
        ];
    }
    public function attributes()
    {
        return [
            'oldPassword' => 'Mật khẩu cũ',
            'newPassword'   => 'Mật khẩu mới',
            'newPassword_confirm'   => 'Mật khẩu nhập lại',
        ];
    }
}

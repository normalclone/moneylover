<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChooseWalletToTransferMoneyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'from_id'       => 'required|exists:tb_wallet,id',
            'to_id'      => 'required|exists:tb_wallet,id',
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'exists'    => ':attribute không tồn tại'
        ];
    }
    public function attributes()
    {
        return [
            'from_id' => 'Ví chuyển tiền', 
            'to_id'=> 'Ví nhận tiền',
        ];
    }
}

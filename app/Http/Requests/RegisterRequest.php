<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email|string|max:100|unique:tb_user,email',
            'password' => 'required|min:6|max:32|',
            'repassword' => 'required|same:password',
            'accept' => 'accepted'
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'string' => ':attribute sai định dạng',
            'same' => 'Mật khẩu nhập lại không đúng',
            'unique' => ':attribute đã tồn tại',
            'accepted' => 'Bạn chưa đồng ý với điều khoản của chúng tôi',
            'max' => ':attribute vượt quá :max kí tự quy định',
            'min' => ':attribute phải từ :min kí tự',
            'regex' => ':attribute không được chứa số'
        ];
    }
    public function attributes()
    {
        return [
            'name' => 'Tên',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'repassword' => 'Xác nhận mật khẩu'
        ];
    }
}

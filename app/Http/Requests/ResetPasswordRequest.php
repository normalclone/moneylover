<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newPassword'     => 'required|max:32|min:6',
            'newPassword_confirm'  => 'required|same:newPassword',
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống.',
            'max'       => ':attribute vượt quá :max kí tự quy định.',
            'min'       => ':attribute phải từ :min kí tự.',
            'same'    => 'Mật khẩu nhập lại không đúng'
        ];
    }
    public function attributes()
    {
        return [
            'newPassword'     => 'Mật khẩu mới',
            'newPassword_confirm'  => 'Nhập lại mật khẩu',
        ];
    }
}

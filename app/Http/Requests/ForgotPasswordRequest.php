<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     => 'required|email|string|max:100|exists:tb_user',
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'string'    => ':attribute sai định dạng',
            'max'       => ':attribute vượt quá :max kí tự quy định',
            'exists'    => ':attribute không tồn tại!'
        ];
    }
    public function attributes()
    {
        return [
            'email'     => 'Email',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:100|regex:/^[\pL\s\-]+$/u',
            'password'  => 'required|min:6|max:32',
            'avatar_img'    => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'min'       => ':attribute phải từ :min kí tự',
            'mimes'     => ':attribute sai định dạng',
            'max'       => ':attribute vượt quá kích thước quy định',
            'regex'     => ':attribute không được chứa kí tự số'
        ];
    }
    public function attributes()
    {
        return [
            'avatar_img' => 'Ảnh đại diện',
            'name'      => 'Tên',
            'password'  => 'Mật khẩu',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeTransactionDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => 'required|exists:tb_transaction,id',
            'type_id'   => 'required|exists:tb_transactionType,id',
            'amount'    => 'required',
            'description' => 'max:400',
            'transaction_date' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'exists'   => ':attribute không tồn tại',
            'max'       => ':attribute không được vượt quá :max',
        ];
    }
    public function attributes()
    {
        return [
            'transaction_id'     => 'Giao dịch',
            'type_id'       => 'Kiểu giao dịch',
            'amount'        => 'Số tiền',
            'description'   => 'Mô tả',
            'transaction_date' => 'Ngày thực hiện'
        ];
    }
}

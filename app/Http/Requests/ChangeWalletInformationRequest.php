<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeWalletInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_id'       => 'required',
            'walletName'      => 'required|max:100',
            'amount'          => 'required',
            'type_id'         => 'required'
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'max'       => ':attribute vượt quá kích thước quy định',
        ];
    }
    public function attributes()
    {
        return [
            'wallet_id' => 'Ví', 
            'walletName'=> 'Tên ví',
            'amount'    => 'Số dư',
            'type_id'   => 'Kiểu ví'
        ];
    }
}

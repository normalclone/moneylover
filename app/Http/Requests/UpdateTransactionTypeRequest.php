<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTransactionTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id'   => 'required|exists:tb_transactionType,id',
            'nameType'  => 'required|max:100',
            'icon'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'password'  => 'required'
        ];
    }
    public function messages()
    {
        return [
            'required'  => ':attribute không được để trống',
            'max'       => ':attribute vượt quá kích thước quy định',
            'mimes'     => ':attribute sai định dạng',
            'exists'    => ':attribute không tồn tại',
        ];
    }
    public function attributes()
    {
        return [
            'type_id'      => 'Loại giao dịch',
            'nameType'          => 'Tên kiểu',
            'parent_id'     => 'Kiểu giao dịch cha',
            'icon'          => 'Icon minh họa',
            'type'          => 'Loại',
            'password'      => 'Mật khẩu'
        ];
    }
}

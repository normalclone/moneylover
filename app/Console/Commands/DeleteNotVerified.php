<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class DeleteNotVerified extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DeleteNotVerified';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::delete("DELETE FROM tb_registerToken WHERE TIMESTAMPDIFF(SECOND,created_at,'".now()."') > 300");
        DB::delete("DELETE FROM tb_user WHERE TIMESTAMPDIFF(SECOND,created_at,'".now()."') > 86400 AND status = 0");
        DB::delete("DELETE FROM tb_transferLog WHERE from_id = null AND to_id = null");

        foreach(DB::table("tb_user")->select('id')->whereRaw("expired_at < '".now()."' and id != 0")->get() as $user){
            if(DB::table('tb_wallet')->where('user_id',$user->id)->count() > 0){
                foreach (DB::table('tb_wallet')->select('id')->where('user_id',$user->id)->get() as $wallet) {
                    DB::table('tb_transaction')->where('wallet_id',$wallet->id)->delete();
                    DB::table('tb_transferLog')->where('from_id',$wallet->id)->orWhere('to_id',$wallet->id)->delete();
                }
                DB::table('tb_wallet')->where('user_id',$user->id)->delete();
            }
            DB::table('tb_todo')->where('user_id',$user->id)->delete();
            foreach(DB::table('tb_transactionType')->where('user_id',$user->id)->get() as $type){
                $icon = "./assets/img/icons/uploads/".explode('/',$type->icon)[7];
                if(file_exists($icon)){unlink($icon);}
            }
            DB::table('tb_transactionType')->where('user_id',$user->id)->delete();
            $avt = "./uploads/".explode(url('/'), $user->avatar);
            if(file_exists($avt)){
                unlink($avt);
            }
        }
        DB::table("tb_user")->select('id')->whereRaw("expired_at < '".now()."' and id != 0")->delete();
    }
}
